// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

syntax = "proto3";

package zms.geo.v1;

import "validate/validate.proto";

option go_package = "gitlab.flux.utah.edu/openzms/zms-api/go/zms/geo/v1;geo_v1";

// Generic geospatial types.

// Describes a generic Point.
message Point {
    int32 srid = 1;
    double x = 2;
    double y = 3;
    optional double z = 4;
}

// Describes a Point that is part of an Area.
message AreaPoint {
    string id = 1 [(validate.rules).string.uuid = true];
    string area_id = 2 [(validate.rules).string.uuid = true];
    double x = 3;
    double y = 4;
    optional double z = 5;
}

// Describes a geographic area as a list of AreaPoints with metadata.
message Area {
    string id = 1 [(validate.rules).string.uuid = true];
    string element_id = 2 [(validate.rules).string.uuid = true];
    string name = 3;
    optional string description = 4;
    int32 srid = 5;
    repeated AreaPoint points = 6;
}
