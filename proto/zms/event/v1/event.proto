// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

syntax = "proto3";

package zms.event.v1;

import "validate/validate.proto";
import "google/protobuf/timestamp.proto";

option go_package = "gitlab.flux.utah.edu/openzms/zms-api/go/zms/event/v1;event_v1";

// Generic types of event action or state change associated with a ZMS
// object.
enum EventType {
    ET_UNSPECIFIED   =  0;
    ET_OTHER         =  1;
    ET_CREATED       =  2;
    ET_UPDATED       =  3;
    ET_DELETED       =  4;
    ET_REVOKED       =  5;
    ET_ENABLED       =  6;
    ET_DISABLED      =  7;
    ET_APPROVED      =  8;
    ET_DENIED        =  9;
    ET_SCHEDULED     = 10;
    ET_RUNNING       = 11;
    ET_COMPLETED     = 12;
    ET_FAILED        = 13;
    ET_SUCCEEDED     = 14;
    ET_PROGRESS      = 15;
    ET_SUSPENDED     = 16;
    ET_RESUMED       = 17;
    ET_STARTED       = 18;
    ET_STOPPED       = 19;
    ET_VIOLATION     = 20;
    ET_INTERFERENCE  = 21;
    ET_ACTION        = 22;
    ET_PENDING       = 23;
}

enum EventSourceType {
    EST_UNSPECIFIED = 0;
    EST_IDENTITY    = 1;
    EST_ZMC         = 2;
    EST_DST         = 3;
    EST_ALARM       = 4;
}

// Describes an generic header for a ZMS event.
message EventHeader {
    // The type of event.  It would be ideal if this could be an enumerated
    // type, but we want a generic header for future portability, and we do
    // not want to use `Any` if we can avoid it.
    // Users of the `EventHeader` type should map values stored in
    // event_type to an enumerated type.  The values in the `EventType`
    // proto enumeration above should be accepted as canonical defaults.
    int32 type = 1;
    // The numeric code of the event.
    // Users of the `EventHeader` type should map values stored in
    // event_code to an enumerated type in their service descriptors.
    int32 code = 2;
    // The numeric origin service type of the event, as defined by
    // `EventSourceType`, but open to extension in languages with closed
    // enumerated types.  It would be nicer to make this an actual
    // enumerated type, and technically proto3 enumerations are open integer
    // types, but we want to make it more convenient for other languages.
    int32 source_type = 3;
    // The event source service's UUID.
    string source_id = 4 [(validate.rules).string.uuid = true];
    // The event UUID.
    string id = 5 [(validate.rules).string.uuid = true];
    // The event time.
    optional google.protobuf.Timestamp time = 6;
    // The associated object's UUID, if any.
    optional string object_id = 7 [(validate.rules).string.uuid = true];
    // The associated `User` UUID, if any.
    optional string user_id = 8;
    // The associated `Element` UUID, if any.
    optional string element_id = 9;
}

// Describes a filter for event subscriptions.
message EventFilter {
    // Types of events to match; if empty, match any event type.  Use the
    // canonical type values in the `EventType` enumeration insofar as
    // possible, and extend beyond as needed.
    repeated int32 types = 1;
    // Codes of events to match; if empty, match any event code.  Codes are
    // defined by the event originating service.  You should use the
    // canonical codes in the service's EventCode enumeration insofar as
    // possible, and extend beyond as needed.
    repeated int32 codes = 2;
    // A list of specific object UUIDs to match.
    repeated string object_ids = 3;
    // A list of specific object-associated user UUIDs to match.
    repeated string user_ids = 4;
    // A list of specific object-associated element UUIDs to match.
    repeated string element_ids = 5;

}
