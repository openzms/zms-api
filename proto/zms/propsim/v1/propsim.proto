// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

syntax = "proto3";

package zms.propsim.v1;

import "validate/validate.proto";
import "google/protobuf/timestamp.proto";

option go_package = "gitlab.flux.utah.edu/openzms/zms-api/go/zms/propsim/v1;propsim_v1";

message AreaPoint {
    double x = 1;
    double y = 2;
    double z = 3;
}

message Area {
    int32 crs_ogc_srid = 1;
    repeated AreaPoint points = 2;
}

enum MapType {
    MT_UNSPECIFIED = 0;
    MT_RAW         = 1;
    MT_GEOTIFF     = 2;
    MT_DEM         = 3;
}

enum MapKind {
    MK_UNSPECIFIED = 0;
    MK_OTHER       = 1;
    MK_PATHLOSS    = 2;
    MK_POWER       = 3;
    MK_LOS         = 4;
}

enum MapUnit {
    MU_UNSPECIFIED = 0;
    MU_OTHER       = 1;
    MU_dB          = 2;
    MU_dBm         = 3;
    MU_mW          = 4;
    MU_LOS         = 5;
}

message HttpBasicAuth {
    string username = 1;
    string password = 2;
}

message HttpHeaderAuth {
    string name = 1;
    string value = 2;
}

message UrlData {
    string url = 1;
    uint64 size = 2;
    oneof auth {
	HttpBasicAuth basic = 3;
	HttpHeaderAuth header = 4;
    }
    optional string desc = 5;
}

message RawData {
    bytes data = 1;
    optional string format = 2;
    optional string desc = 3;
}

message Map {
    string id = 1 [(validate.rules).string.uuid = true];
    MapType type = 2;
    Area area = 3;
    oneof data {
	RawData raw_data = 4;
	UrlData url_data = 5;
    }
    MapKind kind = 6;
    MapUnit unit = 7;
}

message Feature {
    string name = 1;
    optional string description = 2;
}

enum ParameterType {
    PARAMETER_TYPE_BOOL = 0;
    PARAMETER_TYPE_INT = 1;
    PARAMETER_TYPE_DOUBLE = 2;
    PARAMETER_TYPE_STRING = 3;
}

message ParameterDefinition {
    string name = 1;
    ParameterType ptype = 2;
    string desc = 3;
    bool is_required = 4;
    oneof default_value {
	bool b = 5;
	int64 i = 6;
	double d = 7;
	string s = 8;
    }
}

message ParameterValue {
    string name = 1;
    oneof value {
	bool b = 2;
	int64 i = 3;
	double d = 4;
	string s = 5;
    }
}

enum JobStatus {
    JS_UNSPECIFIED = 0;
    JS_CREATED     = 1;
    JS_SCHEDULED   = 2;
    JS_RUNNING     = 3;
    JS_COMPLETE    = 4;
    JS_FAILED      = 5;
    JS_DELETED     = 6;
}

enum JobEvent {
    JOB_EVENT_STATUS = 0;
    JOB_EVENT_PROGRESS = 1;
    JOB_EVENT_OUTPUT = 2;
}

message JobOutput {
    string id = 1 [(validate.rules).string.uuid = true];
    string name = 2;
    optional string desc = 3;
    oneof output {
	Map map = 4;
	RawData raw_data = 5;
	UrlData url_data = 6;
    }
}

message Job {
    string id = 1 [(validate.rules).string.uuid = true];
    repeated ParameterValue parameters = 4;
    optional Area area = 2;
    optional Map base_map = 3;
    optional JobStatus status = 5;
    optional float completion_percentage = 6;
    repeated JobOutput outputs = 7;
    optional google.protobuf.Timestamp created_at = 8;
    optional google.protobuf.Timestamp estimated_start = 9;
    optional google.protobuf.Timestamp estimated_finish = 10;
    optional google.protobuf.Timestamp started_at = 11;
    optional google.protobuf.Timestamp updated_at = 12;
    optional google.protobuf.Timestamp finished_at = 13;
    optional google.protobuf.Timestamp deleted_at = 14;
}

message ServiceDescriptor {
    // describes a Server's capabilities
    string id = 1 [(validate.rules).string.uuid = true];
    string endpoint = 2;
    string description = 3;
    string version = 4;
    string api_version = 5;
    bool persistent = 6;
    repeated Feature features = 7;
    repeated ParameterDefinition parameters = 8;
}

message GetServiceDescriptorRequest {
}

message GetServiceDescriptorResponse {
    ServiceDescriptor desc = 1;
}

message CreateJobRequest {
    Job job = 1;
}

message CreateJobResponse {
    string job_id = 1 [(validate.rules).string.uuid = true];
}

message EstimateJobRequest {
    string job_id = 1 [(validate.rules).string.uuid = true];
}

message EstimateJobResponse {
    int64 total_time = 1;
    optional google.protobuf.Timestamp estimated_start = 2;
}

message StartJobRequest {
    string job_id = 1 [(validate.rules).string.uuid = true];
}

message StartJobResponse {
}

message PauseJobRequest {
    string job_id = 1 [(validate.rules).string.uuid = true];
}

message PauseJobResponse {
}

message CancelJobRequest {
    string job_id = 1 [(validate.rules).string.uuid = true];
}

message CancelJobResponse {
}

message DeleteJobRequest {
    string job_id = 1 [(validate.rules).string.uuid = true];
}

message DeleteJobResponse {
}

message GetJobEventsRequest {
    string job_id = 1 [(validate.rules).string.uuid = true];
}

message GetJobEventsResponse {
    JobEvent event = 1;
    Job job = 2;
}

message GetJobOutputRequest {
    string job_id = 1 [(validate.rules).string.uuid = true];
    string output_id = 2 [(validate.rules).string.uuid = true];
}

message GetJobOutputResponse {
    JobOutput output = 1;
}

// The Propagation Simulation (Propsim) service API for OpenZMS.  Provides a
// general, parameterizable job run interface, and supports a variety of
// output formats (e.g. `geotiff` maps, raw data series).  In OpenZMS, this
// API is typically consumed by the DST (Digital Spectrum Twin) service to
// run propagation simulations for specific radio ports in the Zone.
service JobService {
    // Returns this service's descriptor: its features, parameters, and run
    // constraints (e.g. parallel job limitations, its
    // persistence/statefulness, etc).
    rpc GetServiceDescriptor(GetServiceDescriptorRequest) returns (GetServiceDescriptorResponse);
    // Creates a Job in accordance with request parameters, or returns an
    // error.  Services must allow Jobs to be created, even if they
    // subsequently refuse to Start them, which they may do temporarily if
    // insufficient resources are available.
    rpc CreateJob(CreateJobRequest) returns (CreateJobResponse);
    // Returns an estimate of job computation duration.  (Optional.)
    rpc EstimateJob(EstimateJobRequest) returns (EstimateJobResponse);
    // Starts a Job.  The service may refuse to Start a Job if it lacks
    // resources.
    rpc StartJob(StartJobRequest) returns (StartJobResponse);
    // Pauses execution of a Job (e.g., so that a higher-priority job may
    // have a better chance of execution).  Optional.
    rpc PauseJob(PauseJobRequest) returns (PauseJobResponse);
    // Cancels ongoing execution of a Job entirely.  Optional.
    rpc CancelJob(CancelJobRequest) returns (CancelJobResponse);
    // Deletes a Job from memory, and persistence storage, if persistent.
    rpc DeleteJob(DeleteJobRequest) returns (DeleteJobResponse);
    // Returns a stream of JobEvents.  If a client calls this on an
    // incomplete job, the server must close the stream when the job becomes
    // complete or failed.  If a client calls this on a completed or failed
    // job that is not deleted, we will close on deletion.
    rpc GetJobEvents(GetJobEventsRequest) returns (stream GetJobEventsResponse);
    // Returns a JobOutput.
    rpc GetJobOutput(GetJobOutputRequest) returns (GetJobOutputResponse);
}
