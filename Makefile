# SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
#
# SPDX-License-Identifier: Apache-2.0

PROTOIMAGE="gitlab.flux.utah.edu:4567/openzms/zms-api/protoc-go:latest"
PROTOPYTHONIMAGE="gitlab.flux.utah.edu:4567/openzms/zms-api/protoc-python:latest"
PROTOC_GO_ARGS = \
	-I/go/pkg/mod/github.com/envoyproxy/protoc-gen-validate@v1.0.2 \
	--proto_path=proto/ \
	--go_out=paths=source_relative:./go \
	--go-grpc_out=paths=source_relative:./go \
	--validate_out=lang=go,paths=source_relative:./go

.PHONY: build

all: protos

protoc-images:
	docker build -f docker/Dockerfile.protoc-go -t ${PROTOIMAGE} .
	docker build -f docker/Dockerfile.protoc-python -t ${PROTOPYTHONIMAGE} .

protos:
	docker run --rm -it \
		-v `pwd`:/app \
		-w /app \
		$(PROTOIMAGE) \
		protoc \
			$(PROTOC_GO_ARGS) \
			--doc_out=docs/zms/event/v1 \
			--doc_opt=markdown,event.md \
			zms/event/v1/event.proto
	docker run --rm -it \
		-v `pwd`:/app \
		-w /app \
		$(PROTOIMAGE) \
		protoc \
			$(PROTOC_GO_ARGS) \
			--doc_out=docs/zms/geo/v1 \
			--doc_opt=markdown,geo.md \
			zms/geo/v1/geo.proto
	docker run --rm -it \
		-v `pwd`:/app \
		-w /app \
		$(PROTOIMAGE) \
		protoc \
			$(PROTOC_GO_ARGS) \
			--doc_out=docs/zms/identity/v1 \
			--doc_opt=markdown,identity.md \
			zms/identity/v1/identity.proto
	docker run --rm -it \
		-v `pwd`:/app \
		-w /app \
		$(PROTOIMAGE) \
		protoc \
			$(PROTOC_GO_ARGS) \
			--doc_out=docs/zms/zmc/v1 \
			--doc_opt=markdown,zmc.md \
			zms/zmc/v1/zmc.proto
	docker run --rm -it \
		-v `pwd`:/app \
		-w /app \
		$(PROTOIMAGE) \
		protoc \
			$(PROTOC_GO_ARGS) \
			--doc_out=docs/zms/dst/v1 \
			--doc_opt=markdown,dst.md \
			zms/dst/v1/dst.proto
	docker run --rm -it \
		-v `pwd`:/app \
		-w /app \
		$(PROTOIMAGE) \
		protoc \
			$(PROTOC_GO_ARGS) \
			--doc_out=docs/zms/alarm/v1 \
			--doc_opt=markdown,alarm.md \
			zms/alarm/v1/alarm.proto
	docker run --rm -it \
		-v `pwd`:/app \
		-w /app \
		$(PROTOIMAGE) \
		protoc \
			$(PROTOC_GO_ARGS) \
			--doc_out=docs/zms/propsim/v1 \
			--doc_opt=markdown,propsim.md \
			zms/propsim/v1/propsim.proto \
			zms/propsim/v1/controller.proto
	docker run --rm -it \
		-v `pwd`:/app \
		-w /app \
		$(PROTOPYTHONIMAGE) \
		/app/build/bin/python-protoc-helper \
			/protos/validate/validate.proto
	docker run --rm -it \
		-v `pwd`:/app \
		-w /app \
		$(PROTOPYTHONIMAGE) \
		/app/build/bin/python-protoc-helper \
			proto/zms/event/v1/event.proto
	docker run --rm -it \
		-v `pwd`:/app \
		-w /app \
		$(PROTOPYTHONIMAGE) \
		/app/build/bin/python-protoc-helper \
			proto/zms/geo/v1/geo.proto
	docker run --rm -it \
		-v `pwd`:/app \
		-w /app \
		$(PROTOPYTHONIMAGE) \
		/app/build/bin/python-protoc-helper \
			proto/zms/identity/v1/identity.proto
	docker run --rm -it \
		-v `pwd`:/app \
		-w /app \
		$(PROTOPYTHONIMAGE) \
		/app/build/bin/python-protoc-helper \
			proto/zms/zmc/v1/zmc.proto
	docker run --rm -it \
		-v `pwd`:/app \
		-w /app \
		$(PROTOPYTHONIMAGE) \
		/app/build/bin/python-protoc-helper \
			proto/zms/dst/v1/dst.proto
	docker run --rm -it \
		-v `pwd`:/app \
		-w /app \
		$(PROTOPYTHONIMAGE) \
		/app/build/bin/python-protoc-helper \
			proto/zms/alarm/v1/alarm.proto
	docker run --rm -it \
		-v `pwd`:/app \
		-w /app \
		$(PROTOPYTHONIMAGE) \
		/app/build/bin/python-protoc-helper \
			proto/zms/propsim/v1/propsim.proto \
			proto/zms/propsim/v1/controller.proto
