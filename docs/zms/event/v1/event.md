# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [zms/event/v1/event.proto](#zms_event_v1_event-proto)
    - [EventFilter](#zms-event-v1-EventFilter)
    - [EventHeader](#zms-event-v1-EventHeader)
  
    - [EventSourceType](#zms-event-v1-EventSourceType)
    - [EventType](#zms-event-v1-EventType)
  
- [Scalar Value Types](#scalar-value-types)



<a name="zms_event_v1_event-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## zms/event/v1/event.proto



<a name="zms-event-v1-EventFilter"></a>

### EventFilter
Describes a filter for event subscriptions.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| types | [int32](#int32) | repeated | Types of events to match; if empty, match any event type. Use the canonical type values in the `EventType` enumeration insofar as possible, and extend beyond as needed. |
| codes | [int32](#int32) | repeated | Codes of events to match; if empty, match any event code. Codes are defined by the event originating service. You should use the canonical codes in the service&#39;s EventCode enumeration insofar as possible, and extend beyond as needed. |
| object_ids | [string](#string) | repeated | A list of specific object UUIDs to match. |
| user_ids | [string](#string) | repeated | A list of specific object-associated user UUIDs to match. |
| element_ids | [string](#string) | repeated | A list of specific object-associated element UUIDs to match. |






<a name="zms-event-v1-EventHeader"></a>

### EventHeader
Describes an generic header for a ZMS event.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| type | [int32](#int32) |  | The type of event. It would be ideal if this could be an enumerated type, but we want a generic header for future portability, and we do not want to use `Any` if we can avoid it. Users of the `EventHeader` type should map values stored in event_type to an enumerated type. The values in the `EventType` proto enumeration above should be accepted as canonical defaults. |
| code | [int32](#int32) |  | The numeric code of the event. Users of the `EventHeader` type should map values stored in event_code to an enumerated type in their service descriptors. |
| source_type | [int32](#int32) |  | The numeric origin service type of the event, as defined by `EventSourceType`, but open to extension in languages with closed enumerated types. It would be nicer to make this an actual enumerated type, and technically proto3 enumerations are open integer types, but we want to make it more convenient for other languages. |
| source_id | [string](#string) |  | The event source service&#39;s UUID. |
| id | [string](#string) |  | The event UUID. |
| time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional | The event time. |
| object_id | [string](#string) | optional | The associated object&#39;s UUID, if any. |
| user_id | [string](#string) | optional | The associated `User` UUID, if any. |
| element_id | [string](#string) | optional | The associated `Element` UUID, if any. |





 


<a name="zms-event-v1-EventSourceType"></a>

### EventSourceType


| Name | Number | Description |
| ---- | ------ | ----------- |
| EST_UNSPECIFIED | 0 |  |
| EST_IDENTITY | 1 |  |
| EST_ZMC | 2 |  |
| EST_DST | 3 |  |
| EST_ALARM | 4 |  |



<a name="zms-event-v1-EventType"></a>

### EventType
Generic types of event action or state change associated with a ZMS
object.

| Name | Number | Description |
| ---- | ------ | ----------- |
| ET_UNSPECIFIED | 0 |  |
| ET_OTHER | 1 |  |
| ET_CREATED | 2 |  |
| ET_UPDATED | 3 |  |
| ET_DELETED | 4 |  |
| ET_REVOKED | 5 |  |
| ET_ENABLED | 6 |  |
| ET_DISABLED | 7 |  |
| ET_APPROVED | 8 |  |
| ET_DENIED | 9 |  |
| ET_SCHEDULED | 10 |  |
| ET_RUNNING | 11 |  |
| ET_COMPLETED | 12 |  |
| ET_FAILED | 13 |  |
| ET_SUCCEEDED | 14 |  |
| ET_PROGRESS | 15 |  |
| ET_SUSPENDED | 16 |  |
| ET_RESUMED | 17 |  |
| ET_STARTED | 18 |  |
| ET_STOPPED | 19 |  |
| ET_VIOLATION | 20 |  |
| ET_INTERFERENCE | 21 |  |
| ET_ACTION | 22 |  |
| ET_PENDING | 23 |  |


 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

