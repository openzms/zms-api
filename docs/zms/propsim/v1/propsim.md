# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [zms/propsim/v1/propsim.proto](#zms_propsim_v1_propsim-proto)
    - [Area](#zms-propsim-v1-Area)
    - [AreaPoint](#zms-propsim-v1-AreaPoint)
    - [CancelJobRequest](#zms-propsim-v1-CancelJobRequest)
    - [CancelJobResponse](#zms-propsim-v1-CancelJobResponse)
    - [CreateJobRequest](#zms-propsim-v1-CreateJobRequest)
    - [CreateJobResponse](#zms-propsim-v1-CreateJobResponse)
    - [DeleteJobRequest](#zms-propsim-v1-DeleteJobRequest)
    - [DeleteJobResponse](#zms-propsim-v1-DeleteJobResponse)
    - [EstimateJobRequest](#zms-propsim-v1-EstimateJobRequest)
    - [EstimateJobResponse](#zms-propsim-v1-EstimateJobResponse)
    - [Feature](#zms-propsim-v1-Feature)
    - [GetJobEventsRequest](#zms-propsim-v1-GetJobEventsRequest)
    - [GetJobEventsResponse](#zms-propsim-v1-GetJobEventsResponse)
    - [GetJobOutputRequest](#zms-propsim-v1-GetJobOutputRequest)
    - [GetJobOutputResponse](#zms-propsim-v1-GetJobOutputResponse)
    - [GetServiceDescriptorRequest](#zms-propsim-v1-GetServiceDescriptorRequest)
    - [GetServiceDescriptorResponse](#zms-propsim-v1-GetServiceDescriptorResponse)
    - [HttpBasicAuth](#zms-propsim-v1-HttpBasicAuth)
    - [HttpHeaderAuth](#zms-propsim-v1-HttpHeaderAuth)
    - [Job](#zms-propsim-v1-Job)
    - [JobOutput](#zms-propsim-v1-JobOutput)
    - [Map](#zms-propsim-v1-Map)
    - [ParameterDefinition](#zms-propsim-v1-ParameterDefinition)
    - [ParameterValue](#zms-propsim-v1-ParameterValue)
    - [PauseJobRequest](#zms-propsim-v1-PauseJobRequest)
    - [PauseJobResponse](#zms-propsim-v1-PauseJobResponse)
    - [RawData](#zms-propsim-v1-RawData)
    - [ServiceDescriptor](#zms-propsim-v1-ServiceDescriptor)
    - [StartJobRequest](#zms-propsim-v1-StartJobRequest)
    - [StartJobResponse](#zms-propsim-v1-StartJobResponse)
    - [UrlData](#zms-propsim-v1-UrlData)
  
    - [JobEvent](#zms-propsim-v1-JobEvent)
    - [JobStatus](#zms-propsim-v1-JobStatus)
    - [MapKind](#zms-propsim-v1-MapKind)
    - [MapType](#zms-propsim-v1-MapType)
    - [MapUnit](#zms-propsim-v1-MapUnit)
    - [ParameterType](#zms-propsim-v1-ParameterType)
  
    - [JobService](#zms-propsim-v1-JobService)
  
- [zms/propsim/v1/controller.proto](#zms_propsim_v1_controller-proto)
    - [RegisterJobServiceRequest](#zms-propsim-v1-RegisterJobServiceRequest)
    - [RegisterJobServiceResponse](#zms-propsim-v1-RegisterJobServiceResponse)
  
    - [PropSimController](#zms-propsim-v1-PropSimController)
  
- [Scalar Value Types](#scalar-value-types)



<a name="zms_propsim_v1_propsim-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## zms/propsim/v1/propsim.proto



<a name="zms-propsim-v1-Area"></a>

### Area



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| crs_ogc_srid | [int32](#int32) |  |  |
| points | [AreaPoint](#zms-propsim-v1-AreaPoint) | repeated |  |






<a name="zms-propsim-v1-AreaPoint"></a>

### AreaPoint



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| x | [double](#double) |  |  |
| y | [double](#double) |  |  |
| z | [double](#double) |  |  |






<a name="zms-propsim-v1-CancelJobRequest"></a>

### CancelJobRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| job_id | [string](#string) |  |  |






<a name="zms-propsim-v1-CancelJobResponse"></a>

### CancelJobResponse







<a name="zms-propsim-v1-CreateJobRequest"></a>

### CreateJobRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| job | [Job](#zms-propsim-v1-Job) |  |  |






<a name="zms-propsim-v1-CreateJobResponse"></a>

### CreateJobResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| job_id | [string](#string) |  |  |






<a name="zms-propsim-v1-DeleteJobRequest"></a>

### DeleteJobRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| job_id | [string](#string) |  |  |






<a name="zms-propsim-v1-DeleteJobResponse"></a>

### DeleteJobResponse







<a name="zms-propsim-v1-EstimateJobRequest"></a>

### EstimateJobRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| job_id | [string](#string) |  |  |






<a name="zms-propsim-v1-EstimateJobResponse"></a>

### EstimateJobResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| total_time | [int64](#int64) |  |  |
| estimated_start | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |






<a name="zms-propsim-v1-Feature"></a>

### Feature



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  |  |
| description | [string](#string) | optional |  |






<a name="zms-propsim-v1-GetJobEventsRequest"></a>

### GetJobEventsRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| job_id | [string](#string) |  |  |






<a name="zms-propsim-v1-GetJobEventsResponse"></a>

### GetJobEventsResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| event | [JobEvent](#zms-propsim-v1-JobEvent) |  |  |
| job | [Job](#zms-propsim-v1-Job) |  |  |






<a name="zms-propsim-v1-GetJobOutputRequest"></a>

### GetJobOutputRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| job_id | [string](#string) |  |  |
| output_id | [string](#string) |  |  |






<a name="zms-propsim-v1-GetJobOutputResponse"></a>

### GetJobOutputResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| output | [JobOutput](#zms-propsim-v1-JobOutput) |  |  |






<a name="zms-propsim-v1-GetServiceDescriptorRequest"></a>

### GetServiceDescriptorRequest







<a name="zms-propsim-v1-GetServiceDescriptorResponse"></a>

### GetServiceDescriptorResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| desc | [ServiceDescriptor](#zms-propsim-v1-ServiceDescriptor) |  |  |






<a name="zms-propsim-v1-HttpBasicAuth"></a>

### HttpBasicAuth



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| username | [string](#string) |  |  |
| password | [string](#string) |  |  |






<a name="zms-propsim-v1-HttpHeaderAuth"></a>

### HttpHeaderAuth



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  |  |
| value | [string](#string) |  |  |






<a name="zms-propsim-v1-Job"></a>

### Job



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| parameters | [ParameterValue](#zms-propsim-v1-ParameterValue) | repeated |  |
| area | [Area](#zms-propsim-v1-Area) | optional |  |
| base_map | [Map](#zms-propsim-v1-Map) | optional |  |
| status | [JobStatus](#zms-propsim-v1-JobStatus) | optional |  |
| completion_percentage | [float](#float) | optional |  |
| outputs | [JobOutput](#zms-propsim-v1-JobOutput) | repeated |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| estimated_start | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| estimated_finish | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| started_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| finished_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |






<a name="zms-propsim-v1-JobOutput"></a>

### JobOutput



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| desc | [string](#string) | optional |  |
| map | [Map](#zms-propsim-v1-Map) |  |  |
| raw_data | [RawData](#zms-propsim-v1-RawData) |  |  |
| url_data | [UrlData](#zms-propsim-v1-UrlData) |  |  |






<a name="zms-propsim-v1-Map"></a>

### Map



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| type | [MapType](#zms-propsim-v1-MapType) |  |  |
| area | [Area](#zms-propsim-v1-Area) |  |  |
| raw_data | [RawData](#zms-propsim-v1-RawData) |  |  |
| url_data | [UrlData](#zms-propsim-v1-UrlData) |  |  |
| kind | [MapKind](#zms-propsim-v1-MapKind) |  |  |
| unit | [MapUnit](#zms-propsim-v1-MapUnit) |  |  |






<a name="zms-propsim-v1-ParameterDefinition"></a>

### ParameterDefinition



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  |  |
| ptype | [ParameterType](#zms-propsim-v1-ParameterType) |  |  |
| desc | [string](#string) |  |  |
| is_required | [bool](#bool) |  |  |
| b | [bool](#bool) |  |  |
| i | [int64](#int64) |  |  |
| d | [double](#double) |  |  |
| s | [string](#string) |  |  |






<a name="zms-propsim-v1-ParameterValue"></a>

### ParameterValue



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  |  |
| b | [bool](#bool) |  |  |
| i | [int64](#int64) |  |  |
| d | [double](#double) |  |  |
| s | [string](#string) |  |  |






<a name="zms-propsim-v1-PauseJobRequest"></a>

### PauseJobRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| job_id | [string](#string) |  |  |






<a name="zms-propsim-v1-PauseJobResponse"></a>

### PauseJobResponse







<a name="zms-propsim-v1-RawData"></a>

### RawData



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| data | [bytes](#bytes) |  |  |
| format | [string](#string) | optional |  |
| desc | [string](#string) | optional |  |






<a name="zms-propsim-v1-ServiceDescriptor"></a>

### ServiceDescriptor



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | describes a Server&#39;s capabilities |
| endpoint | [string](#string) |  |  |
| description | [string](#string) |  |  |
| version | [string](#string) |  |  |
| api_version | [string](#string) |  |  |
| persistent | [bool](#bool) |  |  |
| features | [Feature](#zms-propsim-v1-Feature) | repeated |  |
| parameters | [ParameterDefinition](#zms-propsim-v1-ParameterDefinition) | repeated |  |






<a name="zms-propsim-v1-StartJobRequest"></a>

### StartJobRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| job_id | [string](#string) |  |  |






<a name="zms-propsim-v1-StartJobResponse"></a>

### StartJobResponse







<a name="zms-propsim-v1-UrlData"></a>

### UrlData



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| url | [string](#string) |  |  |
| size | [uint64](#uint64) |  |  |
| basic | [HttpBasicAuth](#zms-propsim-v1-HttpBasicAuth) |  |  |
| header | [HttpHeaderAuth](#zms-propsim-v1-HttpHeaderAuth) |  |  |
| desc | [string](#string) | optional |  |





 


<a name="zms-propsim-v1-JobEvent"></a>

### JobEvent


| Name | Number | Description |
| ---- | ------ | ----------- |
| JOB_EVENT_STATUS | 0 |  |
| JOB_EVENT_PROGRESS | 1 |  |
| JOB_EVENT_OUTPUT | 2 |  |



<a name="zms-propsim-v1-JobStatus"></a>

### JobStatus


| Name | Number | Description |
| ---- | ------ | ----------- |
| JS_UNSPECIFIED | 0 |  |
| JS_CREATED | 1 |  |
| JS_SCHEDULED | 2 |  |
| JS_RUNNING | 3 |  |
| JS_COMPLETE | 4 |  |
| JS_FAILED | 5 |  |
| JS_DELETED | 6 |  |



<a name="zms-propsim-v1-MapKind"></a>

### MapKind


| Name | Number | Description |
| ---- | ------ | ----------- |
| MK_UNSPECIFIED | 0 |  |
| MK_OTHER | 1 |  |
| MK_PATHLOSS | 2 |  |
| MK_POWER | 3 |  |
| MK_LOS | 4 |  |



<a name="zms-propsim-v1-MapType"></a>

### MapType


| Name | Number | Description |
| ---- | ------ | ----------- |
| MT_UNSPECIFIED | 0 |  |
| MT_RAW | 1 |  |
| MT_GEOTIFF | 2 |  |
| MT_DEM | 3 |  |



<a name="zms-propsim-v1-MapUnit"></a>

### MapUnit


| Name | Number | Description |
| ---- | ------ | ----------- |
| MU_UNSPECIFIED | 0 |  |
| MU_OTHER | 1 |  |
| MU_dB | 2 |  |
| MU_dBm | 3 |  |
| MU_mW | 4 |  |
| MU_LOS | 5 |  |



<a name="zms-propsim-v1-ParameterType"></a>

### ParameterType


| Name | Number | Description |
| ---- | ------ | ----------- |
| PARAMETER_TYPE_BOOL | 0 |  |
| PARAMETER_TYPE_INT | 1 |  |
| PARAMETER_TYPE_DOUBLE | 2 |  |
| PARAMETER_TYPE_STRING | 3 |  |


 

 


<a name="zms-propsim-v1-JobService"></a>

### JobService
The Propagation Simulation (Propsim) service API for OpenZMS.  Provides a
general, parameterizable job run interface, and supports a variety of
output formats (e.g. `geotiff` maps, raw data series).  In OpenZMS, this
API is typically consumed by the DST (Digital Spectrum Twin) service to
run propagation simulations for specific radio ports in the Zone.

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| GetServiceDescriptor | [GetServiceDescriptorRequest](#zms-propsim-v1-GetServiceDescriptorRequest) | [GetServiceDescriptorResponse](#zms-propsim-v1-GetServiceDescriptorResponse) | Returns this service&#39;s descriptor: its features, parameters, and run constraints (e.g. parallel job limitations, its persistence/statefulness, etc). |
| CreateJob | [CreateJobRequest](#zms-propsim-v1-CreateJobRequest) | [CreateJobResponse](#zms-propsim-v1-CreateJobResponse) | Creates a Job in accordance with request parameters, or returns an error. Services must allow Jobs to be created, even if they subsequently refuse to Start them, which they may do temporarily if insufficient resources are available. |
| EstimateJob | [EstimateJobRequest](#zms-propsim-v1-EstimateJobRequest) | [EstimateJobResponse](#zms-propsim-v1-EstimateJobResponse) | Returns an estimate of job computation duration. (Optional.) |
| StartJob | [StartJobRequest](#zms-propsim-v1-StartJobRequest) | [StartJobResponse](#zms-propsim-v1-StartJobResponse) | Starts a Job. The service may refuse to Start a Job if it lacks resources. |
| PauseJob | [PauseJobRequest](#zms-propsim-v1-PauseJobRequest) | [PauseJobResponse](#zms-propsim-v1-PauseJobResponse) | Pauses execution of a Job (e.g., so that a higher-priority job may have a better chance of execution). Optional. |
| CancelJob | [CancelJobRequest](#zms-propsim-v1-CancelJobRequest) | [CancelJobResponse](#zms-propsim-v1-CancelJobResponse) | Cancels ongoing execution of a Job entirely. Optional. |
| DeleteJob | [DeleteJobRequest](#zms-propsim-v1-DeleteJobRequest) | [DeleteJobResponse](#zms-propsim-v1-DeleteJobResponse) | Deletes a Job from memory, and persistence storage, if persistent. |
| GetJobEvents | [GetJobEventsRequest](#zms-propsim-v1-GetJobEventsRequest) | [GetJobEventsResponse](#zms-propsim-v1-GetJobEventsResponse) stream | Returns a stream of JobEvents. If a client calls this on an incomplete job, the server must close the stream when the job becomes complete or failed. If a client calls this on a completed or failed job that is not deleted, we will close on deletion. |
| GetJobOutput | [GetJobOutputRequest](#zms-propsim-v1-GetJobOutputRequest) | [GetJobOutputResponse](#zms-propsim-v1-GetJobOutputResponse) | Returns a JobOutput. |

 



<a name="zms_propsim_v1_controller-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## zms/propsim/v1/controller.proto



<a name="zms-propsim-v1-RegisterJobServiceRequest"></a>

### RegisterJobServiceRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| desc | [ServiceDescriptor](#zms-propsim-v1-ServiceDescriptor) |  |  |






<a name="zms-propsim-v1-RegisterJobServiceResponse"></a>

### RegisterJobServiceResponse






 

 

 


<a name="zms-propsim-v1-PropSimController"></a>

### PropSimController
The caller of a propsim service implements this manager interface,
whether it is the DST, or a hierarchical propsim service itself.

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| RegisterJobService | [RegisterJobServiceRequest](#zms-propsim-v1-RegisterJobServiceRequest) | [RegisterJobServiceResponse](#zms-propsim-v1-RegisterJobServiceResponse) |  |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

