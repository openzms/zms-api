# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [zms/identity/v1/identity.proto](#zms_identity_v1_identity-proto)
    - [Element](#zms-identity-v1-Element)
    - [ElementAttribute](#zms-identity-v1-ElementAttribute)
    - [Event](#zms-identity-v1-Event)
    - [GetElementRequest](#zms-identity-v1-GetElementRequest)
    - [GetElementResponse](#zms-identity-v1-GetElementResponse)
    - [GetTokenRequest](#zms-identity-v1-GetTokenRequest)
    - [GetTokenResponse](#zms-identity-v1-GetTokenResponse)
    - [GetUserRequest](#zms-identity-v1-GetUserRequest)
    - [GetUserResponse](#zms-identity-v1-GetUserResponse)
    - [RegisterServiceRequest](#zms-identity-v1-RegisterServiceRequest)
    - [RegisterServiceResponse](#zms-identity-v1-RegisterServiceResponse)
    - [RequestHeader](#zms-identity-v1-RequestHeader)
    - [ResponseHeader](#zms-identity-v1-ResponseHeader)
    - [Role](#zms-identity-v1-Role)
    - [RoleBinding](#zms-identity-v1-RoleBinding)
    - [RoleListRequest](#zms-identity-v1-RoleListRequest)
    - [RoleListResponse](#zms-identity-v1-RoleListResponse)
    - [Service](#zms-identity-v1-Service)
    - [ServiceListRequest](#zms-identity-v1-ServiceListRequest)
    - [ServiceListResponse](#zms-identity-v1-ServiceListResponse)
    - [SubscribeRequest](#zms-identity-v1-SubscribeRequest)
    - [SubscribeResponse](#zms-identity-v1-SubscribeResponse)
    - [Token](#zms-identity-v1-Token)
    - [UnregisterServiceRequest](#zms-identity-v1-UnregisterServiceRequest)
    - [UnregisterServiceResponse](#zms-identity-v1-UnregisterServiceResponse)
    - [UpdateServiceRequest](#zms-identity-v1-UpdateServiceRequest)
    - [UpdateServiceResponse](#zms-identity-v1-UpdateServiceResponse)
    - [User](#zms-identity-v1-User)
    - [UserEmailAddress](#zms-identity-v1-UserEmailAddress)
    - [ValidateTokenRequest](#zms-identity-v1-ValidateTokenRequest)
    - [ValidateTokenResponse](#zms-identity-v1-ValidateTokenResponse)
  
    - [EventCode](#zms-identity-v1-EventCode)
  
    - [Identity](#zms-identity-v1-Identity)
  
- [Scalar Value Types](#scalar-value-types)



<a name="zms_identity_v1_identity-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## zms/identity/v1/identity.proto



<a name="zms-identity-v1-Element"></a>

### Element
Describes a ZMS `Element`.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | The element UUID. |
| creator_user_id | [string](#string) |  | The creator user ID. |
| name | [string](#string) |  | The element name. |
| html_url | [string](#string) |  | The element&#39;s HTML URL. |
| description | [string](#string) | optional | The element&#39;s description. |
| is_public | [bool](#bool) |  | Whether or not the Element is publicly viewable. |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | The creation time. |
| approved_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional | The approval time. |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional | The last update time. |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional | The deletion time. |
| enabled | [bool](#bool) |  | Whether or not the Element is enabled. |
| attributes | [ElementAttribute](#zms-identity-v1-ElementAttribute) | repeated | A list of per-Element attributes. |






<a name="zms-identity-v1-ElementAttribute"></a>

### ElementAttribute
Describes a ZMS `ElementAttribute`.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | The element attribute UUID. |
| element_id | [string](#string) |  | The associated element. |
| name | [string](#string) |  | The attribute name. |
| value | [string](#string) |  | The attribute value. |
| description | [string](#string) | optional | The attribute description. |






<a name="zms-identity-v1-Event"></a>

### Event
Describes an event produced by the identity service.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [zms.event.v1.EventHeader](#zms-event-v1-EventHeader) |  | The common, generic `zms.event.v1.EventHeader`. |
| user | [User](#zms-identity-v1-User) |  |  |
| element | [Element](#zms-identity-v1-Element) |  |  |
| role | [Role](#zms-identity-v1-Role) |  |  |
| role_binding | [RoleBinding](#zms-identity-v1-RoleBinding) |  |  |
| token | [Token](#zms-identity-v1-Token) |  |  |
| service | [Service](#zms-identity-v1-Service) |  |  |






<a name="zms-identity-v1-GetElementRequest"></a>

### GetElementRequest
The request body for the `GetElement` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-identity-v1-RequestHeader) |  | The generic request header. |
| id | [string](#string) |  | The element ID. |






<a name="zms-identity-v1-GetElementResponse"></a>

### GetElementResponse
The response body for the `GetElement` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-identity-v1-ResponseHeader) |  | The generic response header. |
| element | [Element](#zms-identity-v1-Element) |  | The requested Element object. |






<a name="zms-identity-v1-GetTokenRequest"></a>

### GetTokenRequest
The request body for the `GetToken` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-identity-v1-RequestHeader) |  | The generic request header. |
| token | [string](#string) |  | The token string; `base62` with appropriate ZMS prefix. |






<a name="zms-identity-v1-GetTokenResponse"></a>

### GetTokenResponse
The response body for the `GetToken` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-identity-v1-ResponseHeader) |  | The generic response header. |
| token | [Token](#zms-identity-v1-Token) |  | The requested Token object. |






<a name="zms-identity-v1-GetUserRequest"></a>

### GetUserRequest
The request body for the `GetUser` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-identity-v1-RequestHeader) |  | The generic request header. |
| id | [string](#string) |  | The user ID. |






<a name="zms-identity-v1-GetUserResponse"></a>

### GetUserResponse
The response body for the `GetUser` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-identity-v1-ResponseHeader) |  | The generic response header. |
| user | [User](#zms-identity-v1-User) |  | The requested User object. |






<a name="zms-identity-v1-RegisterServiceRequest"></a>

### RegisterServiceRequest
The request body for the `RegisterServiceRequest` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-identity-v1-RequestHeader) |  | The generic request header. |
| service | [Service](#zms-identity-v1-Service) |  | The `Service` object that describes the caller&#39;s ZMS service. |






<a name="zms-identity-v1-RegisterServiceResponse"></a>

### RegisterServiceResponse
The response body from the `RegisterService` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-identity-v1-ResponseHeader) |  | The generic response header. |
| service | [Service](#zms-identity-v1-Service) |  | The `Service` object, updated post-registration. |
| secret | [string](#string) |  | A secret that must be supplied when calling `UnregisterService`. |
| bootstrapElementId | [string](#string) |  | The id of the Element which bootstrapped this ZMS deployment. |
| bootstrapUserId | [string](#string) |  | The id of the User which bootstrapped this ZMS deployment. |






<a name="zms-identity-v1-RequestHeader"></a>

### RequestHeader
A generic request header.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| req_id | [string](#string) | optional | A request ID; will be returned in ResponseHeader if set. |
| elaborate | [bool](#bool) | optional | Whether or not to elaborate (to return parent-child relationships). |






<a name="zms-identity-v1-ResponseHeader"></a>

### ResponseHeader



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| req_id | [string](#string) | optional | A request ID; will be returned in ResponseHeader if set. |






<a name="zms-identity-v1-Role"></a>

### Role
Definition of a `Role` object.  A `Role` is a simple, named key-value
pair that corresponds to a permission.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | `Role` id. |
| name | [string](#string) |  | The name of the `Role`. |
| value | [int32](#int32) |  | The value of the `Role`. |
| description | [string](#string) |  | The description of the permission conveyed by the `Role`. |






<a name="zms-identity-v1-RoleBinding"></a>

### RoleBinding
Definition of a `RoleBinding` object.  A `RoleBinding` associates a
`User` to a `Role`, and may restrict the binding to a particular
`Element`.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | `RoleBinding` id. |
| role_id | [string](#string) |  | Associated `Role` id. |
| user_id | [string](#string) |  | Associated `User` id. |
| element_id | [string](#string) | optional | Associated `Element` id, if any. |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | Creation time. |
| approved_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional | Approval time; if none, unapproved. |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional | Deletion time; if set, no longer valid. |






<a name="zms-identity-v1-RoleListRequest"></a>

### RoleListRequest
The request body for the `RoleList` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-identity-v1-RequestHeader) |  | The generic request header. |






<a name="zms-identity-v1-RoleListResponse"></a>

### RoleListResponse
The response body for the `RoleList` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-identity-v1-ResponseHeader) |  | The generic response header. |
| roles | [Role](#zms-identity-v1-Role) | repeated | The current list of `Role` objects the server is configured with. |






<a name="zms-identity-v1-Service"></a>

### Service
Definition of a `Service` object.  A `Service` is a service that runs
within the ZMS and provides one or both RESTful HTTP (northbound) and
gRPC (southbound, internal) service endpoints.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | The `Service` id. |
| name | [string](#string) |  | The name of the service. |
| kind | [string](#string) |  | The service kind; for instance, `identity`. |
| endpoint | [string](#string) |  | The gRPC endpoint, if any. |
| endpoint_api_uri | [string](#string) |  | The RESTful HTTP endpoint, if any. |
| description | [string](#string) |  | A description of the service. |
| version | [string](#string) |  | The service&#39;s version. |
| api_version | [string](#string) |  | The service API version. |
| enabled | [bool](#bool) |  | True if the service is administratively enabled. |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | Creation time; set by server in `RegisterService`. |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional | Last updated time; set by server upon service update. |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional | Deletion time; set by server in `UnregisterService`. |
| heartbeat_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional | Last heartbeat time; set by server upon service heartbeat. |






<a name="zms-identity-v1-ServiceListRequest"></a>

### ServiceListRequest
The request body for the `ServiceList` method; empty.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-identity-v1-RequestHeader) |  | The generic request header. |






<a name="zms-identity-v1-ServiceListResponse"></a>

### ServiceListResponse
The response body from the `ServiceList` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-identity-v1-ResponseHeader) |  | The generic response header. |
| services | [Service](#zms-identity-v1-Service) | repeated | A list of `Service` objects connected to this identity service. |






<a name="zms-identity-v1-SubscribeRequest"></a>

### SubscribeRequest
The request body for the `Subscribe` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-identity-v1-RequestHeader) |  | The generic request header. |
| filters | [zms.event.v1.EventFilter](#zms-event-v1-EventFilter) | repeated | A list of `zms.event.v1.EventFilter`. |
| include | [bool](#bool) | optional | Whether or not to return the associated object in the Event response. Defaults to `true` if unset. |






<a name="zms-identity-v1-SubscribeResponse"></a>

### SubscribeResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-identity-v1-ResponseHeader) |  | The generic response header. |
| events | [Event](#zms-identity-v1-Event) | repeated | A list of matching events. |






<a name="zms-identity-v1-Token"></a>

### Token
Describes a ZMS token.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | The token UUID. |
| user_id | [string](#string) |  | The owner user UUID. |
| token | [string](#string) |  | The token string itself. |
| issued_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | The issue time. |
| expires_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional | The expiration time, if any. |
| role_bindings | [RoleBinding](#zms-identity-v1-RoleBinding) | repeated | A list of associated `RoleBinding` objects. |






<a name="zms-identity-v1-UnregisterServiceRequest"></a>

### UnregisterServiceRequest
The request body for the `UnregisterService` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-identity-v1-RequestHeader) |  | The generic request header. |
| id | [string](#string) |  | The id of the `Service` to be unregistered. |
| secret | [string](#string) |  | The secret the service was assigned after successful `ServiceRegister` invocation. |






<a name="zms-identity-v1-UnregisterServiceResponse"></a>

### UnregisterServiceResponse
The response body from the `UnregisterService` method; empty.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-identity-v1-ResponseHeader) |  | The generic response header. |






<a name="zms-identity-v1-UpdateServiceRequest"></a>

### UpdateServiceRequest
The response body from the `UpdateService` method; empty.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-identity-v1-RequestHeader) |  | The generic request header. |
| id | [string](#string) |  | The id of the `Service` to Update. |
| secret | [string](#string) |  | The secret the service was assigned after successful `RegisterService` invocation. |
| enabled | [bool](#bool) | optional | True if the service is administratively enabled. |






<a name="zms-identity-v1-UpdateServiceResponse"></a>

### UpdateServiceResponse
The response body from the `UpdateService` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-identity-v1-ResponseHeader) |  | The generic response header. |
| service | [Service](#zms-identity-v1-Service) |  | The requested Service object. |






<a name="zms-identity-v1-User"></a>

### User
Describes a ZMS `User`.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | The user UUID. |
| name | [string](#string) |  | The username. |
| given_name | [string](#string) | optional | The user&#39;s given name. |
| family_name | [string](#string) | optional | The user&#39;s family name. |
| full_name | [string](#string) | optional | The user&#39;s full name. |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  | The creation time. |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional | The last update time. |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional | The deletion time. |
| enabled | [bool](#bool) |  | Whether or not the User account is enabled. |
| primary_email_address_id | [string](#string) |  | The User&#39;s primary EmailAddress id. |
| primary_email_address | [UserEmailAddress](#zms-identity-v1-UserEmailAddress) | optional | The User&#39;s primary EmailAddress (if elaborate requested). |
| email_addresses | [UserEmailAddress](#zms-identity-v1-UserEmailAddress) | repeated | A list of all User EmailAddresses (if elaborate requested). |
| role_bindings | [RoleBinding](#zms-identity-v1-RoleBinding) | repeated | A list of the User&#39;s RoleBindings (if elaborate requested). |






<a name="zms-identity-v1-UserEmailAddress"></a>

### UserEmailAddress
Describes a ZMS user email address.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  | The user email address UUID. |
| user_id | [string](#string) |  | The user ID. |
| email_address | [string](#string) |  | The user email address. |






<a name="zms-identity-v1-ValidateTokenRequest"></a>

### ValidateTokenRequest
The request body for the `ValidateToken` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-identity-v1-RequestHeader) |  | The generic request header. |
| token | [string](#string) |  | The token string; `base62` with appropriate ZMS prefix. |






<a name="zms-identity-v1-ValidateTokenResponse"></a>

### ValidateTokenResponse
The response body for the `ValidateToken` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-identity-v1-ResponseHeader) |  | The generic response header. |





 


<a name="zms-identity-v1-EventCode"></a>

### EventCode
Event codes that correspond to identity service objects.

| Name | Number | Description |
| ---- | ------ | ----------- |
| EC_UNSPECIFIED | 0 |  |
| EC_USER | 1001 |  |
| EC_ELEMENT | 1002 |  |
| EC_ROLE | 1003 |  |
| EC_ROLEBINDING | 1004 |  |
| EC_TOKEN | 1005 |  |
| EC_SERVICE | 1006 |  |


 

 


<a name="zms-identity-v1-Identity"></a>

### Identity
The service that other ZMS services rely upon for authentication,
authorization, tokens, roles, and service directory capabilities.

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| RegisterService | [RegisterServiceRequest](#zms-identity-v1-RegisterServiceRequest) | [RegisterServiceResponse](#zms-identity-v1-RegisterServiceResponse) | Registers another `Service` with an Identity service. |
| UnregisterService | [UnregisterServiceRequest](#zms-identity-v1-UnregisterServiceRequest) | [UnregisterServiceResponse](#zms-identity-v1-UnregisterServiceResponse) | Unregisters the calling `Service` from this Identity service. |
| UpdateService | [UpdateServiceRequest](#zms-identity-v1-UpdateServiceRequest) | [UpdateServiceResponse](#zms-identity-v1-UpdateServiceResponse) | Updates a registered service at the Identity service. If there are no service data or state changes (e.g., an empty UpdateServiceRequest with only the Id and Secret fields set), only the HeartbeatAt field is updated. |
| GetUser | [GetUserRequest](#zms-identity-v1-GetUserRequest) | [GetUserResponse](#zms-identity-v1-GetUserResponse) | Returns the requested `User` object. |
| GetElement | [GetElementRequest](#zms-identity-v1-GetElementRequest) | [GetElementResponse](#zms-identity-v1-GetElementResponse) | Returns the requested `Element` object. |
| GetToken | [GetTokenRequest](#zms-identity-v1-GetTokenRequest) | [GetTokenResponse](#zms-identity-v1-GetTokenResponse) | Returns metadata about the supplied `Token`: user uuid, issued_at/expires_at, and `RoleBinding` information. |
| ValidateToken | [ValidateTokenRequest](#zms-identity-v1-ValidateTokenRequest) | [ValidateTokenResponse](#zms-identity-v1-ValidateTokenResponse) | Validates the supplied `Token`; returns an error if invalid. |
| GetRoleList | [RoleListRequest](#zms-identity-v1-RoleListRequest) | [RoleListResponse](#zms-identity-v1-RoleListResponse) | Returns the service&#39;s configured list of `Role` objects. |
| GetServiceList | [ServiceListRequest](#zms-identity-v1-ServiceListRequest) | [ServiceListResponse](#zms-identity-v1-ServiceListResponse) | Returns the current list of `Service` objects in the server&#39;s directory. |
| Subscribe | [SubscribeRequest](#zms-identity-v1-SubscribeRequest) | [SubscribeResponse](#zms-identity-v1-SubscribeResponse) stream | Subscribe returns a stream of events from this service according to the list of filters provided in SubscribeRequest. The server will only close this stream when it shuts down; clients are responsible to detect the case where all objects allowed or referenced by the filter have been deleted or revoked, and therefore no events will be streamed in the future. |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

