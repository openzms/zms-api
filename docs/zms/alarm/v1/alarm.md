# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [zms/alarm/v1/alarm.proto](#zms_alarm_v1_alarm-proto)
    - [ConstraintChange](#zms-alarm-v1-ConstraintChange)
    - [Event](#zms-alarm-v1-Event)
    - [GrantChange](#zms-alarm-v1-GrantChange)
    - [RequestHeader](#zms-alarm-v1-RequestHeader)
    - [ResponseHeader](#zms-alarm-v1-ResponseHeader)
    - [SubscribeRequest](#zms-alarm-v1-SubscribeRequest)
    - [SubscribeResponse](#zms-alarm-v1-SubscribeResponse)
  
    - [EventCode](#zms-alarm-v1-EventCode)
  
    - [Alarm](#zms-alarm-v1-Alarm)
  
- [Scalar Value Types](#scalar-value-types)



<a name="zms_alarm_v1_alarm-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## zms/alarm/v1/alarm.proto



<a name="zms-alarm-v1-ConstraintChange"></a>

### ConstraintChange
Describes a recommended Constraint change.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| grant_id | [string](#string) |  |  |
| constraint_id | [string](#string) |  |  |
| observation_id | [string](#string) | optional |  |
| min_freq | [int64](#int64) | optional |  |
| max_freq | [int64](#int64) | optional |  |
| bandwidth | [int64](#int64) | optional |  |
| max_eirp | [double](#double) | optional |  |
| min_eirp | [double](#double) | optional |  |
| description | [string](#string) | optional |  |
| creator_id | [string](#string) | optional |  |
| updater_id | [string](#string) | optional |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |






<a name="zms-alarm-v1-Event"></a>

### Event
Describes an event produced by the alarm service.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [zms.event.v1.EventHeader](#zms-event-v1-EventHeader) |  | The common, generic `zms.event.v1.EventHeader`. |
| constraint_change | [ConstraintChange](#zms-alarm-v1-ConstraintChange) |  |  |
| grant_change | [GrantChange](#zms-alarm-v1-GrantChange) |  |  |






<a name="zms-alarm-v1-GrantChange"></a>

### GrantChange
Describes a recommended Grant change.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| grant_id | [string](#string) |  |  |
| observation_id | [string](#string) | optional |  |
| action | [string](#string) | optional |  |
| description | [string](#string) | optional |  |
| creator_id | [string](#string) | optional |  |
| updater_id | [string](#string) | optional |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |






<a name="zms-alarm-v1-RequestHeader"></a>

### RequestHeader
A generic request header.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| req_id | [string](#string) | optional | A request ID; will be returned in ResponseHeader if set. |
| elaborate | [bool](#bool) | optional | Whether or not to elaborate (to return parent-child relationships). |






<a name="zms-alarm-v1-ResponseHeader"></a>

### ResponseHeader
A generic response header.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| req_id | [string](#string) | optional | A request ID; will be returned in ResponseHeader if set. |






<a name="zms-alarm-v1-SubscribeRequest"></a>

### SubscribeRequest
The request body for the `Subscribe` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-alarm-v1-RequestHeader) |  | The generic request header. |
| filters | [zms.event.v1.EventFilter](#zms-event-v1-EventFilter) | repeated | A list of `zms.event.v1.EventFilter`. |
| include | [bool](#bool) | optional | Whether or not to return the associated object in the Event response. Defaults to `true` if unset. |






<a name="zms-alarm-v1-SubscribeResponse"></a>

### SubscribeResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-alarm-v1-ResponseHeader) |  | The generic response header. |
| events | [Event](#zms-alarm-v1-Event) | repeated | A list of matching events. |





 


<a name="zms-alarm-v1-EventCode"></a>

### EventCode
Event codes that correspond to alarm service objects.

| Name | Number | Description |
| ---- | ------ | ----------- |
| EC_UNSPECIFIED | 0 |  |
| EC_CONSTRAINTCHANGE | 4001 |  |
| EC_GRANTCHANGE | 4002 |  |


 

 


<a name="zms-alarm-v1-Alarm"></a>

### Alarm
The Alarm service for a ZMS.  Processes Observations, Violations, and
Interference events, and recommends corrective actions to the zmc
service&#39;s Grants.

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| Subscribe | [SubscribeRequest](#zms-alarm-v1-SubscribeRequest) | [SubscribeResponse](#zms-alarm-v1-SubscribeResponse) stream | Subscribe returns a stream of events from this service according to the list of filters provided in SubscribeRequest. The server will only close this stream when it shuts down; clients are responsible to detect the case where all objects allowed or referenced by the filter have been deleted or revoked, and therefore no events will be streamed in the future. |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

