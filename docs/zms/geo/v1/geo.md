# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [zms/geo/v1/geo.proto](#zms_geo_v1_geo-proto)
    - [Area](#zms-geo-v1-Area)
    - [AreaPoint](#zms-geo-v1-AreaPoint)
    - [Point](#zms-geo-v1-Point)
  
- [Scalar Value Types](#scalar-value-types)



<a name="zms_geo_v1_geo-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## zms/geo/v1/geo.proto



<a name="zms-geo-v1-Area"></a>

### Area
Describes a geographic area as a list of AreaPoints with metadata.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| element_id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| description | [string](#string) | optional |  |
| srid | [int32](#int32) |  |  |
| points | [AreaPoint](#zms-geo-v1-AreaPoint) | repeated |  |






<a name="zms-geo-v1-AreaPoint"></a>

### AreaPoint
Describes a Point that is part of an Area.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| area_id | [string](#string) |  |  |
| x | [double](#double) |  |  |
| y | [double](#double) |  |  |
| z | [double](#double) | optional |  |






<a name="zms-geo-v1-Point"></a>

### Point
Describes a generic Point.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| srid | [int32](#int32) |  |  |
| x | [double](#double) |  |  |
| y | [double](#double) |  |  |
| z | [double](#double) | optional |  |





 

 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

