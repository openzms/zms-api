# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [zms/zmc/v1/zmc.proto](#zms_zmc_v1_zmc-proto)
    - [Antenna](#zms-zmc-v1-Antenna)
    - [Claim](#zms-zmc-v1-Claim)
    - [Constraint](#zms-zmc-v1-Constraint)
    - [Event](#zms-zmc-v1-Event)
    - [GetAntennaRequest](#zms-zmc-v1-GetAntennaRequest)
    - [GetAntennaResponse](#zms-zmc-v1-GetAntennaResponse)
    - [GetGrantRequest](#zms-zmc-v1-GetGrantRequest)
    - [GetGrantResponse](#zms-zmc-v1-GetGrantResponse)
    - [GetGrantsRequest](#zms-zmc-v1-GetGrantsRequest)
    - [GetGrantsResponse](#zms-zmc-v1-GetGrantsResponse)
    - [GetLocationRequest](#zms-zmc-v1-GetLocationRequest)
    - [GetLocationResponse](#zms-zmc-v1-GetLocationResponse)
    - [GetMonitorRequest](#zms-zmc-v1-GetMonitorRequest)
    - [GetMonitorResponse](#zms-zmc-v1-GetMonitorResponse)
    - [GetMonitorsRequest](#zms-zmc-v1-GetMonitorsRequest)
    - [GetMonitorsResponse](#zms-zmc-v1-GetMonitorsResponse)
    - [GetRadioPortRequest](#zms-zmc-v1-GetRadioPortRequest)
    - [GetRadioPortResponse](#zms-zmc-v1-GetRadioPortResponse)
    - [GetRadioRequest](#zms-zmc-v1-GetRadioRequest)
    - [GetRadioResponse](#zms-zmc-v1-GetRadioResponse)
    - [GetSpectrumRequest](#zms-zmc-v1-GetSpectrumRequest)
    - [GetSpectrumResponse](#zms-zmc-v1-GetSpectrumResponse)
    - [GetZoneRequest](#zms-zmc-v1-GetZoneRequest)
    - [GetZoneResponse](#zms-zmc-v1-GetZoneResponse)
    - [Grant](#zms-zmc-v1-Grant)
    - [GrantLog](#zms-zmc-v1-GrantLog)
    - [GrantReplacement](#zms-zmc-v1-GrantReplacement)
    - [IntConstraint](#zms-zmc-v1-IntConstraint)
    - [Location](#zms-zmc-v1-Location)
    - [Monitor](#zms-zmc-v1-Monitor)
    - [Policy](#zms-zmc-v1-Policy)
    - [Radio](#zms-zmc-v1-Radio)
    - [RadioPort](#zms-zmc-v1-RadioPort)
    - [RequestHeader](#zms-zmc-v1-RequestHeader)
    - [ResponseHeader](#zms-zmc-v1-ResponseHeader)
    - [RtIntConstraint](#zms-zmc-v1-RtIntConstraint)
    - [Spectrum](#zms-zmc-v1-Spectrum)
    - [SubscribeRequest](#zms-zmc-v1-SubscribeRequest)
    - [SubscribeResponse](#zms-zmc-v1-SubscribeResponse)
    - [Zone](#zms-zmc-v1-Zone)
  
    - [EventCode](#zms-zmc-v1-EventCode)
  
    - [Zmc](#zms-zmc-v1-Zmc)
  
- [Scalar Value Types](#scalar-value-types)



<a name="zms_zmc_v1_zmc-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## zms/zmc/v1/zmc.proto



<a name="zms-zmc-v1-Antenna"></a>

### Antenna
Describes an antenna.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| element_id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |
| type | [string](#string) |  |  |
| vendor | [string](#string) |  |  |
| model | [string](#string) |  |  |
| url | [string](#string) |  |  |
| gain | [float](#float) |  |  |
| beam_width | [int32](#int32) |  |  |
| gain_profile_data | [bytes](#bytes) | optional |  |
| gain_profile_format | [string](#string) | optional |  |
| gain_profile_url | [string](#string) | optional |  |
| creator_id | [string](#string) |  |  |
| updater_id | [string](#string) | optional |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |






<a name="zms-zmc-v1-Claim"></a>

### Claim
Describes a claim.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| element_id | [string](#string) |  |  |
| ext_id | [string](#string) |  |  |
| type | [string](#string) |  |  |
| source | [string](#string) |  |  |
| html_url | [string](#string) | optional |  |
| name | [string](#string) |  |  |
| description | [string](#string) | optional |  |
| creator_id | [string](#string) |  |  |
| updater_id | [string](#string) | optional |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| verified_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| denied_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| grant_id | [string](#string) | optional |  |
| grant | [Grant](#zms-zmc-v1-Grant) | optional |  |






<a name="zms-zmc-v1-Constraint"></a>

### Constraint
Describes a grant or spectrum constraint.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| min_freq | [int64](#int64) |  |  |
| max_freq | [int64](#int64) |  |  |
| bandwidth | [int64](#int64) |  |  |
| max_eirp | [double](#double) |  |  |
| min_eirp | [double](#double) | optional |  |
| exclusive | [bool](#bool) |  |  |
| area_id | [string](#string) | optional |  |
| area | [zms.geo.v1.Area](#zms-geo-v1-Area) | optional |  |






<a name="zms-zmc-v1-Event"></a>

### Event
Describes an event produced by the zmc service.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [zms.event.v1.EventHeader](#zms-event-v1-EventHeader) |  | The common, generic `zms.event.v1.EventHeader`. |
| zone | [Zone](#zms-zmc-v1-Zone) |  |  |
| antenna | [Antenna](#zms-zmc-v1-Antenna) |  |  |
| radio | [Radio](#zms-zmc-v1-Radio) |  |  |
| radio_port | [RadioPort](#zms-zmc-v1-RadioPort) |  |  |
| monitor | [Monitor](#zms-zmc-v1-Monitor) |  |  |
| grant | [Grant](#zms-zmc-v1-Grant) |  |  |
| spectrum | [Spectrum](#zms-zmc-v1-Spectrum) |  |  |
| claim | [Claim](#zms-zmc-v1-Claim) |  |  |






<a name="zms-zmc-v1-GetAntennaRequest"></a>

### GetAntennaRequest
The request body for the `GetAntenna` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-zmc-v1-RequestHeader) |  | The generic request header. |
| id | [string](#string) |  | The antenna ID. |






<a name="zms-zmc-v1-GetAntennaResponse"></a>

### GetAntennaResponse
The response body for the `GetAntenna` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-zmc-v1-ResponseHeader) |  | The generic response header. |
| antenna | [Antenna](#zms-zmc-v1-Antenna) |  | The requested Antenna. |






<a name="zms-zmc-v1-GetGrantRequest"></a>

### GetGrantRequest
The request body for the `GetGrant` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-zmc-v1-RequestHeader) |  | The generic request header. |
| id | [string](#string) |  | The grant ID. |






<a name="zms-zmc-v1-GetGrantResponse"></a>

### GetGrantResponse
The response body for the `GetGrant` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-zmc-v1-ResponseHeader) |  | The generic response header. |
| grant | [Grant](#zms-zmc-v1-Grant) |  | The requested Grant. |






<a name="zms-zmc-v1-GetGrantsRequest"></a>

### GetGrantsRequest
The request body for the `GetGrants` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-zmc-v1-RequestHeader) |  | The generic request header. |
| element_id | [string](#string) | optional | Filters the returned list of grants by this element ID. |
| radio_port_id | [string](#string) | optional | Filters the returned list of grants by this radio port ID. |
| creator_id | [string](#string) | optional | Filter by creator user ID. |
| time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional | Filter by time. If only `Time` is specified, this will return grants where `Time` is within starts_at and expires_at. If both `Time` and `TimeMax` are specified, it will return grants that overlap with this range. If `Time` is unset, only grants that are &#34;live&#34; (started and not expired) will be returned. |
| time_max | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| freq | [int64](#int64) | optional | Filters the returned list of grants by frequency. |
| exclusive | [bool](#bool) | optional | Filters the returned list of grants by their exclusivity. |
| approved | [bool](#bool) | optional | Filters the returned list of grants by approved. |
| owner_approved | [bool](#bool) | optional | Filters the returned list of grants by owner-approved. |
| denied | [bool](#bool) | optional | Filters the returned list of grants by denied. |
| revoked | [bool](#bool) | optional | Filters the returned list of grants by revoked. |
| deleted | [bool](#bool) | optional | Filters the returned list of grants by deleted. |
| spectrum_id | [string](#string) | optional | Filters the returned list of grants by associated spectrum id. |
| status | [string](#string) | optional | Filters the returned list of grants by status. |
| op_status | [string](#string) | optional | Filters the returned list of grants by op_status. |






<a name="zms-zmc-v1-GetGrantsResponse"></a>

### GetGrantsResponse
The response body for the `GetGrants` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-zmc-v1-ResponseHeader) |  | The generic response header. |
| grants | [Grant](#zms-zmc-v1-Grant) | repeated | The matching Grant list. |






<a name="zms-zmc-v1-GetLocationRequest"></a>

### GetLocationRequest
The request body for the `GetLocation` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-zmc-v1-RequestHeader) |  | The generic request header. |
| id | [string](#string) |  | The location ID. |






<a name="zms-zmc-v1-GetLocationResponse"></a>

### GetLocationResponse
The response body for the `GetLocation` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-zmc-v1-ResponseHeader) |  | The generic response header. |
| location | [Location](#zms-zmc-v1-Location) |  | The requested Location object. |






<a name="zms-zmc-v1-GetMonitorRequest"></a>

### GetMonitorRequest
The request body for the `GetMonitor` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-zmc-v1-RequestHeader) |  | The generic request header. |
| id | [string](#string) |  | The monitor ID. |






<a name="zms-zmc-v1-GetMonitorResponse"></a>

### GetMonitorResponse
The response body for the `GetMonitor` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-zmc-v1-ResponseHeader) |  | The generic response header. |
| monitor | [Monitor](#zms-zmc-v1-Monitor) |  | The requested Monitor. |






<a name="zms-zmc-v1-GetMonitorsRequest"></a>

### GetMonitorsRequest
The request body for the `GetMonitors` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-zmc-v1-RequestHeader) |  | The generic request header. |
| element_id | [string](#string) | optional | Filters the returned list of monitors by this element ID. |
| radio_port_id | [string](#string) | optional | Filters the returned list of monitors by this radio port ID. |
| monitor | [string](#string) | optional | Filters the returned list of monitors by this monitor name. |
| types | [string](#string) | optional | Filters the returned list of monitors by these monitor types. |
| formats | [string](#string) | optional | Filters the returned list of monitors by these monitor data formats. |
| exclusive | [bool](#bool) | optional | Filters the returned list of monitors by their exclusivity. |
| enabled | [bool](#bool) | optional | Filters the returned list of monitors to include only enabled monitors. |






<a name="zms-zmc-v1-GetMonitorsResponse"></a>

### GetMonitorsResponse
The response body for the `GetMonitors` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-zmc-v1-ResponseHeader) |  | The generic response header. |
| monitors | [Monitor](#zms-zmc-v1-Monitor) | repeated | The matching Monitor list. |






<a name="zms-zmc-v1-GetRadioPortRequest"></a>

### GetRadioPortRequest
The request body for the `GetRadioPort` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-zmc-v1-RequestHeader) |  | The generic request header. |
| id | [string](#string) |  | The radio port ID. |






<a name="zms-zmc-v1-GetRadioPortResponse"></a>

### GetRadioPortResponse
The response body for the `GetRadioPort` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-zmc-v1-ResponseHeader) |  | The generic response header. |
| radio_port | [RadioPort](#zms-zmc-v1-RadioPort) |  | The requested RadioPort. |






<a name="zms-zmc-v1-GetRadioRequest"></a>

### GetRadioRequest
The request body for the `GetRadio` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-zmc-v1-RequestHeader) |  | The generic request header. |
| id | [string](#string) |  | The radio ID. |






<a name="zms-zmc-v1-GetRadioResponse"></a>

### GetRadioResponse
The response body for the `GetRadio` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-zmc-v1-ResponseHeader) |  | The generic response header. |
| radio | [Radio](#zms-zmc-v1-Radio) |  | The requested Radio. |






<a name="zms-zmc-v1-GetSpectrumRequest"></a>

### GetSpectrumRequest
The request body for the `GetSpectrum` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-zmc-v1-RequestHeader) |  | The generic request header. |
| id | [string](#string) |  | The spectrum ID. |






<a name="zms-zmc-v1-GetSpectrumResponse"></a>

### GetSpectrumResponse
The response body for the `GetSpectrum` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-zmc-v1-ResponseHeader) |  | The generic response header. |
| spectrum | [Spectrum](#zms-zmc-v1-Spectrum) |  | The requested Spectrum. |






<a name="zms-zmc-v1-GetZoneRequest"></a>

### GetZoneRequest
The request body for the `GetZone` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-zmc-v1-RequestHeader) |  | The generic request header. |
| id | [string](#string) |  | The zone ID. |






<a name="zms-zmc-v1-GetZoneResponse"></a>

### GetZoneResponse
The response body for the `GetZone` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-zmc-v1-ResponseHeader) |  | The generic response header. |
| zone | [Zone](#zms-zmc-v1-Zone) |  | The requested Zone object. |






<a name="zms-zmc-v1-Grant"></a>

### Grant
Describes a grant.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| element_id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |
| priority | [int32](#int32) |  |  |
| spectrum_id | [string](#string) | optional |  |
| starts_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| expires_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| status | [string](#string) |  |  |
| status_ack_by | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| op_status | [string](#string) |  |  |
| op_status_updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| creator_id | [string](#string) |  |  |
| updater_id | [string](#string) | optional |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| approved_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| owner_approved_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| denied_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| revoked_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| constraints | [Constraint](#zms-zmc-v1-Constraint) | repeated |  |
| int_constraints | [IntConstraint](#zms-zmc-v1-IntConstraint) | repeated |  |
| rt_int_constraints | [RtIntConstraint](#zms-zmc-v1-RtIntConstraint) | repeated |  |
| radio_ports | [RadioPort](#zms-zmc-v1-RadioPort) | repeated |  |
| logs | [GrantLog](#zms-zmc-v1-GrantLog) | repeated |  |
| replacement | [GrantReplacement](#zms-zmc-v1-GrantReplacement) | optional |  |






<a name="zms-zmc-v1-GrantLog"></a>

### GrantLog
Describes a grant log entry (status/opstatus change &#43; message).


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| grant_id | [string](#string) |  |  |
| status | [string](#string) |  |  |
| op_status | [string](#string) |  |  |
| message | [string](#string) |  |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |






<a name="zms-zmc-v1-GrantReplacement"></a>

### GrantReplacement
Describes a grant replacement.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| grant_id | [string](#string) |  |  |
| new_grant_id | [string](#string) |  |  |
| description | [string](#string) |  |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| constraint_change_id | [string](#string) | optional |  |
| observation_id | [string](#string) | optional |  |






<a name="zms-zmc-v1-IntConstraint"></a>

### IntConstraint
Describes an interference constraint.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| max_power | [double](#double) | optional |  |
| area_id | [string](#string) | optional |  |
| area | [zms.geo.v1.Area](#zms-geo-v1-Area) | optional |  |
| description | [string](#string) | optional |  |






<a name="zms-zmc-v1-Location"></a>

### Location
Describes a location.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| element_id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| srid | [int32](#int32) |  |  |
| x | [double](#double) |  |  |
| y | [double](#double) |  |  |
| z | [double](#double) | optional |  |
| is_public | [bool](#bool) |  |  |
| creator_id | [string](#string) |  |  |
| updater_id | [string](#string) | optional |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |






<a name="zms-zmc-v1-Monitor"></a>

### Monitor
Describes a monitor associated with a radio port, possibly monitoring
another radio port.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| radio_port_id | [string](#string) |  |  |
| monitored_radio_port_id | [string](#string) | optional |  |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |
| types | [string](#string) |  |  |
| formats | [string](#string) |  |  |
| config | [string](#string) |  |  |
| exclusive | [bool](#bool) |  |  |
| enabled | [bool](#bool) |  |  |
| creator_id | [string](#string) |  |  |
| updater_id | [string](#string) | optional |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| radio_port | [RadioPort](#zms-zmc-v1-RadioPort) | optional |  |
| monitored_radio_port | [RadioPort](#zms-zmc-v1-RadioPort) | optional |  |






<a name="zms-zmc-v1-Policy"></a>

### Policy
Describes a spectrum policy.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| spectrum_id | [string](#string) |  |  |
| element_id | [string](#string) | optional |  |
| allowed | [bool](#bool) |  |  |
| auto_approve | [bool](#bool) |  |  |
| url | [string](#string) |  |  |
| priority | [int32](#int32) |  |  |
| max_duration | [int32](#int32) |  |  |
| when_unoccupied | [bool](#bool) |  |  |
| disable_emit_check | [bool](#bool) |  |  |
| allow_inactive | [bool](#bool) |  |  |
| allow_conflicts | [bool](#bool) |  |  |






<a name="zms-zmc-v1-Radio"></a>

### Radio
Describes a radio.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| element_id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |
| fcc_id | [string](#string) |  |  |
| device_id | [string](#string) |  |  |
| serial_number | [string](#string) |  |  |
| location_id | [string](#string) | optional |  |
| location | [Location](#zms-zmc-v1-Location) | optional |  |
| enabled | [bool](#bool) |  |  |
| creator_id | [string](#string) |  |  |
| updater_id | [string](#string) | optional |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| ports | [RadioPort](#zms-zmc-v1-RadioPort) | repeated |  |






<a name="zms-zmc-v1-RadioPort"></a>

### RadioPort
Describes a radio port.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| radio_id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| tx | [bool](#bool) |  |  |
| rx | [bool](#bool) |  |  |
| min_freq | [int64](#int64) |  |  |
| max_freq | [int64](#int64) |  |  |
| max_power | [double](#double) |  |  |
| antenna_id | [string](#string) | optional |  |
| antenna_location_id | [string](#string) | optional |  |
| antenna_azimuth_angle | [float](#float) | optional |  |
| antenna_elevation_angle | [float](#float) | optional |  |
| enabled | [bool](#bool) |  |  |
| creator_id | [string](#string) |  |  |
| updater_id | [string](#string) | optional |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| antenna | [Antenna](#zms-zmc-v1-Antenna) | optional |  |
| antenna_location | [Location](#zms-zmc-v1-Location) | optional |  |
| antenna_elevation_delta | [float](#float) | optional |  |






<a name="zms-zmc-v1-RequestHeader"></a>

### RequestHeader
A generic request header.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| req_id | [string](#string) | optional | A request ID; will be returned in ResponseHeader if set. |
| elaborate | [bool](#bool) | optional | Whether or not to elaborate (to return parent-child relationships). |






<a name="zms-zmc-v1-ResponseHeader"></a>

### ResponseHeader
A generic response header.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| req_id | [string](#string) | optional | A request ID; will be returned in ResponseHeader if set. |






<a name="zms-zmc-v1-RtIntConstraint"></a>

### RtIntConstraint
Describes a runtime interference constraint.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| metric | [string](#string) |  |  |
| comparator | [string](#string) |  |  |
| value | [double](#double) |  |  |
| aggregator | [string](#string) | optional |  |
| period | [int32](#int32) | optional |  |
| description | [string](#string) | optional |  |
| criticality | [int32](#int32) | optional |  |






<a name="zms-zmc-v1-Spectrum"></a>

### Spectrum
Describes a spectrum range.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| element_id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |
| url | [string](#string) |  |  |
| enabled | [bool](#bool) |  |  |
| creator_id | [string](#string) |  |  |
| updater_id | [string](#string) | optional |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| approved_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| denied_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| starts_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| expires_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| constraints | [Constraint](#zms-zmc-v1-Constraint) | repeated |  |
| policies | [Policy](#zms-zmc-v1-Policy) | repeated |  |






<a name="zms-zmc-v1-SubscribeRequest"></a>

### SubscribeRequest
The request body for the `Subscribe` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-zmc-v1-RequestHeader) |  | The generic request header. |
| filters | [zms.event.v1.EventFilter](#zms-event-v1-EventFilter) | repeated | A list of `zms.event.v1.EventFilter`. |
| include | [bool](#bool) | optional | Whether or not to return the associated object in the Event response. Defaults to `true` if unset. |






<a name="zms-zmc-v1-SubscribeResponse"></a>

### SubscribeResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-zmc-v1-ResponseHeader) |  | The generic response header. |
| events | [Event](#zms-zmc-v1-Event) | repeated | A list of matching events. |






<a name="zms-zmc-v1-Zone"></a>

### Zone
Describes a zone.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| element_id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |
| area_id | [string](#string) | optional |  |
| creator_id | [string](#string) |  |  |
| updater_id | [string](#string) | optional |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| area | [zms.geo.v1.Area](#zms-geo-v1-Area) | optional |  |





 


<a name="zms-zmc-v1-EventCode"></a>

### EventCode
Event codes that correspond to zmc service objects.

| Name | Number | Description |
| ---- | ------ | ----------- |
| EC_UNSPECIFIED | 0 |  |
| EC_ZONE | 2001 |  |
| EC_ANTENNA | 2002 |  |
| EC_RADIO | 2003 |  |
| EC_RADIOPORT | 2004 |  |
| EC_MONITOR | 2005 |  |
| EC_GRANT | 2006 |  |
| EC_SPECTRUM | 2007 |  |
| EC_CLAIM | 2008 |  |


 

 


<a name="zms-zmc-v1-Zmc"></a>

### Zmc
The spectrum and radio service for a ZMS.  Acts as an information base
and provides scheduling support for spectrum and monitors.

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| GetZone | [GetZoneRequest](#zms-zmc-v1-GetZoneRequest) | [GetZoneResponse](#zms-zmc-v1-GetZoneResponse) | Returns the requested `Zone` object. If the requested zone id is the 0 uuid, the server must return the bootstrap Zone. |
| GetLocation | [GetLocationRequest](#zms-zmc-v1-GetLocationRequest) | [GetLocationResponse](#zms-zmc-v1-GetLocationResponse) | Returns the requested `Location` object. |
| GetAntenna | [GetAntennaRequest](#zms-zmc-v1-GetAntennaRequest) | [GetAntennaResponse](#zms-zmc-v1-GetAntennaResponse) | Returns the requested `Antenna` object. |
| GetRadioPort | [GetRadioPortRequest](#zms-zmc-v1-GetRadioPortRequest) | [GetRadioPortResponse](#zms-zmc-v1-GetRadioPortResponse) | Returns the requested `RadioPort` object. |
| GetRadio | [GetRadioRequest](#zms-zmc-v1-GetRadioRequest) | [GetRadioResponse](#zms-zmc-v1-GetRadioResponse) | Returns the requested `Radio` object. |
| GetMonitor | [GetMonitorRequest](#zms-zmc-v1-GetMonitorRequest) | [GetMonitorResponse](#zms-zmc-v1-GetMonitorResponse) | Returns the requested `Monitor` object. |
| GetMonitors | [GetMonitorsRequest](#zms-zmc-v1-GetMonitorsRequest) | [GetMonitorsResponse](#zms-zmc-v1-GetMonitorsResponse) | Returns a filtered list of requested `Monitor` objects. |
| GetGrant | [GetGrantRequest](#zms-zmc-v1-GetGrantRequest) | [GetGrantResponse](#zms-zmc-v1-GetGrantResponse) | Returns the requred `Grant` object. |
| GetGrants | [GetGrantsRequest](#zms-zmc-v1-GetGrantsRequest) | [GetGrantsResponse](#zms-zmc-v1-GetGrantsResponse) | Returns a filtered list of requested `Grant` objects. |
| GetSpectrum | [GetSpectrumRequest](#zms-zmc-v1-GetSpectrumRequest) | [GetSpectrumResponse](#zms-zmc-v1-GetSpectrumResponse) | Returns the requred `Spectrum` object. |
| Subscribe | [SubscribeRequest](#zms-zmc-v1-SubscribeRequest) | [SubscribeResponse](#zms-zmc-v1-SubscribeResponse) stream | Subscribe returns a stream of events from this service according to the list of filters provided in SubscribeRequest. The server will only close this stream when it shuts down; clients are responsible to detect the case where all objects allowed or referenced by the filter have been deleted or revoked, and therefore no events will be streamed in the future. |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

