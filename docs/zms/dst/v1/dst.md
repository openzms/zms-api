# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [zms/dst/v1/dst.proto](#zms_dst_v1_dst-proto)
    - [Annotation](#zms-dst-v1-Annotation)
    - [CheckGrantConflictRequest](#zms-dst-v1-CheckGrantConflictRequest)
    - [CheckGrantConflictResponse](#zms-dst-v1-CheckGrantConflictResponse)
    - [CreatePropsimPowerGlobalRequest](#zms-dst-v1-CreatePropsimPowerGlobalRequest)
    - [CreatePropsimPowerGlobalResponse](#zms-dst-v1-CreatePropsimPowerGlobalResponse)
    - [CreatePropsimPowerGrantRequest](#zms-dst-v1-CreatePropsimPowerGrantRequest)
    - [CreatePropsimPowerGrantResponse](#zms-dst-v1-CreatePropsimPowerGrantResponse)
    - [Event](#zms-dst-v1-Event)
    - [FrequencyRange](#zms-dst-v1-FrequencyRange)
    - [GetExpectedLossRequest](#zms-dst-v1-GetExpectedLossRequest)
    - [GetExpectedLossResponse](#zms-dst-v1-GetExpectedLossResponse)
    - [GetExpectedPowerRequest](#zms-dst-v1-GetExpectedPowerRequest)
    - [GetExpectedPowerResponse](#zms-dst-v1-GetExpectedPowerResponse)
    - [GetObservationRequest](#zms-dst-v1-GetObservationRequest)
    - [GetObservationResponse](#zms-dst-v1-GetObservationResponse)
    - [GetObservationsRequest](#zms-dst-v1-GetObservationsRequest)
    - [GetObservationsResponse](#zms-dst-v1-GetObservationsResponse)
    - [GetTxConstraintsRequest](#zms-dst-v1-GetTxConstraintsRequest)
    - [GetTxConstraintsResponse](#zms-dst-v1-GetTxConstraintsResponse)
    - [GetUnoccupiedSpectrumRequest](#zms-dst-v1-GetUnoccupiedSpectrumRequest)
    - [GetUnoccupiedSpectrumResponse](#zms-dst-v1-GetUnoccupiedSpectrumResponse)
    - [Observation](#zms-dst-v1-Observation)
    - [PowerPerRadioPort](#zms-dst-v1-PowerPerRadioPort)
    - [PropsimPowerGlobal](#zms-dst-v1-PropsimPowerGlobal)
    - [PropsimPowerGrant](#zms-dst-v1-PropsimPowerGrant)
    - [PropsimPowerGrantJobOutput](#zms-dst-v1-PropsimPowerGrantJobOutput)
    - [RequestHeader](#zms-dst-v1-RequestHeader)
    - [ResponseHeader](#zms-dst-v1-ResponseHeader)
    - [SubscribeRequest](#zms-dst-v1-SubscribeRequest)
    - [SubscribeResponse](#zms-dst-v1-SubscribeResponse)
  
    - [EventCode](#zms-dst-v1-EventCode)
  
    - [Dst](#zms-dst-v1-Dst)
  
- [Scalar Value Types](#scalar-value-types)



<a name="zms_dst_v1_dst-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## zms/dst/v1/dst.proto



<a name="zms-dst-v1-Annotation"></a>

### Annotation



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| observation_id | [string](#string) |  |  |
| type | [string](#string) |  |  |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |
| value | [string](#string) |  |  |
| creator_id | [string](#string) |  |  |
| updater_id | [string](#string) | optional |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |






<a name="zms-dst-v1-CheckGrantConflictRequest"></a>

### CheckGrantConflictRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-dst-v1-RequestHeader) |  | The generic request header. |
| grant1_id | [string](#string) |  | The grant on which to calculate an aggregate power propsim. |
| grant2_id | [string](#string) |  | The grant on which to calculate an aggregate power propsim. |






<a name="zms-dst-v1-CheckGrantConflictResponse"></a>

### CheckGrantConflictResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-dst-v1-ResponseHeader) |  | The generic response header. |
| is_conflict | [bool](#bool) |  |  |






<a name="zms-dst-v1-CreatePropsimPowerGlobalRequest"></a>

### CreatePropsimPowerGlobalRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-dst-v1-RequestHeader) |  | The generic request header. |
| grant_ids | [string](#string) | repeated | The grant on which to calculate an aggregate power propsim. |






<a name="zms-dst-v1-CreatePropsimPowerGlobalResponse"></a>

### CreatePropsimPowerGlobalResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-dst-v1-ResponseHeader) |  | The generic response header. |
| propsim_power_global | [PropsimPowerGlobal](#zms-dst-v1-PropsimPowerGlobal) |  | The created PropsimPowerGlobal; may still be in progress. |






<a name="zms-dst-v1-CreatePropsimPowerGrantRequest"></a>

### CreatePropsimPowerGrantRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-dst-v1-RequestHeader) |  | The generic request header. |
| grant_id | [string](#string) |  | The grant on which to calculate an aggregate power propsim. |






<a name="zms-dst-v1-CreatePropsimPowerGrantResponse"></a>

### CreatePropsimPowerGrantResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-dst-v1-ResponseHeader) |  | The generic response header. |
| propsim_power_grant | [PropsimPowerGrant](#zms-dst-v1-PropsimPowerGrant) |  | The created PropsimPowerGrant; may still be in progress. |






<a name="zms-dst-v1-Event"></a>

### Event
Describes an event produced by the zmc service.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [zms.event.v1.EventHeader](#zms-event-v1-EventHeader) |  | The common, generic `zms.event.v1.EventHeader`. |
| observation | [Observation](#zms-dst-v1-Observation) |  |  |
| annotation | [Annotation](#zms-dst-v1-Annotation) |  |  |






<a name="zms-dst-v1-FrequencyRange"></a>

### FrequencyRange



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| min_freq | [int64](#int64) |  |  |
| max_freq | [int64](#int64) |  |  |






<a name="zms-dst-v1-GetExpectedLossRequest"></a>

### GetExpectedLossRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-dst-v1-RequestHeader) |  | The generic request header. |
| radio_port_id | [string](#string) |  |  |
| freq | [int64](#int64) |  |  |
| bandwidth | [int64](#int64) | optional |  |
| point | [zms.geo.v1.Point](#zms-geo-v1-Point) |  |  |






<a name="zms-dst-v1-GetExpectedLossResponse"></a>

### GetExpectedLossResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-dst-v1-ResponseHeader) |  | The generic response header. |
| loss | [double](#double) |  |  |






<a name="zms-dst-v1-GetExpectedPowerRequest"></a>

### GetExpectedPowerRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-dst-v1-RequestHeader) |  | The generic request header. |
| time | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional | If `time` is set, neither `grant_id` nor `radio_port_id` may be set (doing so returns an error). If `time` is set, we filter for live grants at that time, and compute expected power from all RadioPorts in those grants at the desired location. |
| grant_id | [string](#string) | optional | If `grant_id` is set, `time` may not be set (doing so returns an error). We compute expected power from all RadioPorts in that grant, optionally filtered by `radio_port_id` if that is set. The utility of setting only a single radio_port_id is to compute power based on the grant&#39;s operating characteristics, and not at the port&#39;s max tx power. (Given that if radio_port_id is not supplied, we return expected power from all ports in the grant, the utility of `radio_port_id` as a filter when `grant_id` is supplied is questionable, but it does minimize the number of rasters that must be paged in to query, when the caller needs exactly the per-grant result for a specific port.) |
| radio_port_id | [string](#string) | optional | If `radio_port_id` is set alone (`grant_id` and `time` unset), we simply compute expected power according to the radio port&#39;s max tx power. |
| freq | [int64](#int64) |  | The center frequency used to filter grants (frequency must be in grant&#39;s min/max frequency allocation), and to check propagation simulation. |
| bandwidth | [int64](#int64) | optional | The width of frequency slop to accept when checking a propagation simulation, since we might not want to have to generate a simulation for every center frequency. For instance, freq=3.5GHz and bandwidth=100MHz, accept any simulation run at frequencies between 3.4GHz and 3.6GHz. If unset, the system will determine a reasonable default. If set to 0, no slop will be allowed, and there must be a simulation that matches exactly the frequency requested. |
| point | [zms.geo.v1.Point](#zms-geo-v1-Point) |  |  |






<a name="zms-dst-v1-GetExpectedPowerResponse"></a>

### GetExpectedPowerResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-dst-v1-ResponseHeader) |  | The generic response header. |
| power | [double](#double) |  | The max expected power at the queried location. |
| radio_port_id | [string](#string) | optional | The RadioPort that produced the max expected power at the queried location. |
| power_per_radio_port | [PowerPerRadioPort](#zms-dst-v1-PowerPerRadioPort) | repeated | The max expected power produced per transmitting radio port at the queried location. |






<a name="zms-dst-v1-GetObservationRequest"></a>

### GetObservationRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-dst-v1-RequestHeader) |  | The generic request header. |
| id | [string](#string) |  |  |






<a name="zms-dst-v1-GetObservationResponse"></a>

### GetObservationResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-dst-v1-ResponseHeader) |  | The generic response header. |
| observation | [Observation](#zms-dst-v1-Observation) |  |  |






<a name="zms-dst-v1-GetObservationsRequest"></a>

### GetObservationsRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-dst-v1-RequestHeader) |  | The generic request header. |
| min_freq | [int64](#int64) | optional |  |
| max_freq | [int64](#int64) | optional |  |
| min_power | [double](#double) | optional |  |
| max_power | [double](#double) | optional |  |
| violation | [bool](#bool) | optional |  |
| starts_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| ends_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |






<a name="zms-dst-v1-GetObservationsResponse"></a>

### GetObservationsResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-dst-v1-ResponseHeader) |  | The generic response header. |
| observations | [Observation](#zms-dst-v1-Observation) | repeated |  |






<a name="zms-dst-v1-GetTxConstraintsRequest"></a>

### GetTxConstraintsRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-dst-v1-RequestHeader) |  | The generic request header. |
| radio_port_id | [string](#string) |  |  |
| freq | [int64](#int64) |  |  |
| area | [zms.geo.v1.Area](#zms-geo-v1-Area) |  |  |
| max_ext_rssi | [double](#double) |  |  |






<a name="zms-dst-v1-GetTxConstraintsResponse"></a>

### GetTxConstraintsResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-dst-v1-ResponseHeader) |  | The generic response header. |
| max_power | [double](#double) |  |  |






<a name="zms-dst-v1-GetUnoccupiedSpectrumRequest"></a>

### GetUnoccupiedSpectrumRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-dst-v1-RequestHeader) |  | The generic request header. |
| min_freq | [int64](#int64) |  |  |
| max_freq | [int64](#int64) |  |  |
| occupancy_threshold | [double](#double) |  |  |
| min_bandwidth | [int64](#int64) | optional |  |
| starts_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| ends_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| type | [string](#string) | optional |  |
| format | [string](#string) | optional |  |
| monitor_ids | [string](#string) | repeated |  |






<a name="zms-dst-v1-GetUnoccupiedSpectrumResponse"></a>

### GetUnoccupiedSpectrumResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-dst-v1-ResponseHeader) |  | The generic response header. |
| ranges | [FrequencyRange](#zms-dst-v1-FrequencyRange) | repeated |  |
| starts_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| ends_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |






<a name="zms-dst-v1-Observation"></a>

### Observation



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| monitor_id | [string](#string) |  |  |
| collection_id | [string](#string) | optional |  |
| types | [string](#string) |  |  |
| format | [string](#string) |  |  |
| description | [string](#string) |  |  |
| min_freq | [int64](#int64) |  |  |
| max_freq | [int64](#int64) |  |  |
| freq_step | [int64](#int64) |  |  |
| starts_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| ends_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| data | [bytes](#bytes) | optional |  |
| violation | [bool](#bool) |  |  |
| violation_verified_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| interference | [bool](#bool) |  |  |
| interference_verified_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| element_id | [string](#string) |  |  |
| creator_id | [string](#string) |  |  |
| updater_id | [string](#string) | optional |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| updated_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) | optional |  |
| violated_grant_id | [string](#string) | optional |  |
| interfered_grant_id | [string](#string) | optional |  |
| annotations | [Annotation](#zms-dst-v1-Annotation) | repeated |  |






<a name="zms-dst-v1-PowerPerRadioPort"></a>

### PowerPerRadioPort
A message encapsulating power per radio port.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| radio_port_id | [string](#string) |  |  |
| grant_id | [string](#string) | optional |  |
| power | [double](#double) |  |  |






<a name="zms-dst-v1-PropsimPowerGlobal"></a>

### PropsimPowerGlobal



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |






<a name="zms-dst-v1-PropsimPowerGrant"></a>

### PropsimPowerGrant



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| grant_id | [string](#string) |  |  |
| size | [uint64](#uint64) |  |  |
| created_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| finished_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| deleted_at | [google.protobuf.Timestamp](#google-protobuf-Timestamp) |  |  |
| geoserver_workspace | [string](#string) | optional |  |
| geoserver_layer | [string](#string) | optional |  |
| job_outputs | [PropsimPowerGrantJobOutput](#zms-dst-v1-PropsimPowerGrantJobOutput) | repeated |  |






<a name="zms-dst-v1-PropsimPowerGrantJobOutput"></a>

### PropsimPowerGrantJobOutput



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [string](#string) |  |  |
| propsim_power_grant_id | [string](#string) |  |  |
| propsim_job_output_id | [string](#string) |  |  |






<a name="zms-dst-v1-RequestHeader"></a>

### RequestHeader
A generic request header.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| req_id | [string](#string) | optional | A request ID; will be returned in ResponseHeader if set. |
| elaborate | [bool](#bool) | optional | Whether or not to elaborate (to return parent-child relationships). |






<a name="zms-dst-v1-ResponseHeader"></a>

### ResponseHeader
A generic response header.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| req_id | [string](#string) | optional | A request ID; will be returned in ResponseHeader if set. |






<a name="zms-dst-v1-SubscribeRequest"></a>

### SubscribeRequest
The request body for the `Subscribe` method.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [RequestHeader](#zms-dst-v1-RequestHeader) |  | The generic request header. |
| filters | [zms.event.v1.EventFilter](#zms-event-v1-EventFilter) | repeated | A list of `zms.event.v1.EventFilter`. |
| include | [bool](#bool) | optional | Whether or not to return the associated object in the Event response. Defaults to `true` if unset. |






<a name="zms-dst-v1-SubscribeResponse"></a>

### SubscribeResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| header | [ResponseHeader](#zms-dst-v1-ResponseHeader) |  | The generic response header. |
| events | [Event](#zms-dst-v1-Event) | repeated | A list of matching events. |





 


<a name="zms-dst-v1-EventCode"></a>

### EventCode
Event codes that correspond to zmc service objects.

| Name | Number | Description |
| ---- | ------ | ----------- |
| EC_UNSPECIFIED | 0 |  |
| EC_OBSERVATION | 3001 |  |
| EC_ANNOTATION | 3002 |  |
| EC_PROPSIM | 3003 |  |
| EC_PROPSIM_JOB | 3004 |  |


 

 


<a name="zms-dst-v1-Dst"></a>

### Dst
The Digital Spectrum Twin service for a ZMS.  Stores observations,
indexes data, propagation simulation maps, and provides a query interface
over observations and predicted maps.

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| GetObservation | [GetObservationRequest](#zms-dst-v1-GetObservationRequest) | [GetObservationResponse](#zms-dst-v1-GetObservationResponse) | Returns the requested Observation. |
| GetObservations | [GetObservationsRequest](#zms-dst-v1-GetObservationsRequest) | [GetObservationsResponse](#zms-dst-v1-GetObservationsResponse) | Returns a list of matching Observations, given constraints. |
| GetUnoccupiedSpectrum | [GetUnoccupiedSpectrumRequest](#zms-dst-v1-GetUnoccupiedSpectrumRequest) | [GetUnoccupiedSpectrumResponse](#zms-dst-v1-GetUnoccupiedSpectrumResponse) | Returns unoccupied spectrum, if any, in accordance with the request. |
| GetTxConstraints | [GetTxConstraintsRequest](#zms-dst-v1-GetTxConstraintsRequest) | [GetTxConstraintsResponse](#zms-dst-v1-GetTxConstraintsResponse) | Returns a set of transmission constraints, if any, in accordance with the request. |
| GetExpectedLoss | [GetExpectedLossRequest](#zms-dst-v1-GetExpectedLossRequest) | [GetExpectedLossResponse](#zms-dst-v1-GetExpectedLossResponse) | Returns the predicted path loss at the given receive location and time, if any. |
| GetExpectedPower | [GetExpectedPowerRequest](#zms-dst-v1-GetExpectedPowerRequest) | [GetExpectedPowerResponse](#zms-dst-v1-GetExpectedPowerResponse) | Returns the max predicted receive power at the given receive location, based on the combination of the `GetExpectedPowerRequest` parameters. In addition to the max, we also return a list of expected power transmitted per-RadioPort. If none of `grant_id`, `time`, or `radio_port_id` are specified, we set `time` to be `now`, and determine expected power based on all immediate live grants. |
| CreatePropsimPowerGrant | [CreatePropsimPowerGrantRequest](#zms-dst-v1-CreatePropsimPowerGrantRequest) | [CreatePropsimPowerGrantResponse](#zms-dst-v1-CreatePropsimPowerGrantResponse) | Produces an aggregate propsim composed of the granted txpower of all radios in the grant. |
| CreatePropsimPowerGlobal | [CreatePropsimPowerGlobalRequest](#zms-dst-v1-CreatePropsimPowerGlobalRequest) | [CreatePropsimPowerGrantResponse](#zms-dst-v1-CreatePropsimPowerGrantResponse) | Produces an aggregate propsim composed of the sum of all grants&#39; aggregate txpower. |
| CheckGrantConflict | [CheckGrantConflictRequest](#zms-dst-v1-CheckGrantConflictRequest) | [CheckGrantConflictResponse](#zms-dst-v1-CheckGrantConflictResponse) | Check if expected power for two grants overlaps according to their IntConstraints. For instance: Check if A&#39;s B-interfering power (-125) enters B&#39;s de facto coverage zone if A(-125) intersects B(-125) Check if B&#39;s A-intefering power (-120) enters A&#39;s de facto coverage zone if B(-120) intersects A(-120) NB: we use a de facto coverage zone for now. |
| Subscribe | [SubscribeRequest](#zms-dst-v1-SubscribeRequest) | [SubscribeResponse](#zms-dst-v1-SubscribeResponse) stream | Subscribe returns a stream of events from this service according to the list of filters provided in SubscribeRequest. The server will only close this stream when it shuts down; clients are responsible to detect the case where all objects allowed or referenced by the filter have been deleted or revoked, and therefore no events will be streamed in the future. |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

