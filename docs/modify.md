<!--
SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# Creating or Modifying Services

This document provides guidance to build new services and create or update
individual APIs.

Make sure to follow the `protobuf` [general best practice](https://protobuf.dev/programming-guides/dos-donts/)
and [gRPC API best practice](https://protobuf.dev/programming-guides/api/)
when modifying and developing service gRPC APIs.  We follow most of this
guidance:

  * Each service `rpc` declaration (e.g. `Foo`) should accept a single
    Request input parameter (e.g. `FooRequest`), or a stream; and return a
    single Response output parameter (e.g. `FooResponse`), or a stream.

  * Each service should define a common `RequestHeader` and `ResponseHeader`
    with an `optional` `Id` member, and each Request and Response message
    type should use these common header types.

  * Use the `validate` protobuf patterns and runtime libraries insofar as
    possible.
