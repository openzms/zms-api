#!/bin/sh

# SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
#
# SPDX-License-Identifier: Apache-2.0

set -x

rm -rf build/tmp
mkdir -p build/tmp/python
python3 -m grpc_tools.protoc \
    -I/protos \
    --proto_path=proto/ \
    --python_out=pyi_out:build/tmp/python/ \
    --grpclib_python_out=build/tmp/python/ \
    $@
if [ -d build/tmp/python/zms ]; then
    mv build/tmp/python/zms build/tmp/python/zms_api
    # Convert the obvious import statement module refs.
    find build/tmp/python/zms_api -name '*.py*' \
        -exec sed -i -e 's/^import zms\./import zms_api./g' {} +
    find build/tmp/python/zms_api -name '*.py*' \
        -exec sed -i -e 's/^from zms\./from zms_api./g' {} +
    # Convert the bareword module refs.  NB: cannot convert any refs in a
    # string; those are protocol-specific.
    find build/tmp/python/zms_api -name '*_grpc.py' \
        -exec sed -i -e '/^[^'"'"'"]*$/{s/zms\./zms_api./g;}' {} +
    #find build/tmp/python/zms_api -name '*.py' \
    #    -exec sed -i -e 's/zms\./zms_api./g' {} +
fi
rsync -av build/tmp/python/ python/
rm -rf build/tmp
