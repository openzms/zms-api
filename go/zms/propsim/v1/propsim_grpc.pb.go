// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.12
// source: zms/propsim/v1/propsim.proto

package propsim_v1

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// JobServiceClient is the client API for JobService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type JobServiceClient interface {
	// Returns this service's descriptor: its features, parameters, and run
	// constraints (e.g. parallel job limitations, its
	// persistence/statefulness, etc).
	GetServiceDescriptor(ctx context.Context, in *GetServiceDescriptorRequest, opts ...grpc.CallOption) (*GetServiceDescriptorResponse, error)
	// Creates a Job in accordance with request parameters, or returns an
	// error.  Services must allow Jobs to be created, even if they
	// subsequently refuse to Start them, which they may do temporarily if
	// insufficient resources are available.
	CreateJob(ctx context.Context, in *CreateJobRequest, opts ...grpc.CallOption) (*CreateJobResponse, error)
	// Returns an estimate of job computation duration.  (Optional.)
	EstimateJob(ctx context.Context, in *EstimateJobRequest, opts ...grpc.CallOption) (*EstimateJobResponse, error)
	// Starts a Job.  The service may refuse to Start a Job if it lacks
	// resources.
	StartJob(ctx context.Context, in *StartJobRequest, opts ...grpc.CallOption) (*StartJobResponse, error)
	// Pauses execution of a Job (e.g., so that a higher-priority job may
	// have a better chance of execution).  Optional.
	PauseJob(ctx context.Context, in *PauseJobRequest, opts ...grpc.CallOption) (*PauseJobResponse, error)
	// Cancels ongoing execution of a Job entirely.  Optional.
	CancelJob(ctx context.Context, in *CancelJobRequest, opts ...grpc.CallOption) (*CancelJobResponse, error)
	// Deletes a Job from memory, and persistence storage, if persistent.
	DeleteJob(ctx context.Context, in *DeleteJobRequest, opts ...grpc.CallOption) (*DeleteJobResponse, error)
	// Returns a stream of JobEvents.  If a client calls this on an
	// incomplete job, the server must close the stream when the job becomes
	// complete or failed.  If a client calls this on a completed or failed
	// job that is not deleted, we will close on deletion.
	GetJobEvents(ctx context.Context, in *GetJobEventsRequest, opts ...grpc.CallOption) (JobService_GetJobEventsClient, error)
	// Returns a JobOutput.
	GetJobOutput(ctx context.Context, in *GetJobOutputRequest, opts ...grpc.CallOption) (*GetJobOutputResponse, error)
}

type jobServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewJobServiceClient(cc grpc.ClientConnInterface) JobServiceClient {
	return &jobServiceClient{cc}
}

func (c *jobServiceClient) GetServiceDescriptor(ctx context.Context, in *GetServiceDescriptorRequest, opts ...grpc.CallOption) (*GetServiceDescriptorResponse, error) {
	out := new(GetServiceDescriptorResponse)
	err := c.cc.Invoke(ctx, "/zms.propsim.v1.JobService/GetServiceDescriptor", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *jobServiceClient) CreateJob(ctx context.Context, in *CreateJobRequest, opts ...grpc.CallOption) (*CreateJobResponse, error) {
	out := new(CreateJobResponse)
	err := c.cc.Invoke(ctx, "/zms.propsim.v1.JobService/CreateJob", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *jobServiceClient) EstimateJob(ctx context.Context, in *EstimateJobRequest, opts ...grpc.CallOption) (*EstimateJobResponse, error) {
	out := new(EstimateJobResponse)
	err := c.cc.Invoke(ctx, "/zms.propsim.v1.JobService/EstimateJob", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *jobServiceClient) StartJob(ctx context.Context, in *StartJobRequest, opts ...grpc.CallOption) (*StartJobResponse, error) {
	out := new(StartJobResponse)
	err := c.cc.Invoke(ctx, "/zms.propsim.v1.JobService/StartJob", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *jobServiceClient) PauseJob(ctx context.Context, in *PauseJobRequest, opts ...grpc.CallOption) (*PauseJobResponse, error) {
	out := new(PauseJobResponse)
	err := c.cc.Invoke(ctx, "/zms.propsim.v1.JobService/PauseJob", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *jobServiceClient) CancelJob(ctx context.Context, in *CancelJobRequest, opts ...grpc.CallOption) (*CancelJobResponse, error) {
	out := new(CancelJobResponse)
	err := c.cc.Invoke(ctx, "/zms.propsim.v1.JobService/CancelJob", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *jobServiceClient) DeleteJob(ctx context.Context, in *DeleteJobRequest, opts ...grpc.CallOption) (*DeleteJobResponse, error) {
	out := new(DeleteJobResponse)
	err := c.cc.Invoke(ctx, "/zms.propsim.v1.JobService/DeleteJob", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *jobServiceClient) GetJobEvents(ctx context.Context, in *GetJobEventsRequest, opts ...grpc.CallOption) (JobService_GetJobEventsClient, error) {
	stream, err := c.cc.NewStream(ctx, &JobService_ServiceDesc.Streams[0], "/zms.propsim.v1.JobService/GetJobEvents", opts...)
	if err != nil {
		return nil, err
	}
	x := &jobServiceGetJobEventsClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type JobService_GetJobEventsClient interface {
	Recv() (*GetJobEventsResponse, error)
	grpc.ClientStream
}

type jobServiceGetJobEventsClient struct {
	grpc.ClientStream
}

func (x *jobServiceGetJobEventsClient) Recv() (*GetJobEventsResponse, error) {
	m := new(GetJobEventsResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *jobServiceClient) GetJobOutput(ctx context.Context, in *GetJobOutputRequest, opts ...grpc.CallOption) (*GetJobOutputResponse, error) {
	out := new(GetJobOutputResponse)
	err := c.cc.Invoke(ctx, "/zms.propsim.v1.JobService/GetJobOutput", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// JobServiceServer is the server API for JobService service.
// All implementations must embed UnimplementedJobServiceServer
// for forward compatibility
type JobServiceServer interface {
	// Returns this service's descriptor: its features, parameters, and run
	// constraints (e.g. parallel job limitations, its
	// persistence/statefulness, etc).
	GetServiceDescriptor(context.Context, *GetServiceDescriptorRequest) (*GetServiceDescriptorResponse, error)
	// Creates a Job in accordance with request parameters, or returns an
	// error.  Services must allow Jobs to be created, even if they
	// subsequently refuse to Start them, which they may do temporarily if
	// insufficient resources are available.
	CreateJob(context.Context, *CreateJobRequest) (*CreateJobResponse, error)
	// Returns an estimate of job computation duration.  (Optional.)
	EstimateJob(context.Context, *EstimateJobRequest) (*EstimateJobResponse, error)
	// Starts a Job.  The service may refuse to Start a Job if it lacks
	// resources.
	StartJob(context.Context, *StartJobRequest) (*StartJobResponse, error)
	// Pauses execution of a Job (e.g., so that a higher-priority job may
	// have a better chance of execution).  Optional.
	PauseJob(context.Context, *PauseJobRequest) (*PauseJobResponse, error)
	// Cancels ongoing execution of a Job entirely.  Optional.
	CancelJob(context.Context, *CancelJobRequest) (*CancelJobResponse, error)
	// Deletes a Job from memory, and persistence storage, if persistent.
	DeleteJob(context.Context, *DeleteJobRequest) (*DeleteJobResponse, error)
	// Returns a stream of JobEvents.  If a client calls this on an
	// incomplete job, the server must close the stream when the job becomes
	// complete or failed.  If a client calls this on a completed or failed
	// job that is not deleted, we will close on deletion.
	GetJobEvents(*GetJobEventsRequest, JobService_GetJobEventsServer) error
	// Returns a JobOutput.
	GetJobOutput(context.Context, *GetJobOutputRequest) (*GetJobOutputResponse, error)
	mustEmbedUnimplementedJobServiceServer()
}

// UnimplementedJobServiceServer must be embedded to have forward compatible implementations.
type UnimplementedJobServiceServer struct {
}

func (UnimplementedJobServiceServer) GetServiceDescriptor(context.Context, *GetServiceDescriptorRequest) (*GetServiceDescriptorResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetServiceDescriptor not implemented")
}
func (UnimplementedJobServiceServer) CreateJob(context.Context, *CreateJobRequest) (*CreateJobResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateJob not implemented")
}
func (UnimplementedJobServiceServer) EstimateJob(context.Context, *EstimateJobRequest) (*EstimateJobResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method EstimateJob not implemented")
}
func (UnimplementedJobServiceServer) StartJob(context.Context, *StartJobRequest) (*StartJobResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method StartJob not implemented")
}
func (UnimplementedJobServiceServer) PauseJob(context.Context, *PauseJobRequest) (*PauseJobResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method PauseJob not implemented")
}
func (UnimplementedJobServiceServer) CancelJob(context.Context, *CancelJobRequest) (*CancelJobResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CancelJob not implemented")
}
func (UnimplementedJobServiceServer) DeleteJob(context.Context, *DeleteJobRequest) (*DeleteJobResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteJob not implemented")
}
func (UnimplementedJobServiceServer) GetJobEvents(*GetJobEventsRequest, JobService_GetJobEventsServer) error {
	return status.Errorf(codes.Unimplemented, "method GetJobEvents not implemented")
}
func (UnimplementedJobServiceServer) GetJobOutput(context.Context, *GetJobOutputRequest) (*GetJobOutputResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetJobOutput not implemented")
}
func (UnimplementedJobServiceServer) mustEmbedUnimplementedJobServiceServer() {}

// UnsafeJobServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to JobServiceServer will
// result in compilation errors.
type UnsafeJobServiceServer interface {
	mustEmbedUnimplementedJobServiceServer()
}

func RegisterJobServiceServer(s grpc.ServiceRegistrar, srv JobServiceServer) {
	s.RegisterService(&JobService_ServiceDesc, srv)
}

func _JobService_GetServiceDescriptor_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetServiceDescriptorRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(JobServiceServer).GetServiceDescriptor(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.propsim.v1.JobService/GetServiceDescriptor",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(JobServiceServer).GetServiceDescriptor(ctx, req.(*GetServiceDescriptorRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _JobService_CreateJob_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateJobRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(JobServiceServer).CreateJob(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.propsim.v1.JobService/CreateJob",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(JobServiceServer).CreateJob(ctx, req.(*CreateJobRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _JobService_EstimateJob_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EstimateJobRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(JobServiceServer).EstimateJob(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.propsim.v1.JobService/EstimateJob",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(JobServiceServer).EstimateJob(ctx, req.(*EstimateJobRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _JobService_StartJob_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(StartJobRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(JobServiceServer).StartJob(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.propsim.v1.JobService/StartJob",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(JobServiceServer).StartJob(ctx, req.(*StartJobRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _JobService_PauseJob_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PauseJobRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(JobServiceServer).PauseJob(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.propsim.v1.JobService/PauseJob",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(JobServiceServer).PauseJob(ctx, req.(*PauseJobRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _JobService_CancelJob_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CancelJobRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(JobServiceServer).CancelJob(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.propsim.v1.JobService/CancelJob",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(JobServiceServer).CancelJob(ctx, req.(*CancelJobRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _JobService_DeleteJob_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteJobRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(JobServiceServer).DeleteJob(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.propsim.v1.JobService/DeleteJob",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(JobServiceServer).DeleteJob(ctx, req.(*DeleteJobRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _JobService_GetJobEvents_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(GetJobEventsRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(JobServiceServer).GetJobEvents(m, &jobServiceGetJobEventsServer{stream})
}

type JobService_GetJobEventsServer interface {
	Send(*GetJobEventsResponse) error
	grpc.ServerStream
}

type jobServiceGetJobEventsServer struct {
	grpc.ServerStream
}

func (x *jobServiceGetJobEventsServer) Send(m *GetJobEventsResponse) error {
	return x.ServerStream.SendMsg(m)
}

func _JobService_GetJobOutput_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetJobOutputRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(JobServiceServer).GetJobOutput(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.propsim.v1.JobService/GetJobOutput",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(JobServiceServer).GetJobOutput(ctx, req.(*GetJobOutputRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// JobService_ServiceDesc is the grpc.ServiceDesc for JobService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var JobService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "zms.propsim.v1.JobService",
	HandlerType: (*JobServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetServiceDescriptor",
			Handler:    _JobService_GetServiceDescriptor_Handler,
		},
		{
			MethodName: "CreateJob",
			Handler:    _JobService_CreateJob_Handler,
		},
		{
			MethodName: "EstimateJob",
			Handler:    _JobService_EstimateJob_Handler,
		},
		{
			MethodName: "StartJob",
			Handler:    _JobService_StartJob_Handler,
		},
		{
			MethodName: "PauseJob",
			Handler:    _JobService_PauseJob_Handler,
		},
		{
			MethodName: "CancelJob",
			Handler:    _JobService_CancelJob_Handler,
		},
		{
			MethodName: "DeleteJob",
			Handler:    _JobService_DeleteJob_Handler,
		},
		{
			MethodName: "GetJobOutput",
			Handler:    _JobService_GetJobOutput_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "GetJobEvents",
			Handler:       _JobService_GetJobEvents_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "zms/propsim/v1/propsim.proto",
}
