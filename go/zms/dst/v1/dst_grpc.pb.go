// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.12
// source: zms/dst/v1/dst.proto

package dst_v1

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// DstClient is the client API for Dst service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type DstClient interface {
	// Returns the requested Observation.
	GetObservation(ctx context.Context, in *GetObservationRequest, opts ...grpc.CallOption) (*GetObservationResponse, error)
	// Returns a list of matching Observations, given constraints.
	GetObservations(ctx context.Context, in *GetObservationsRequest, opts ...grpc.CallOption) (*GetObservationsResponse, error)
	// Returns unoccupied spectrum, if any, in accordance with the request.
	GetUnoccupiedSpectrum(ctx context.Context, in *GetUnoccupiedSpectrumRequest, opts ...grpc.CallOption) (*GetUnoccupiedSpectrumResponse, error)
	// Returns a set of transmission constraints, if any, in accordance with
	// the request.
	GetTxConstraints(ctx context.Context, in *GetTxConstraintsRequest, opts ...grpc.CallOption) (*GetTxConstraintsResponse, error)
	// Returns the predicted path loss at the given receive location and
	// time, if any.
	GetExpectedLoss(ctx context.Context, in *GetExpectedLossRequest, opts ...grpc.CallOption) (*GetExpectedLossResponse, error)
	// Returns the max predicted receive power at the given receive
	// location, based on the combination of the `GetExpectedPowerRequest`
	// parameters.  In addition to the max, we also return a list of
	// expected power transmitted per-RadioPort.
	// If none of `grant_id`, `time`, or `radio_port_id` are specified, we
	// set `time` to be `now`, and determine expected power based on all
	// immediate live grants.
	GetExpectedPower(ctx context.Context, in *GetExpectedPowerRequest, opts ...grpc.CallOption) (*GetExpectedPowerResponse, error)
	// Produces an aggregate propsim composed of the granted txpower of all
	// radios in the grant.
	CreatePropsimPowerGrant(ctx context.Context, in *CreatePropsimPowerGrantRequest, opts ...grpc.CallOption) (*CreatePropsimPowerGrantResponse, error)
	// Produces an aggregate propsim composed of the sum of all grants'
	// aggregate txpower.
	CreatePropsimPowerGlobal(ctx context.Context, in *CreatePropsimPowerGlobalRequest, opts ...grpc.CallOption) (*CreatePropsimPowerGrantResponse, error)
	// Check if expected power for two grants overlaps according to their
	// IntConstraints.  For instance:
	// Check if A's B-interfering power (-125) enters B's de facto coverage zone
	//
	//	if A(-125) intersects B(-125)
	//
	// Check if B's A-intefering power (-120) enters A's de facto coverage zone
	//
	//	if B(-120) intersects A(-120)
	//
	// NB: we use a de facto coverage zone for now.
	CheckGrantConflict(ctx context.Context, in *CheckGrantConflictRequest, opts ...grpc.CallOption) (*CheckGrantConflictResponse, error)
	// Subscribe returns a stream of events from this service according to
	// the list of filters provided in SubscribeRequest.  The server will
	// only close this stream when it shuts down; clients are responsible to
	// detect the case where all objects allowed or referenced by the filter
	// have been deleted or revoked, and therefore no events will be
	// streamed in the future.
	Subscribe(ctx context.Context, in *SubscribeRequest, opts ...grpc.CallOption) (Dst_SubscribeClient, error)
}

type dstClient struct {
	cc grpc.ClientConnInterface
}

func NewDstClient(cc grpc.ClientConnInterface) DstClient {
	return &dstClient{cc}
}

func (c *dstClient) GetObservation(ctx context.Context, in *GetObservationRequest, opts ...grpc.CallOption) (*GetObservationResponse, error) {
	out := new(GetObservationResponse)
	err := c.cc.Invoke(ctx, "/zms.dst.v1.Dst/GetObservation", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dstClient) GetObservations(ctx context.Context, in *GetObservationsRequest, opts ...grpc.CallOption) (*GetObservationsResponse, error) {
	out := new(GetObservationsResponse)
	err := c.cc.Invoke(ctx, "/zms.dst.v1.Dst/GetObservations", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dstClient) GetUnoccupiedSpectrum(ctx context.Context, in *GetUnoccupiedSpectrumRequest, opts ...grpc.CallOption) (*GetUnoccupiedSpectrumResponse, error) {
	out := new(GetUnoccupiedSpectrumResponse)
	err := c.cc.Invoke(ctx, "/zms.dst.v1.Dst/GetUnoccupiedSpectrum", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dstClient) GetTxConstraints(ctx context.Context, in *GetTxConstraintsRequest, opts ...grpc.CallOption) (*GetTxConstraintsResponse, error) {
	out := new(GetTxConstraintsResponse)
	err := c.cc.Invoke(ctx, "/zms.dst.v1.Dst/GetTxConstraints", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dstClient) GetExpectedLoss(ctx context.Context, in *GetExpectedLossRequest, opts ...grpc.CallOption) (*GetExpectedLossResponse, error) {
	out := new(GetExpectedLossResponse)
	err := c.cc.Invoke(ctx, "/zms.dst.v1.Dst/GetExpectedLoss", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dstClient) GetExpectedPower(ctx context.Context, in *GetExpectedPowerRequest, opts ...grpc.CallOption) (*GetExpectedPowerResponse, error) {
	out := new(GetExpectedPowerResponse)
	err := c.cc.Invoke(ctx, "/zms.dst.v1.Dst/GetExpectedPower", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dstClient) CreatePropsimPowerGrant(ctx context.Context, in *CreatePropsimPowerGrantRequest, opts ...grpc.CallOption) (*CreatePropsimPowerGrantResponse, error) {
	out := new(CreatePropsimPowerGrantResponse)
	err := c.cc.Invoke(ctx, "/zms.dst.v1.Dst/CreatePropsimPowerGrant", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dstClient) CreatePropsimPowerGlobal(ctx context.Context, in *CreatePropsimPowerGlobalRequest, opts ...grpc.CallOption) (*CreatePropsimPowerGrantResponse, error) {
	out := new(CreatePropsimPowerGrantResponse)
	err := c.cc.Invoke(ctx, "/zms.dst.v1.Dst/CreatePropsimPowerGlobal", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dstClient) CheckGrantConflict(ctx context.Context, in *CheckGrantConflictRequest, opts ...grpc.CallOption) (*CheckGrantConflictResponse, error) {
	out := new(CheckGrantConflictResponse)
	err := c.cc.Invoke(ctx, "/zms.dst.v1.Dst/CheckGrantConflict", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dstClient) Subscribe(ctx context.Context, in *SubscribeRequest, opts ...grpc.CallOption) (Dst_SubscribeClient, error) {
	stream, err := c.cc.NewStream(ctx, &Dst_ServiceDesc.Streams[0], "/zms.dst.v1.Dst/Subscribe", opts...)
	if err != nil {
		return nil, err
	}
	x := &dstSubscribeClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type Dst_SubscribeClient interface {
	Recv() (*SubscribeResponse, error)
	grpc.ClientStream
}

type dstSubscribeClient struct {
	grpc.ClientStream
}

func (x *dstSubscribeClient) Recv() (*SubscribeResponse, error) {
	m := new(SubscribeResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// DstServer is the server API for Dst service.
// All implementations must embed UnimplementedDstServer
// for forward compatibility
type DstServer interface {
	// Returns the requested Observation.
	GetObservation(context.Context, *GetObservationRequest) (*GetObservationResponse, error)
	// Returns a list of matching Observations, given constraints.
	GetObservations(context.Context, *GetObservationsRequest) (*GetObservationsResponse, error)
	// Returns unoccupied spectrum, if any, in accordance with the request.
	GetUnoccupiedSpectrum(context.Context, *GetUnoccupiedSpectrumRequest) (*GetUnoccupiedSpectrumResponse, error)
	// Returns a set of transmission constraints, if any, in accordance with
	// the request.
	GetTxConstraints(context.Context, *GetTxConstraintsRequest) (*GetTxConstraintsResponse, error)
	// Returns the predicted path loss at the given receive location and
	// time, if any.
	GetExpectedLoss(context.Context, *GetExpectedLossRequest) (*GetExpectedLossResponse, error)
	// Returns the max predicted receive power at the given receive
	// location, based on the combination of the `GetExpectedPowerRequest`
	// parameters.  In addition to the max, we also return a list of
	// expected power transmitted per-RadioPort.
	// If none of `grant_id`, `time`, or `radio_port_id` are specified, we
	// set `time` to be `now`, and determine expected power based on all
	// immediate live grants.
	GetExpectedPower(context.Context, *GetExpectedPowerRequest) (*GetExpectedPowerResponse, error)
	// Produces an aggregate propsim composed of the granted txpower of all
	// radios in the grant.
	CreatePropsimPowerGrant(context.Context, *CreatePropsimPowerGrantRequest) (*CreatePropsimPowerGrantResponse, error)
	// Produces an aggregate propsim composed of the sum of all grants'
	// aggregate txpower.
	CreatePropsimPowerGlobal(context.Context, *CreatePropsimPowerGlobalRequest) (*CreatePropsimPowerGrantResponse, error)
	// Check if expected power for two grants overlaps according to their
	// IntConstraints.  For instance:
	// Check if A's B-interfering power (-125) enters B's de facto coverage zone
	//
	//	if A(-125) intersects B(-125)
	//
	// Check if B's A-intefering power (-120) enters A's de facto coverage zone
	//
	//	if B(-120) intersects A(-120)
	//
	// NB: we use a de facto coverage zone for now.
	CheckGrantConflict(context.Context, *CheckGrantConflictRequest) (*CheckGrantConflictResponse, error)
	// Subscribe returns a stream of events from this service according to
	// the list of filters provided in SubscribeRequest.  The server will
	// only close this stream when it shuts down; clients are responsible to
	// detect the case where all objects allowed or referenced by the filter
	// have been deleted or revoked, and therefore no events will be
	// streamed in the future.
	Subscribe(*SubscribeRequest, Dst_SubscribeServer) error
	mustEmbedUnimplementedDstServer()
}

// UnimplementedDstServer must be embedded to have forward compatible implementations.
type UnimplementedDstServer struct {
}

func (UnimplementedDstServer) GetObservation(context.Context, *GetObservationRequest) (*GetObservationResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetObservation not implemented")
}
func (UnimplementedDstServer) GetObservations(context.Context, *GetObservationsRequest) (*GetObservationsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetObservations not implemented")
}
func (UnimplementedDstServer) GetUnoccupiedSpectrum(context.Context, *GetUnoccupiedSpectrumRequest) (*GetUnoccupiedSpectrumResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetUnoccupiedSpectrum not implemented")
}
func (UnimplementedDstServer) GetTxConstraints(context.Context, *GetTxConstraintsRequest) (*GetTxConstraintsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetTxConstraints not implemented")
}
func (UnimplementedDstServer) GetExpectedLoss(context.Context, *GetExpectedLossRequest) (*GetExpectedLossResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetExpectedLoss not implemented")
}
func (UnimplementedDstServer) GetExpectedPower(context.Context, *GetExpectedPowerRequest) (*GetExpectedPowerResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetExpectedPower not implemented")
}
func (UnimplementedDstServer) CreatePropsimPowerGrant(context.Context, *CreatePropsimPowerGrantRequest) (*CreatePropsimPowerGrantResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreatePropsimPowerGrant not implemented")
}
func (UnimplementedDstServer) CreatePropsimPowerGlobal(context.Context, *CreatePropsimPowerGlobalRequest) (*CreatePropsimPowerGrantResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreatePropsimPowerGlobal not implemented")
}
func (UnimplementedDstServer) CheckGrantConflict(context.Context, *CheckGrantConflictRequest) (*CheckGrantConflictResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CheckGrantConflict not implemented")
}
func (UnimplementedDstServer) Subscribe(*SubscribeRequest, Dst_SubscribeServer) error {
	return status.Errorf(codes.Unimplemented, "method Subscribe not implemented")
}
func (UnimplementedDstServer) mustEmbedUnimplementedDstServer() {}

// UnsafeDstServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to DstServer will
// result in compilation errors.
type UnsafeDstServer interface {
	mustEmbedUnimplementedDstServer()
}

func RegisterDstServer(s grpc.ServiceRegistrar, srv DstServer) {
	s.RegisterService(&Dst_ServiceDesc, srv)
}

func _Dst_GetObservation_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetObservationRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DstServer).GetObservation(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.dst.v1.Dst/GetObservation",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DstServer).GetObservation(ctx, req.(*GetObservationRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Dst_GetObservations_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetObservationsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DstServer).GetObservations(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.dst.v1.Dst/GetObservations",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DstServer).GetObservations(ctx, req.(*GetObservationsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Dst_GetUnoccupiedSpectrum_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetUnoccupiedSpectrumRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DstServer).GetUnoccupiedSpectrum(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.dst.v1.Dst/GetUnoccupiedSpectrum",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DstServer).GetUnoccupiedSpectrum(ctx, req.(*GetUnoccupiedSpectrumRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Dst_GetTxConstraints_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetTxConstraintsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DstServer).GetTxConstraints(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.dst.v1.Dst/GetTxConstraints",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DstServer).GetTxConstraints(ctx, req.(*GetTxConstraintsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Dst_GetExpectedLoss_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetExpectedLossRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DstServer).GetExpectedLoss(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.dst.v1.Dst/GetExpectedLoss",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DstServer).GetExpectedLoss(ctx, req.(*GetExpectedLossRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Dst_GetExpectedPower_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetExpectedPowerRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DstServer).GetExpectedPower(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.dst.v1.Dst/GetExpectedPower",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DstServer).GetExpectedPower(ctx, req.(*GetExpectedPowerRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Dst_CreatePropsimPowerGrant_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreatePropsimPowerGrantRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DstServer).CreatePropsimPowerGrant(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.dst.v1.Dst/CreatePropsimPowerGrant",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DstServer).CreatePropsimPowerGrant(ctx, req.(*CreatePropsimPowerGrantRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Dst_CreatePropsimPowerGlobal_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreatePropsimPowerGlobalRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DstServer).CreatePropsimPowerGlobal(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.dst.v1.Dst/CreatePropsimPowerGlobal",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DstServer).CreatePropsimPowerGlobal(ctx, req.(*CreatePropsimPowerGlobalRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Dst_CheckGrantConflict_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CheckGrantConflictRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DstServer).CheckGrantConflict(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/zms.dst.v1.Dst/CheckGrantConflict",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DstServer).CheckGrantConflict(ctx, req.(*CheckGrantConflictRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Dst_Subscribe_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(SubscribeRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(DstServer).Subscribe(m, &dstSubscribeServer{stream})
}

type Dst_SubscribeServer interface {
	Send(*SubscribeResponse) error
	grpc.ServerStream
}

type dstSubscribeServer struct {
	grpc.ServerStream
}

func (x *dstSubscribeServer) Send(m *SubscribeResponse) error {
	return x.ServerStream.SendMsg(m)
}

// Dst_ServiceDesc is the grpc.ServiceDesc for Dst service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Dst_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "zms.dst.v1.Dst",
	HandlerType: (*DstServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetObservation",
			Handler:    _Dst_GetObservation_Handler,
		},
		{
			MethodName: "GetObservations",
			Handler:    _Dst_GetObservations_Handler,
		},
		{
			MethodName: "GetUnoccupiedSpectrum",
			Handler:    _Dst_GetUnoccupiedSpectrum_Handler,
		},
		{
			MethodName: "GetTxConstraints",
			Handler:    _Dst_GetTxConstraints_Handler,
		},
		{
			MethodName: "GetExpectedLoss",
			Handler:    _Dst_GetExpectedLoss_Handler,
		},
		{
			MethodName: "GetExpectedPower",
			Handler:    _Dst_GetExpectedPower_Handler,
		},
		{
			MethodName: "CreatePropsimPowerGrant",
			Handler:    _Dst_CreatePropsimPowerGrant_Handler,
		},
		{
			MethodName: "CreatePropsimPowerGlobal",
			Handler:    _Dst_CreatePropsimPowerGlobal_Handler,
		},
		{
			MethodName: "CheckGrantConflict",
			Handler:    _Dst_CheckGrantConflict_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "Subscribe",
			Handler:       _Dst_Subscribe_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "zms/dst/v1/dst.proto",
}
