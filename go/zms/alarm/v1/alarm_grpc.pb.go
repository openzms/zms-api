// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.12
// source: zms/alarm/v1/alarm.proto

package alarm_v1

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// AlarmClient is the client API for Alarm service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type AlarmClient interface {
	// Subscribe returns a stream of events from this service according to
	// the list of filters provided in SubscribeRequest.  The server will
	// only close this stream when it shuts down; clients are responsible to
	// detect the case where all objects allowed or referenced by the filter
	// have been deleted or revoked, and therefore no events will be
	// streamed in the future.
	Subscribe(ctx context.Context, in *SubscribeRequest, opts ...grpc.CallOption) (Alarm_SubscribeClient, error)
}

type alarmClient struct {
	cc grpc.ClientConnInterface
}

func NewAlarmClient(cc grpc.ClientConnInterface) AlarmClient {
	return &alarmClient{cc}
}

func (c *alarmClient) Subscribe(ctx context.Context, in *SubscribeRequest, opts ...grpc.CallOption) (Alarm_SubscribeClient, error) {
	stream, err := c.cc.NewStream(ctx, &Alarm_ServiceDesc.Streams[0], "/zms.alarm.v1.Alarm/Subscribe", opts...)
	if err != nil {
		return nil, err
	}
	x := &alarmSubscribeClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type Alarm_SubscribeClient interface {
	Recv() (*SubscribeResponse, error)
	grpc.ClientStream
}

type alarmSubscribeClient struct {
	grpc.ClientStream
}

func (x *alarmSubscribeClient) Recv() (*SubscribeResponse, error) {
	m := new(SubscribeResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// AlarmServer is the server API for Alarm service.
// All implementations must embed UnimplementedAlarmServer
// for forward compatibility
type AlarmServer interface {
	// Subscribe returns a stream of events from this service according to
	// the list of filters provided in SubscribeRequest.  The server will
	// only close this stream when it shuts down; clients are responsible to
	// detect the case where all objects allowed or referenced by the filter
	// have been deleted or revoked, and therefore no events will be
	// streamed in the future.
	Subscribe(*SubscribeRequest, Alarm_SubscribeServer) error
	mustEmbedUnimplementedAlarmServer()
}

// UnimplementedAlarmServer must be embedded to have forward compatible implementations.
type UnimplementedAlarmServer struct {
}

func (UnimplementedAlarmServer) Subscribe(*SubscribeRequest, Alarm_SubscribeServer) error {
	return status.Errorf(codes.Unimplemented, "method Subscribe not implemented")
}
func (UnimplementedAlarmServer) mustEmbedUnimplementedAlarmServer() {}

// UnsafeAlarmServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to AlarmServer will
// result in compilation errors.
type UnsafeAlarmServer interface {
	mustEmbedUnimplementedAlarmServer()
}

func RegisterAlarmServer(s grpc.ServiceRegistrar, srv AlarmServer) {
	s.RegisterService(&Alarm_ServiceDesc, srv)
}

func _Alarm_Subscribe_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(SubscribeRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(AlarmServer).Subscribe(m, &alarmSubscribeServer{stream})
}

type Alarm_SubscribeServer interface {
	Send(*SubscribeResponse) error
	grpc.ServerStream
}

type alarmSubscribeServer struct {
	grpc.ServerStream
}

func (x *alarmSubscribeServer) Send(m *SubscribeResponse) error {
	return x.ServerStream.SendMsg(m)
}

// Alarm_ServiceDesc is the grpc.ServiceDesc for Alarm service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Alarm_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "zms.alarm.v1.Alarm",
	HandlerType: (*AlarmServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "Subscribe",
			Handler:       _Alarm_Subscribe_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "zms/alarm/v1/alarm.proto",
}
