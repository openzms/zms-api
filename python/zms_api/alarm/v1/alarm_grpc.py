# Generated by the Protocol Buffers compiler. DO NOT EDIT!
# source: zms/alarm/v1/alarm.proto
# plugin: grpclib.plugin.main
import abc
import typing

import grpclib.const
import grpclib.client
if typing.TYPE_CHECKING:
    import grpclib.server

import validate.validate_pb2
import google.protobuf.timestamp_pb2
import zms_api.event.v1.event_pb2
import zms_api.alarm.v1.alarm_pb2


class AlarmBase(abc.ABC):

    @abc.abstractmethod
    async def Subscribe(self, stream: 'grpclib.server.Stream[zms.alarm.v1.alarm_pb2.SubscribeRequest, zms.alarm.v1.alarm_pb2.SubscribeResponse]') -> None:
        pass

    def __mapping__(self) -> typing.Dict[str, grpclib.const.Handler]:
        return {
            '/zms.alarm.v1.Alarm/Subscribe': grpclib.const.Handler(
                self.Subscribe,
                grpclib.const.Cardinality.UNARY_STREAM,
                zms_api.alarm.v1.alarm_pb2.SubscribeRequest,
                zms_api.alarm.v1.alarm_pb2.SubscribeResponse,
            ),
        }


class AlarmStub:

    def __init__(self, channel: grpclib.client.Channel) -> None:
        self.Subscribe = grpclib.client.UnaryStreamMethod(
            channel,
            '/zms.alarm.v1.Alarm/Subscribe',
            zms_api.alarm.v1.alarm_pb2.SubscribeRequest,
            zms_api.alarm.v1.alarm_pb2.SubscribeResponse,
        )
