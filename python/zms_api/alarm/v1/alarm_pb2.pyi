from validate import validate_pb2 as _validate_pb2
from google.protobuf import timestamp_pb2 as _timestamp_pb2
from zms_api.event.v1 import event_pb2 as _event_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class EventCode(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    EC_UNSPECIFIED: _ClassVar[EventCode]
    EC_CONSTRAINTCHANGE: _ClassVar[EventCode]
    EC_GRANTCHANGE: _ClassVar[EventCode]
EC_UNSPECIFIED: EventCode
EC_CONSTRAINTCHANGE: EventCode
EC_GRANTCHANGE: EventCode

class ConstraintChange(_message.Message):
    __slots__ = ["id", "grant_id", "constraint_id", "observation_id", "min_freq", "max_freq", "bandwidth", "max_eirp", "min_eirp", "description", "creator_id", "updater_id", "created_at", "updated_at", "deleted_at"]
    ID_FIELD_NUMBER: _ClassVar[int]
    GRANT_ID_FIELD_NUMBER: _ClassVar[int]
    CONSTRAINT_ID_FIELD_NUMBER: _ClassVar[int]
    OBSERVATION_ID_FIELD_NUMBER: _ClassVar[int]
    MIN_FREQ_FIELD_NUMBER: _ClassVar[int]
    MAX_FREQ_FIELD_NUMBER: _ClassVar[int]
    BANDWIDTH_FIELD_NUMBER: _ClassVar[int]
    MAX_EIRP_FIELD_NUMBER: _ClassVar[int]
    MIN_EIRP_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    CREATOR_ID_FIELD_NUMBER: _ClassVar[int]
    UPDATER_ID_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    id: str
    grant_id: str
    constraint_id: str
    observation_id: str
    min_freq: int
    max_freq: int
    bandwidth: int
    max_eirp: float
    min_eirp: float
    description: str
    creator_id: str
    updater_id: str
    created_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    def __init__(self, id: _Optional[str] = ..., grant_id: _Optional[str] = ..., constraint_id: _Optional[str] = ..., observation_id: _Optional[str] = ..., min_freq: _Optional[int] = ..., max_freq: _Optional[int] = ..., bandwidth: _Optional[int] = ..., max_eirp: _Optional[float] = ..., min_eirp: _Optional[float] = ..., description: _Optional[str] = ..., creator_id: _Optional[str] = ..., updater_id: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class GrantChange(_message.Message):
    __slots__ = ["id", "grant_id", "observation_id", "action", "description", "creator_id", "updater_id", "created_at", "updated_at", "deleted_at"]
    ID_FIELD_NUMBER: _ClassVar[int]
    GRANT_ID_FIELD_NUMBER: _ClassVar[int]
    OBSERVATION_ID_FIELD_NUMBER: _ClassVar[int]
    ACTION_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    CREATOR_ID_FIELD_NUMBER: _ClassVar[int]
    UPDATER_ID_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    id: str
    grant_id: str
    observation_id: str
    action: str
    description: str
    creator_id: str
    updater_id: str
    created_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    def __init__(self, id: _Optional[str] = ..., grant_id: _Optional[str] = ..., observation_id: _Optional[str] = ..., action: _Optional[str] = ..., description: _Optional[str] = ..., creator_id: _Optional[str] = ..., updater_id: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class Event(_message.Message):
    __slots__ = ["header", "constraint_change", "grant_change"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    CONSTRAINT_CHANGE_FIELD_NUMBER: _ClassVar[int]
    GRANT_CHANGE_FIELD_NUMBER: _ClassVar[int]
    header: _event_pb2.EventHeader
    constraint_change: ConstraintChange
    grant_change: GrantChange
    def __init__(self, header: _Optional[_Union[_event_pb2.EventHeader, _Mapping]] = ..., constraint_change: _Optional[_Union[ConstraintChange, _Mapping]] = ..., grant_change: _Optional[_Union[GrantChange, _Mapping]] = ...) -> None: ...

class RequestHeader(_message.Message):
    __slots__ = ["req_id", "elaborate"]
    REQ_ID_FIELD_NUMBER: _ClassVar[int]
    ELABORATE_FIELD_NUMBER: _ClassVar[int]
    req_id: str
    elaborate: bool
    def __init__(self, req_id: _Optional[str] = ..., elaborate: bool = ...) -> None: ...

class ResponseHeader(_message.Message):
    __slots__ = ["req_id"]
    REQ_ID_FIELD_NUMBER: _ClassVar[int]
    req_id: str
    def __init__(self, req_id: _Optional[str] = ...) -> None: ...

class SubscribeRequest(_message.Message):
    __slots__ = ["header", "filters", "include"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    FILTERS_FIELD_NUMBER: _ClassVar[int]
    INCLUDE_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    filters: _containers.RepeatedCompositeFieldContainer[_event_pb2.EventFilter]
    include: bool
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., filters: _Optional[_Iterable[_Union[_event_pb2.EventFilter, _Mapping]]] = ..., include: bool = ...) -> None: ...

class SubscribeResponse(_message.Message):
    __slots__ = ["header", "events"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    EVENTS_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    events: _containers.RepeatedCompositeFieldContainer[Event]
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., events: _Optional[_Iterable[_Union[Event, _Mapping]]] = ...) -> None: ...
