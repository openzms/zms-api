# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: zms/alarm/v1/alarm.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from validate import validate_pb2 as validate_dot_validate__pb2
from google.protobuf import timestamp_pb2 as google_dot_protobuf_dot_timestamp__pb2
from zms_api.event.v1 import event_pb2 as zms_dot_event_dot_v1_dot_event__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x18zms/alarm/v1/alarm.proto\x12\x0czms.alarm.v1\x1a\x17validate/validate.proto\x1a\x1fgoogle/protobuf/timestamp.proto\x1a\x18zms/event/v1/event.proto\"\x9b\x05\n\x10\x43onstraintChange\x12\x14\n\x02id\x18\x01 \x01(\tB\x08\xfa\x42\x05r\x03\xb0\x01\x01\x12\x1a\n\x08grant_id\x18\x02 \x01(\tB\x08\xfa\x42\x05r\x03\xb0\x01\x01\x12\x1f\n\rconstraint_id\x18\x03 \x01(\tB\x08\xfa\x42\x05r\x03\xb0\x01\x01\x12%\n\x0eobservation_id\x18\x04 \x01(\tB\x08\xfa\x42\x05r\x03\xb0\x01\x01H\x00\x88\x01\x01\x12\x15\n\x08min_freq\x18\x05 \x01(\x03H\x01\x88\x01\x01\x12\x15\n\x08max_freq\x18\x06 \x01(\x03H\x02\x88\x01\x01\x12\x16\n\tbandwidth\x18\x07 \x01(\x03H\x03\x88\x01\x01\x12\x15\n\x08max_eirp\x18\x08 \x01(\x01H\x04\x88\x01\x01\x12\x15\n\x08min_eirp\x18\t \x01(\x01H\x05\x88\x01\x01\x12\x18\n\x0b\x64\x65scription\x18\n \x01(\tH\x06\x88\x01\x01\x12!\n\ncreator_id\x18\x0b \x01(\tB\x08\xfa\x42\x05r\x03\xb0\x01\x01H\x07\x88\x01\x01\x12!\n\nupdater_id\x18\x0c \x01(\tB\x08\xfa\x42\x05r\x03\xb0\x01\x01H\x08\x88\x01\x01\x12.\n\ncreated_at\x18\r \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12\x33\n\nupdated_at\x18\x0e \x01(\x0b\x32\x1a.google.protobuf.TimestampH\t\x88\x01\x01\x12\x33\n\ndeleted_at\x18\x0f \x01(\x0b\x32\x1a.google.protobuf.TimestampH\n\x88\x01\x01\x42\x11\n\x0f_observation_idB\x0b\n\t_min_freqB\x0b\n\t_max_freqB\x0c\n\n_bandwidthB\x0b\n\t_max_eirpB\x0b\n\t_min_eirpB\x0e\n\x0c_descriptionB\r\n\x0b_creator_idB\r\n\x0b_updater_idB\r\n\x0b_updated_atB\r\n\x0b_deleted_at\"\xdf\x03\n\x0bGrantChange\x12\x14\n\x02id\x18\x01 \x01(\tB\x08\xfa\x42\x05r\x03\xb0\x01\x01\x12\x1a\n\x08grant_id\x18\x02 \x01(\tB\x08\xfa\x42\x05r\x03\xb0\x01\x01\x12%\n\x0eobservation_id\x18\x03 \x01(\tB\x08\xfa\x42\x05r\x03\xb0\x01\x01H\x00\x88\x01\x01\x12\x13\n\x06\x61\x63tion\x18\x04 \x01(\tH\x01\x88\x01\x01\x12\x18\n\x0b\x64\x65scription\x18\x05 \x01(\tH\x02\x88\x01\x01\x12!\n\ncreator_id\x18\x06 \x01(\tB\x08\xfa\x42\x05r\x03\xb0\x01\x01H\x03\x88\x01\x01\x12!\n\nupdater_id\x18\x07 \x01(\tB\x08\xfa\x42\x05r\x03\xb0\x01\x01H\x04\x88\x01\x01\x12.\n\ncreated_at\x18\x08 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12\x33\n\nupdated_at\x18\t \x01(\x0b\x32\x1a.google.protobuf.TimestampH\x05\x88\x01\x01\x12\x33\n\ndeleted_at\x18\n \x01(\x0b\x32\x1a.google.protobuf.TimestampH\x06\x88\x01\x01\x42\x11\n\x0f_observation_idB\t\n\x07_actionB\x0e\n\x0c_descriptionB\r\n\x0b_creator_idB\r\n\x0b_updater_idB\r\n\x0b_updated_atB\r\n\x0b_deleted_at\"\xac\x01\n\x05\x45vent\x12)\n\x06header\x18\x01 \x01(\x0b\x32\x19.zms.event.v1.EventHeader\x12;\n\x11\x63onstraint_change\x18\x02 \x01(\x0b\x32\x1e.zms.alarm.v1.ConstraintChangeH\x00\x12\x31\n\x0cgrant_change\x18\x03 \x01(\x0b\x32\x19.zms.alarm.v1.GrantChangeH\x00\x42\x08\n\x06object\"_\n\rRequestHeader\x12\x1d\n\x06req_id\x18\x01 \x01(\tB\x08\xfa\x42\x05r\x03\xb0\x01\x01H\x00\x88\x01\x01\x12\x16\n\telaborate\x18\x02 \x01(\x08H\x01\x88\x01\x01\x42\t\n\x07_req_idB\x0c\n\n_elaborate\":\n\x0eResponseHeader\x12\x1d\n\x06req_id\x18\x01 \x01(\tB\x08\xfa\x42\x05r\x03\xb0\x01\x01H\x00\x88\x01\x01\x42\t\n\x07_req_id\"\x8d\x01\n\x10SubscribeRequest\x12+\n\x06header\x18\x01 \x01(\x0b\x32\x1b.zms.alarm.v1.RequestHeader\x12*\n\x07\x66ilters\x18\x02 \x03(\x0b\x32\x19.zms.event.v1.EventFilter\x12\x14\n\x07include\x18\x03 \x01(\x08H\x00\x88\x01\x01\x42\n\n\x08_include\"f\n\x11SubscribeResponse\x12,\n\x06header\x18\x01 \x01(\x0b\x32\x1c.zms.alarm.v1.ResponseHeader\x12#\n\x06\x65vents\x18\x02 \x03(\x0b\x32\x13.zms.alarm.v1.Event*N\n\tEventCode\x12\x12\n\x0e\x45\x43_UNSPECIFIED\x10\x00\x12\x18\n\x13\x45\x43_CONSTRAINTCHANGE\x10\xa1\x1f\x12\x13\n\x0e\x45\x43_GRANTCHANGE\x10\xa2\x1f\x32W\n\x05\x41larm\x12N\n\tSubscribe\x12\x1e.zms.alarm.v1.SubscribeRequest\x1a\x1f.zms.alarm.v1.SubscribeResponse0\x01\x42?Z=gitlab.flux.utah.edu/openzms/zms-api/go/zms/alarm/v1;alarm_v1b\x06proto3')

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'zms.alarm.v1.alarm_pb2', _globals)
if _descriptor._USE_C_DESCRIPTORS == False:
  DESCRIPTOR._options = None
  DESCRIPTOR._serialized_options = b'Z=gitlab.flux.utah.edu/openzms/zms-api/go/zms/alarm/v1;alarm_v1'
  _CONSTRAINTCHANGE.fields_by_name['id']._options = None
  _CONSTRAINTCHANGE.fields_by_name['id']._serialized_options = b'\372B\005r\003\260\001\001'
  _CONSTRAINTCHANGE.fields_by_name['grant_id']._options = None
  _CONSTRAINTCHANGE.fields_by_name['grant_id']._serialized_options = b'\372B\005r\003\260\001\001'
  _CONSTRAINTCHANGE.fields_by_name['constraint_id']._options = None
  _CONSTRAINTCHANGE.fields_by_name['constraint_id']._serialized_options = b'\372B\005r\003\260\001\001'
  _CONSTRAINTCHANGE.fields_by_name['observation_id']._options = None
  _CONSTRAINTCHANGE.fields_by_name['observation_id']._serialized_options = b'\372B\005r\003\260\001\001'
  _CONSTRAINTCHANGE.fields_by_name['creator_id']._options = None
  _CONSTRAINTCHANGE.fields_by_name['creator_id']._serialized_options = b'\372B\005r\003\260\001\001'
  _CONSTRAINTCHANGE.fields_by_name['updater_id']._options = None
  _CONSTRAINTCHANGE.fields_by_name['updater_id']._serialized_options = b'\372B\005r\003\260\001\001'
  _GRANTCHANGE.fields_by_name['id']._options = None
  _GRANTCHANGE.fields_by_name['id']._serialized_options = b'\372B\005r\003\260\001\001'
  _GRANTCHANGE.fields_by_name['grant_id']._options = None
  _GRANTCHANGE.fields_by_name['grant_id']._serialized_options = b'\372B\005r\003\260\001\001'
  _GRANTCHANGE.fields_by_name['observation_id']._options = None
  _GRANTCHANGE.fields_by_name['observation_id']._serialized_options = b'\372B\005r\003\260\001\001'
  _GRANTCHANGE.fields_by_name['creator_id']._options = None
  _GRANTCHANGE.fields_by_name['creator_id']._serialized_options = b'\372B\005r\003\260\001\001'
  _GRANTCHANGE.fields_by_name['updater_id']._options = None
  _GRANTCHANGE.fields_by_name['updater_id']._serialized_options = b'\372B\005r\003\260\001\001'
  _REQUESTHEADER.fields_by_name['req_id']._options = None
  _REQUESTHEADER.fields_by_name['req_id']._serialized_options = b'\372B\005r\003\260\001\001'
  _RESPONSEHEADER.fields_by_name['req_id']._options = None
  _RESPONSEHEADER.fields_by_name['req_id']._serialized_options = b'\372B\005r\003\260\001\001'
  _globals['_EVENTCODE']._serialized_start=1858
  _globals['_EVENTCODE']._serialized_end=1936
  _globals['_CONSTRAINTCHANGE']._serialized_start=127
  _globals['_CONSTRAINTCHANGE']._serialized_end=794
  _globals['_GRANTCHANGE']._serialized_start=797
  _globals['_GRANTCHANGE']._serialized_end=1276
  _globals['_EVENT']._serialized_start=1279
  _globals['_EVENT']._serialized_end=1451
  _globals['_REQUESTHEADER']._serialized_start=1453
  _globals['_REQUESTHEADER']._serialized_end=1548
  _globals['_RESPONSEHEADER']._serialized_start=1550
  _globals['_RESPONSEHEADER']._serialized_end=1608
  _globals['_SUBSCRIBEREQUEST']._serialized_start=1611
  _globals['_SUBSCRIBEREQUEST']._serialized_end=1752
  _globals['_SUBSCRIBERESPONSE']._serialized_start=1754
  _globals['_SUBSCRIBERESPONSE']._serialized_end=1856
  _globals['_ALARM']._serialized_start=1938
  _globals['_ALARM']._serialized_end=2025
# @@protoc_insertion_point(module_scope)
