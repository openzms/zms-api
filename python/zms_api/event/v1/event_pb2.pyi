from validate import validate_pb2 as _validate_pb2
from google.protobuf import timestamp_pb2 as _timestamp_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class EventType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    ET_UNSPECIFIED: _ClassVar[EventType]
    ET_OTHER: _ClassVar[EventType]
    ET_CREATED: _ClassVar[EventType]
    ET_UPDATED: _ClassVar[EventType]
    ET_DELETED: _ClassVar[EventType]
    ET_REVOKED: _ClassVar[EventType]
    ET_ENABLED: _ClassVar[EventType]
    ET_DISABLED: _ClassVar[EventType]
    ET_APPROVED: _ClassVar[EventType]
    ET_DENIED: _ClassVar[EventType]
    ET_SCHEDULED: _ClassVar[EventType]
    ET_RUNNING: _ClassVar[EventType]
    ET_COMPLETED: _ClassVar[EventType]
    ET_FAILED: _ClassVar[EventType]
    ET_SUCCEEDED: _ClassVar[EventType]
    ET_PROGRESS: _ClassVar[EventType]
    ET_SUSPENDED: _ClassVar[EventType]
    ET_RESUMED: _ClassVar[EventType]
    ET_STARTED: _ClassVar[EventType]
    ET_STOPPED: _ClassVar[EventType]
    ET_VIOLATION: _ClassVar[EventType]
    ET_INTERFERENCE: _ClassVar[EventType]
    ET_ACTION: _ClassVar[EventType]
    ET_PENDING: _ClassVar[EventType]

class EventSourceType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    EST_UNSPECIFIED: _ClassVar[EventSourceType]
    EST_IDENTITY: _ClassVar[EventSourceType]
    EST_ZMC: _ClassVar[EventSourceType]
    EST_DST: _ClassVar[EventSourceType]
    EST_ALARM: _ClassVar[EventSourceType]
ET_UNSPECIFIED: EventType
ET_OTHER: EventType
ET_CREATED: EventType
ET_UPDATED: EventType
ET_DELETED: EventType
ET_REVOKED: EventType
ET_ENABLED: EventType
ET_DISABLED: EventType
ET_APPROVED: EventType
ET_DENIED: EventType
ET_SCHEDULED: EventType
ET_RUNNING: EventType
ET_COMPLETED: EventType
ET_FAILED: EventType
ET_SUCCEEDED: EventType
ET_PROGRESS: EventType
ET_SUSPENDED: EventType
ET_RESUMED: EventType
ET_STARTED: EventType
ET_STOPPED: EventType
ET_VIOLATION: EventType
ET_INTERFERENCE: EventType
ET_ACTION: EventType
ET_PENDING: EventType
EST_UNSPECIFIED: EventSourceType
EST_IDENTITY: EventSourceType
EST_ZMC: EventSourceType
EST_DST: EventSourceType
EST_ALARM: EventSourceType

class EventHeader(_message.Message):
    __slots__ = ["type", "code", "source_type", "source_id", "id", "time", "object_id", "user_id", "element_id"]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    CODE_FIELD_NUMBER: _ClassVar[int]
    SOURCE_TYPE_FIELD_NUMBER: _ClassVar[int]
    SOURCE_ID_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    TIME_FIELD_NUMBER: _ClassVar[int]
    OBJECT_ID_FIELD_NUMBER: _ClassVar[int]
    USER_ID_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_ID_FIELD_NUMBER: _ClassVar[int]
    type: int
    code: int
    source_type: int
    source_id: str
    id: str
    time: _timestamp_pb2.Timestamp
    object_id: str
    user_id: str
    element_id: str
    def __init__(self, type: _Optional[int] = ..., code: _Optional[int] = ..., source_type: _Optional[int] = ..., source_id: _Optional[str] = ..., id: _Optional[str] = ..., time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., object_id: _Optional[str] = ..., user_id: _Optional[str] = ..., element_id: _Optional[str] = ...) -> None: ...

class EventFilter(_message.Message):
    __slots__ = ["types", "codes", "object_ids", "user_ids", "element_ids"]
    TYPES_FIELD_NUMBER: _ClassVar[int]
    CODES_FIELD_NUMBER: _ClassVar[int]
    OBJECT_IDS_FIELD_NUMBER: _ClassVar[int]
    USER_IDS_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_IDS_FIELD_NUMBER: _ClassVar[int]
    types: _containers.RepeatedScalarFieldContainer[int]
    codes: _containers.RepeatedScalarFieldContainer[int]
    object_ids: _containers.RepeatedScalarFieldContainer[str]
    user_ids: _containers.RepeatedScalarFieldContainer[str]
    element_ids: _containers.RepeatedScalarFieldContainer[str]
    def __init__(self, types: _Optional[_Iterable[int]] = ..., codes: _Optional[_Iterable[int]] = ..., object_ids: _Optional[_Iterable[str]] = ..., user_ids: _Optional[_Iterable[str]] = ..., element_ids: _Optional[_Iterable[str]] = ...) -> None: ...
