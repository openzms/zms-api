from zms_api.propsim.v1 import propsim_pb2 as _propsim_pb2
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class RegisterJobServiceRequest(_message.Message):
    __slots__ = ["desc"]
    DESC_FIELD_NUMBER: _ClassVar[int]
    desc: _propsim_pb2.ServiceDescriptor
    def __init__(self, desc: _Optional[_Union[_propsim_pb2.ServiceDescriptor, _Mapping]] = ...) -> None: ...

class RegisterJobServiceResponse(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...
