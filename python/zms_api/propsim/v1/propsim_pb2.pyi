from validate import validate_pb2 as _validate_pb2
from google.protobuf import timestamp_pb2 as _timestamp_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class MapType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    MT_UNSPECIFIED: _ClassVar[MapType]
    MT_RAW: _ClassVar[MapType]
    MT_GEOTIFF: _ClassVar[MapType]
    MT_DEM: _ClassVar[MapType]

class MapKind(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    MK_UNSPECIFIED: _ClassVar[MapKind]
    MK_OTHER: _ClassVar[MapKind]
    MK_PATHLOSS: _ClassVar[MapKind]
    MK_POWER: _ClassVar[MapKind]
    MK_LOS: _ClassVar[MapKind]

class MapUnit(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    MU_UNSPECIFIED: _ClassVar[MapUnit]
    MU_OTHER: _ClassVar[MapUnit]
    MU_dB: _ClassVar[MapUnit]
    MU_dBm: _ClassVar[MapUnit]
    MU_mW: _ClassVar[MapUnit]
    MU_LOS: _ClassVar[MapUnit]

class ParameterType(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    PARAMETER_TYPE_BOOL: _ClassVar[ParameterType]
    PARAMETER_TYPE_INT: _ClassVar[ParameterType]
    PARAMETER_TYPE_DOUBLE: _ClassVar[ParameterType]
    PARAMETER_TYPE_STRING: _ClassVar[ParameterType]

class JobStatus(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    JS_UNSPECIFIED: _ClassVar[JobStatus]
    JS_CREATED: _ClassVar[JobStatus]
    JS_SCHEDULED: _ClassVar[JobStatus]
    JS_RUNNING: _ClassVar[JobStatus]
    JS_COMPLETE: _ClassVar[JobStatus]
    JS_FAILED: _ClassVar[JobStatus]
    JS_DELETED: _ClassVar[JobStatus]

class JobEvent(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    JOB_EVENT_STATUS: _ClassVar[JobEvent]
    JOB_EVENT_PROGRESS: _ClassVar[JobEvent]
    JOB_EVENT_OUTPUT: _ClassVar[JobEvent]
MT_UNSPECIFIED: MapType
MT_RAW: MapType
MT_GEOTIFF: MapType
MT_DEM: MapType
MK_UNSPECIFIED: MapKind
MK_OTHER: MapKind
MK_PATHLOSS: MapKind
MK_POWER: MapKind
MK_LOS: MapKind
MU_UNSPECIFIED: MapUnit
MU_OTHER: MapUnit
MU_dB: MapUnit
MU_dBm: MapUnit
MU_mW: MapUnit
MU_LOS: MapUnit
PARAMETER_TYPE_BOOL: ParameterType
PARAMETER_TYPE_INT: ParameterType
PARAMETER_TYPE_DOUBLE: ParameterType
PARAMETER_TYPE_STRING: ParameterType
JS_UNSPECIFIED: JobStatus
JS_CREATED: JobStatus
JS_SCHEDULED: JobStatus
JS_RUNNING: JobStatus
JS_COMPLETE: JobStatus
JS_FAILED: JobStatus
JS_DELETED: JobStatus
JOB_EVENT_STATUS: JobEvent
JOB_EVENT_PROGRESS: JobEvent
JOB_EVENT_OUTPUT: JobEvent

class AreaPoint(_message.Message):
    __slots__ = ["x", "y", "z"]
    X_FIELD_NUMBER: _ClassVar[int]
    Y_FIELD_NUMBER: _ClassVar[int]
    Z_FIELD_NUMBER: _ClassVar[int]
    x: float
    y: float
    z: float
    def __init__(self, x: _Optional[float] = ..., y: _Optional[float] = ..., z: _Optional[float] = ...) -> None: ...

class Area(_message.Message):
    __slots__ = ["crs_ogc_srid", "points"]
    CRS_OGC_SRID_FIELD_NUMBER: _ClassVar[int]
    POINTS_FIELD_NUMBER: _ClassVar[int]
    crs_ogc_srid: int
    points: _containers.RepeatedCompositeFieldContainer[AreaPoint]
    def __init__(self, crs_ogc_srid: _Optional[int] = ..., points: _Optional[_Iterable[_Union[AreaPoint, _Mapping]]] = ...) -> None: ...

class HttpBasicAuth(_message.Message):
    __slots__ = ["username", "password"]
    USERNAME_FIELD_NUMBER: _ClassVar[int]
    PASSWORD_FIELD_NUMBER: _ClassVar[int]
    username: str
    password: str
    def __init__(self, username: _Optional[str] = ..., password: _Optional[str] = ...) -> None: ...

class HttpHeaderAuth(_message.Message):
    __slots__ = ["name", "value"]
    NAME_FIELD_NUMBER: _ClassVar[int]
    VALUE_FIELD_NUMBER: _ClassVar[int]
    name: str
    value: str
    def __init__(self, name: _Optional[str] = ..., value: _Optional[str] = ...) -> None: ...

class UrlData(_message.Message):
    __slots__ = ["url", "size", "basic", "header", "desc"]
    URL_FIELD_NUMBER: _ClassVar[int]
    SIZE_FIELD_NUMBER: _ClassVar[int]
    BASIC_FIELD_NUMBER: _ClassVar[int]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    DESC_FIELD_NUMBER: _ClassVar[int]
    url: str
    size: int
    basic: HttpBasicAuth
    header: HttpHeaderAuth
    desc: str
    def __init__(self, url: _Optional[str] = ..., size: _Optional[int] = ..., basic: _Optional[_Union[HttpBasicAuth, _Mapping]] = ..., header: _Optional[_Union[HttpHeaderAuth, _Mapping]] = ..., desc: _Optional[str] = ...) -> None: ...

class RawData(_message.Message):
    __slots__ = ["data", "format", "desc"]
    DATA_FIELD_NUMBER: _ClassVar[int]
    FORMAT_FIELD_NUMBER: _ClassVar[int]
    DESC_FIELD_NUMBER: _ClassVar[int]
    data: bytes
    format: str
    desc: str
    def __init__(self, data: _Optional[bytes] = ..., format: _Optional[str] = ..., desc: _Optional[str] = ...) -> None: ...

class Map(_message.Message):
    __slots__ = ["id", "type", "area", "raw_data", "url_data", "kind", "unit"]
    ID_FIELD_NUMBER: _ClassVar[int]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    AREA_FIELD_NUMBER: _ClassVar[int]
    RAW_DATA_FIELD_NUMBER: _ClassVar[int]
    URL_DATA_FIELD_NUMBER: _ClassVar[int]
    KIND_FIELD_NUMBER: _ClassVar[int]
    UNIT_FIELD_NUMBER: _ClassVar[int]
    id: str
    type: MapType
    area: Area
    raw_data: RawData
    url_data: UrlData
    kind: MapKind
    unit: MapUnit
    def __init__(self, id: _Optional[str] = ..., type: _Optional[_Union[MapType, str]] = ..., area: _Optional[_Union[Area, _Mapping]] = ..., raw_data: _Optional[_Union[RawData, _Mapping]] = ..., url_data: _Optional[_Union[UrlData, _Mapping]] = ..., kind: _Optional[_Union[MapKind, str]] = ..., unit: _Optional[_Union[MapUnit, str]] = ...) -> None: ...

class Feature(_message.Message):
    __slots__ = ["name", "description"]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    name: str
    description: str
    def __init__(self, name: _Optional[str] = ..., description: _Optional[str] = ...) -> None: ...

class ParameterDefinition(_message.Message):
    __slots__ = ["name", "ptype", "desc", "is_required", "b", "i", "d", "s"]
    NAME_FIELD_NUMBER: _ClassVar[int]
    PTYPE_FIELD_NUMBER: _ClassVar[int]
    DESC_FIELD_NUMBER: _ClassVar[int]
    IS_REQUIRED_FIELD_NUMBER: _ClassVar[int]
    B_FIELD_NUMBER: _ClassVar[int]
    I_FIELD_NUMBER: _ClassVar[int]
    D_FIELD_NUMBER: _ClassVar[int]
    S_FIELD_NUMBER: _ClassVar[int]
    name: str
    ptype: ParameterType
    desc: str
    is_required: bool
    b: bool
    i: int
    d: float
    s: str
    def __init__(self, name: _Optional[str] = ..., ptype: _Optional[_Union[ParameterType, str]] = ..., desc: _Optional[str] = ..., is_required: bool = ..., b: bool = ..., i: _Optional[int] = ..., d: _Optional[float] = ..., s: _Optional[str] = ...) -> None: ...

class ParameterValue(_message.Message):
    __slots__ = ["name", "b", "i", "d", "s"]
    NAME_FIELD_NUMBER: _ClassVar[int]
    B_FIELD_NUMBER: _ClassVar[int]
    I_FIELD_NUMBER: _ClassVar[int]
    D_FIELD_NUMBER: _ClassVar[int]
    S_FIELD_NUMBER: _ClassVar[int]
    name: str
    b: bool
    i: int
    d: float
    s: str
    def __init__(self, name: _Optional[str] = ..., b: bool = ..., i: _Optional[int] = ..., d: _Optional[float] = ..., s: _Optional[str] = ...) -> None: ...

class JobOutput(_message.Message):
    __slots__ = ["id", "name", "desc", "map", "raw_data", "url_data"]
    ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESC_FIELD_NUMBER: _ClassVar[int]
    MAP_FIELD_NUMBER: _ClassVar[int]
    RAW_DATA_FIELD_NUMBER: _ClassVar[int]
    URL_DATA_FIELD_NUMBER: _ClassVar[int]
    id: str
    name: str
    desc: str
    map: Map
    raw_data: RawData
    url_data: UrlData
    def __init__(self, id: _Optional[str] = ..., name: _Optional[str] = ..., desc: _Optional[str] = ..., map: _Optional[_Union[Map, _Mapping]] = ..., raw_data: _Optional[_Union[RawData, _Mapping]] = ..., url_data: _Optional[_Union[UrlData, _Mapping]] = ...) -> None: ...

class Job(_message.Message):
    __slots__ = ["id", "parameters", "area", "base_map", "status", "completion_percentage", "outputs", "created_at", "estimated_start", "estimated_finish", "started_at", "updated_at", "finished_at", "deleted_at"]
    ID_FIELD_NUMBER: _ClassVar[int]
    PARAMETERS_FIELD_NUMBER: _ClassVar[int]
    AREA_FIELD_NUMBER: _ClassVar[int]
    BASE_MAP_FIELD_NUMBER: _ClassVar[int]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    COMPLETION_PERCENTAGE_FIELD_NUMBER: _ClassVar[int]
    OUTPUTS_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    ESTIMATED_START_FIELD_NUMBER: _ClassVar[int]
    ESTIMATED_FINISH_FIELD_NUMBER: _ClassVar[int]
    STARTED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    FINISHED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    id: str
    parameters: _containers.RepeatedCompositeFieldContainer[ParameterValue]
    area: Area
    base_map: Map
    status: JobStatus
    completion_percentage: float
    outputs: _containers.RepeatedCompositeFieldContainer[JobOutput]
    created_at: _timestamp_pb2.Timestamp
    estimated_start: _timestamp_pb2.Timestamp
    estimated_finish: _timestamp_pb2.Timestamp
    started_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    finished_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    def __init__(self, id: _Optional[str] = ..., parameters: _Optional[_Iterable[_Union[ParameterValue, _Mapping]]] = ..., area: _Optional[_Union[Area, _Mapping]] = ..., base_map: _Optional[_Union[Map, _Mapping]] = ..., status: _Optional[_Union[JobStatus, str]] = ..., completion_percentage: _Optional[float] = ..., outputs: _Optional[_Iterable[_Union[JobOutput, _Mapping]]] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., estimated_start: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., estimated_finish: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., started_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., finished_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class ServiceDescriptor(_message.Message):
    __slots__ = ["id", "endpoint", "description", "version", "api_version", "persistent", "features", "parameters"]
    ID_FIELD_NUMBER: _ClassVar[int]
    ENDPOINT_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    VERSION_FIELD_NUMBER: _ClassVar[int]
    API_VERSION_FIELD_NUMBER: _ClassVar[int]
    PERSISTENT_FIELD_NUMBER: _ClassVar[int]
    FEATURES_FIELD_NUMBER: _ClassVar[int]
    PARAMETERS_FIELD_NUMBER: _ClassVar[int]
    id: str
    endpoint: str
    description: str
    version: str
    api_version: str
    persistent: bool
    features: _containers.RepeatedCompositeFieldContainer[Feature]
    parameters: _containers.RepeatedCompositeFieldContainer[ParameterDefinition]
    def __init__(self, id: _Optional[str] = ..., endpoint: _Optional[str] = ..., description: _Optional[str] = ..., version: _Optional[str] = ..., api_version: _Optional[str] = ..., persistent: bool = ..., features: _Optional[_Iterable[_Union[Feature, _Mapping]]] = ..., parameters: _Optional[_Iterable[_Union[ParameterDefinition, _Mapping]]] = ...) -> None: ...

class GetServiceDescriptorRequest(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class GetServiceDescriptorResponse(_message.Message):
    __slots__ = ["desc"]
    DESC_FIELD_NUMBER: _ClassVar[int]
    desc: ServiceDescriptor
    def __init__(self, desc: _Optional[_Union[ServiceDescriptor, _Mapping]] = ...) -> None: ...

class CreateJobRequest(_message.Message):
    __slots__ = ["job"]
    JOB_FIELD_NUMBER: _ClassVar[int]
    job: Job
    def __init__(self, job: _Optional[_Union[Job, _Mapping]] = ...) -> None: ...

class CreateJobResponse(_message.Message):
    __slots__ = ["job_id"]
    JOB_ID_FIELD_NUMBER: _ClassVar[int]
    job_id: str
    def __init__(self, job_id: _Optional[str] = ...) -> None: ...

class EstimateJobRequest(_message.Message):
    __slots__ = ["job_id"]
    JOB_ID_FIELD_NUMBER: _ClassVar[int]
    job_id: str
    def __init__(self, job_id: _Optional[str] = ...) -> None: ...

class EstimateJobResponse(_message.Message):
    __slots__ = ["total_time", "estimated_start"]
    TOTAL_TIME_FIELD_NUMBER: _ClassVar[int]
    ESTIMATED_START_FIELD_NUMBER: _ClassVar[int]
    total_time: int
    estimated_start: _timestamp_pb2.Timestamp
    def __init__(self, total_time: _Optional[int] = ..., estimated_start: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class StartJobRequest(_message.Message):
    __slots__ = ["job_id"]
    JOB_ID_FIELD_NUMBER: _ClassVar[int]
    job_id: str
    def __init__(self, job_id: _Optional[str] = ...) -> None: ...

class StartJobResponse(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class PauseJobRequest(_message.Message):
    __slots__ = ["job_id"]
    JOB_ID_FIELD_NUMBER: _ClassVar[int]
    job_id: str
    def __init__(self, job_id: _Optional[str] = ...) -> None: ...

class PauseJobResponse(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class CancelJobRequest(_message.Message):
    __slots__ = ["job_id"]
    JOB_ID_FIELD_NUMBER: _ClassVar[int]
    job_id: str
    def __init__(self, job_id: _Optional[str] = ...) -> None: ...

class CancelJobResponse(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class DeleteJobRequest(_message.Message):
    __slots__ = ["job_id"]
    JOB_ID_FIELD_NUMBER: _ClassVar[int]
    job_id: str
    def __init__(self, job_id: _Optional[str] = ...) -> None: ...

class DeleteJobResponse(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class GetJobEventsRequest(_message.Message):
    __slots__ = ["job_id"]
    JOB_ID_FIELD_NUMBER: _ClassVar[int]
    job_id: str
    def __init__(self, job_id: _Optional[str] = ...) -> None: ...

class GetJobEventsResponse(_message.Message):
    __slots__ = ["event", "job"]
    EVENT_FIELD_NUMBER: _ClassVar[int]
    JOB_FIELD_NUMBER: _ClassVar[int]
    event: JobEvent
    job: Job
    def __init__(self, event: _Optional[_Union[JobEvent, str]] = ..., job: _Optional[_Union[Job, _Mapping]] = ...) -> None: ...

class GetJobOutputRequest(_message.Message):
    __slots__ = ["job_id", "output_id"]
    JOB_ID_FIELD_NUMBER: _ClassVar[int]
    OUTPUT_ID_FIELD_NUMBER: _ClassVar[int]
    job_id: str
    output_id: str
    def __init__(self, job_id: _Optional[str] = ..., output_id: _Optional[str] = ...) -> None: ...

class GetJobOutputResponse(_message.Message):
    __slots__ = ["output"]
    OUTPUT_FIELD_NUMBER: _ClassVar[int]
    output: JobOutput
    def __init__(self, output: _Optional[_Union[JobOutput, _Mapping]] = ...) -> None: ...
