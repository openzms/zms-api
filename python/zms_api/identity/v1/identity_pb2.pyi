from validate import validate_pb2 as _validate_pb2
from google.protobuf import timestamp_pb2 as _timestamp_pb2
from zms_api.event.v1 import event_pb2 as _event_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class EventCode(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    EC_UNSPECIFIED: _ClassVar[EventCode]
    EC_USER: _ClassVar[EventCode]
    EC_ELEMENT: _ClassVar[EventCode]
    EC_ROLE: _ClassVar[EventCode]
    EC_ROLEBINDING: _ClassVar[EventCode]
    EC_TOKEN: _ClassVar[EventCode]
    EC_SERVICE: _ClassVar[EventCode]
EC_UNSPECIFIED: EventCode
EC_USER: EventCode
EC_ELEMENT: EventCode
EC_ROLE: EventCode
EC_ROLEBINDING: EventCode
EC_TOKEN: EventCode
EC_SERVICE: EventCode

class UserEmailAddress(_message.Message):
    __slots__ = ["id", "user_id", "email_address"]
    ID_FIELD_NUMBER: _ClassVar[int]
    USER_ID_FIELD_NUMBER: _ClassVar[int]
    EMAIL_ADDRESS_FIELD_NUMBER: _ClassVar[int]
    id: str
    user_id: str
    email_address: str
    def __init__(self, id: _Optional[str] = ..., user_id: _Optional[str] = ..., email_address: _Optional[str] = ...) -> None: ...

class User(_message.Message):
    __slots__ = ["id", "name", "given_name", "family_name", "full_name", "created_at", "updated_at", "deleted_at", "enabled", "primary_email_address_id", "primary_email_address", "email_addresses", "role_bindings"]
    ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    GIVEN_NAME_FIELD_NUMBER: _ClassVar[int]
    FAMILY_NAME_FIELD_NUMBER: _ClassVar[int]
    FULL_NAME_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    ENABLED_FIELD_NUMBER: _ClassVar[int]
    PRIMARY_EMAIL_ADDRESS_ID_FIELD_NUMBER: _ClassVar[int]
    PRIMARY_EMAIL_ADDRESS_FIELD_NUMBER: _ClassVar[int]
    EMAIL_ADDRESSES_FIELD_NUMBER: _ClassVar[int]
    ROLE_BINDINGS_FIELD_NUMBER: _ClassVar[int]
    id: str
    name: str
    given_name: str
    family_name: str
    full_name: str
    created_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    enabled: bool
    primary_email_address_id: str
    primary_email_address: UserEmailAddress
    email_addresses: _containers.RepeatedCompositeFieldContainer[UserEmailAddress]
    role_bindings: _containers.RepeatedCompositeFieldContainer[RoleBinding]
    def __init__(self, id: _Optional[str] = ..., name: _Optional[str] = ..., given_name: _Optional[str] = ..., family_name: _Optional[str] = ..., full_name: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., enabled: bool = ..., primary_email_address_id: _Optional[str] = ..., primary_email_address: _Optional[_Union[UserEmailAddress, _Mapping]] = ..., email_addresses: _Optional[_Iterable[_Union[UserEmailAddress, _Mapping]]] = ..., role_bindings: _Optional[_Iterable[_Union[RoleBinding, _Mapping]]] = ...) -> None: ...

class ElementAttribute(_message.Message):
    __slots__ = ["id", "element_id", "name", "value", "description"]
    ID_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    VALUE_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    id: str
    element_id: str
    name: str
    value: str
    description: str
    def __init__(self, id: _Optional[str] = ..., element_id: _Optional[str] = ..., name: _Optional[str] = ..., value: _Optional[str] = ..., description: _Optional[str] = ...) -> None: ...

class Element(_message.Message):
    __slots__ = ["id", "creator_user_id", "name", "html_url", "description", "is_public", "created_at", "approved_at", "updated_at", "deleted_at", "enabled", "attributes"]
    ID_FIELD_NUMBER: _ClassVar[int]
    CREATOR_USER_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    HTML_URL_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    IS_PUBLIC_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    APPROVED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    ENABLED_FIELD_NUMBER: _ClassVar[int]
    ATTRIBUTES_FIELD_NUMBER: _ClassVar[int]
    id: str
    creator_user_id: str
    name: str
    html_url: str
    description: str
    is_public: bool
    created_at: _timestamp_pb2.Timestamp
    approved_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    enabled: bool
    attributes: _containers.RepeatedCompositeFieldContainer[ElementAttribute]
    def __init__(self, id: _Optional[str] = ..., creator_user_id: _Optional[str] = ..., name: _Optional[str] = ..., html_url: _Optional[str] = ..., description: _Optional[str] = ..., is_public: bool = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., approved_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., enabled: bool = ..., attributes: _Optional[_Iterable[_Union[ElementAttribute, _Mapping]]] = ...) -> None: ...

class Token(_message.Message):
    __slots__ = ["id", "user_id", "token", "issued_at", "expires_at", "role_bindings"]
    ID_FIELD_NUMBER: _ClassVar[int]
    USER_ID_FIELD_NUMBER: _ClassVar[int]
    TOKEN_FIELD_NUMBER: _ClassVar[int]
    ISSUED_AT_FIELD_NUMBER: _ClassVar[int]
    EXPIRES_AT_FIELD_NUMBER: _ClassVar[int]
    ROLE_BINDINGS_FIELD_NUMBER: _ClassVar[int]
    id: str
    user_id: str
    token: str
    issued_at: _timestamp_pb2.Timestamp
    expires_at: _timestamp_pb2.Timestamp
    role_bindings: _containers.RepeatedCompositeFieldContainer[RoleBinding]
    def __init__(self, id: _Optional[str] = ..., user_id: _Optional[str] = ..., token: _Optional[str] = ..., issued_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., expires_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., role_bindings: _Optional[_Iterable[_Union[RoleBinding, _Mapping]]] = ...) -> None: ...

class RoleBinding(_message.Message):
    __slots__ = ["id", "role_id", "user_id", "element_id", "created_at", "approved_at", "deleted_at"]
    ID_FIELD_NUMBER: _ClassVar[int]
    ROLE_ID_FIELD_NUMBER: _ClassVar[int]
    USER_ID_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_ID_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    APPROVED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    id: str
    role_id: str
    user_id: str
    element_id: str
    created_at: _timestamp_pb2.Timestamp
    approved_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    def __init__(self, id: _Optional[str] = ..., role_id: _Optional[str] = ..., user_id: _Optional[str] = ..., element_id: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., approved_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class Role(_message.Message):
    __slots__ = ["id", "name", "value", "description"]
    ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    VALUE_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    id: str
    name: str
    value: int
    description: str
    def __init__(self, id: _Optional[str] = ..., name: _Optional[str] = ..., value: _Optional[int] = ..., description: _Optional[str] = ...) -> None: ...

class Service(_message.Message):
    __slots__ = ["id", "name", "kind", "endpoint", "endpoint_api_uri", "description", "version", "api_version", "enabled", "created_at", "updated_at", "deleted_at", "heartbeat_at"]
    ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    KIND_FIELD_NUMBER: _ClassVar[int]
    ENDPOINT_FIELD_NUMBER: _ClassVar[int]
    ENDPOINT_API_URI_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    VERSION_FIELD_NUMBER: _ClassVar[int]
    API_VERSION_FIELD_NUMBER: _ClassVar[int]
    ENABLED_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    HEARTBEAT_AT_FIELD_NUMBER: _ClassVar[int]
    id: str
    name: str
    kind: str
    endpoint: str
    endpoint_api_uri: str
    description: str
    version: str
    api_version: str
    enabled: bool
    created_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    heartbeat_at: _timestamp_pb2.Timestamp
    def __init__(self, id: _Optional[str] = ..., name: _Optional[str] = ..., kind: _Optional[str] = ..., endpoint: _Optional[str] = ..., endpoint_api_uri: _Optional[str] = ..., description: _Optional[str] = ..., version: _Optional[str] = ..., api_version: _Optional[str] = ..., enabled: bool = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., heartbeat_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class Event(_message.Message):
    __slots__ = ["header", "user", "element", "role", "role_binding", "token", "service"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    USER_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_FIELD_NUMBER: _ClassVar[int]
    ROLE_FIELD_NUMBER: _ClassVar[int]
    ROLE_BINDING_FIELD_NUMBER: _ClassVar[int]
    TOKEN_FIELD_NUMBER: _ClassVar[int]
    SERVICE_FIELD_NUMBER: _ClassVar[int]
    header: _event_pb2.EventHeader
    user: User
    element: Element
    role: Role
    role_binding: RoleBinding
    token: Token
    service: Service
    def __init__(self, header: _Optional[_Union[_event_pb2.EventHeader, _Mapping]] = ..., user: _Optional[_Union[User, _Mapping]] = ..., element: _Optional[_Union[Element, _Mapping]] = ..., role: _Optional[_Union[Role, _Mapping]] = ..., role_binding: _Optional[_Union[RoleBinding, _Mapping]] = ..., token: _Optional[_Union[Token, _Mapping]] = ..., service: _Optional[_Union[Service, _Mapping]] = ...) -> None: ...

class RequestHeader(_message.Message):
    __slots__ = ["req_id", "elaborate"]
    REQ_ID_FIELD_NUMBER: _ClassVar[int]
    ELABORATE_FIELD_NUMBER: _ClassVar[int]
    req_id: str
    elaborate: bool
    def __init__(self, req_id: _Optional[str] = ..., elaborate: bool = ...) -> None: ...

class ResponseHeader(_message.Message):
    __slots__ = ["req_id"]
    REQ_ID_FIELD_NUMBER: _ClassVar[int]
    req_id: str
    def __init__(self, req_id: _Optional[str] = ...) -> None: ...

class GetUserRequest(_message.Message):
    __slots__ = ["header", "id"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    id: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., id: _Optional[str] = ...) -> None: ...

class GetUserResponse(_message.Message):
    __slots__ = ["header", "user"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    USER_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    user: User
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., user: _Optional[_Union[User, _Mapping]] = ...) -> None: ...

class GetElementRequest(_message.Message):
    __slots__ = ["header", "id"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    id: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., id: _Optional[str] = ...) -> None: ...

class GetElementResponse(_message.Message):
    __slots__ = ["header", "element"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    element: Element
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., element: _Optional[_Union[Element, _Mapping]] = ...) -> None: ...

class GetTokenRequest(_message.Message):
    __slots__ = ["header", "token"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    TOKEN_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    token: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., token: _Optional[str] = ...) -> None: ...

class ValidateTokenRequest(_message.Message):
    __slots__ = ["header", "token"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    TOKEN_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    token: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., token: _Optional[str] = ...) -> None: ...

class GetTokenResponse(_message.Message):
    __slots__ = ["header", "token"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    TOKEN_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    token: Token
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., token: _Optional[_Union[Token, _Mapping]] = ...) -> None: ...

class ValidateTokenResponse(_message.Message):
    __slots__ = ["header"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ...) -> None: ...

class RoleListRequest(_message.Message):
    __slots__ = ["header"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ...) -> None: ...

class RoleListResponse(_message.Message):
    __slots__ = ["header", "roles"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ROLES_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    roles: _containers.RepeatedCompositeFieldContainer[Role]
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., roles: _Optional[_Iterable[_Union[Role, _Mapping]]] = ...) -> None: ...

class ServiceListRequest(_message.Message):
    __slots__ = ["header"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ...) -> None: ...

class ServiceListResponse(_message.Message):
    __slots__ = ["header", "services"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    SERVICES_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    services: _containers.RepeatedCompositeFieldContainer[Service]
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., services: _Optional[_Iterable[_Union[Service, _Mapping]]] = ...) -> None: ...

class RegisterServiceRequest(_message.Message):
    __slots__ = ["header", "service"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    SERVICE_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    service: Service
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., service: _Optional[_Union[Service, _Mapping]] = ...) -> None: ...

class RegisterServiceResponse(_message.Message):
    __slots__ = ["header", "service", "secret", "bootstrapElementId", "bootstrapUserId"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    SERVICE_FIELD_NUMBER: _ClassVar[int]
    SECRET_FIELD_NUMBER: _ClassVar[int]
    BOOTSTRAPELEMENTID_FIELD_NUMBER: _ClassVar[int]
    BOOTSTRAPUSERID_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    service: Service
    secret: str
    bootstrapElementId: str
    bootstrapUserId: str
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., service: _Optional[_Union[Service, _Mapping]] = ..., secret: _Optional[str] = ..., bootstrapElementId: _Optional[str] = ..., bootstrapUserId: _Optional[str] = ...) -> None: ...

class UnregisterServiceRequest(_message.Message):
    __slots__ = ["header", "id", "secret"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    SECRET_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    id: str
    secret: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., id: _Optional[str] = ..., secret: _Optional[str] = ...) -> None: ...

class UnregisterServiceResponse(_message.Message):
    __slots__ = ["header"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ...) -> None: ...

class UpdateServiceRequest(_message.Message):
    __slots__ = ["header", "id", "secret", "enabled"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    SECRET_FIELD_NUMBER: _ClassVar[int]
    ENABLED_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    id: str
    secret: str
    enabled: bool
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., id: _Optional[str] = ..., secret: _Optional[str] = ..., enabled: bool = ...) -> None: ...

class UpdateServiceResponse(_message.Message):
    __slots__ = ["header", "service"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    SERVICE_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    service: Service
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., service: _Optional[_Union[Service, _Mapping]] = ...) -> None: ...

class SubscribeRequest(_message.Message):
    __slots__ = ["header", "filters", "include"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    FILTERS_FIELD_NUMBER: _ClassVar[int]
    INCLUDE_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    filters: _containers.RepeatedCompositeFieldContainer[_event_pb2.EventFilter]
    include: bool
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., filters: _Optional[_Iterable[_Union[_event_pb2.EventFilter, _Mapping]]] = ..., include: bool = ...) -> None: ...

class SubscribeResponse(_message.Message):
    __slots__ = ["header", "events"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    EVENTS_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    events: _containers.RepeatedCompositeFieldContainer[Event]
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., events: _Optional[_Iterable[_Union[Event, _Mapping]]] = ...) -> None: ...
