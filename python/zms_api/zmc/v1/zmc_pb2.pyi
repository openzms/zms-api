from validate import validate_pb2 as _validate_pb2
from google.protobuf import timestamp_pb2 as _timestamp_pb2
from zms_api.event.v1 import event_pb2 as _event_pb2
from zms_api.geo.v1 import geo_pb2 as _geo_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class EventCode(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    EC_UNSPECIFIED: _ClassVar[EventCode]
    EC_ZONE: _ClassVar[EventCode]
    EC_ANTENNA: _ClassVar[EventCode]
    EC_RADIO: _ClassVar[EventCode]
    EC_RADIOPORT: _ClassVar[EventCode]
    EC_MONITOR: _ClassVar[EventCode]
    EC_GRANT: _ClassVar[EventCode]
    EC_SPECTRUM: _ClassVar[EventCode]
    EC_CLAIM: _ClassVar[EventCode]
EC_UNSPECIFIED: EventCode
EC_ZONE: EventCode
EC_ANTENNA: EventCode
EC_RADIO: EventCode
EC_RADIOPORT: EventCode
EC_MONITOR: EventCode
EC_GRANT: EventCode
EC_SPECTRUM: EventCode
EC_CLAIM: EventCode

class Zone(_message.Message):
    __slots__ = ["id", "element_id", "name", "description", "area_id", "creator_id", "updater_id", "created_at", "updated_at", "deleted_at", "area"]
    ID_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    AREA_ID_FIELD_NUMBER: _ClassVar[int]
    CREATOR_ID_FIELD_NUMBER: _ClassVar[int]
    UPDATER_ID_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    AREA_FIELD_NUMBER: _ClassVar[int]
    id: str
    element_id: str
    name: str
    description: str
    area_id: str
    creator_id: str
    updater_id: str
    created_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    area: _geo_pb2.Area
    def __init__(self, id: _Optional[str] = ..., element_id: _Optional[str] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., area_id: _Optional[str] = ..., creator_id: _Optional[str] = ..., updater_id: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., area: _Optional[_Union[_geo_pb2.Area, _Mapping]] = ...) -> None: ...

class Location(_message.Message):
    __slots__ = ["id", "element_id", "name", "srid", "x", "y", "z", "is_public", "creator_id", "updater_id", "created_at", "updated_at", "deleted_at"]
    ID_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    SRID_FIELD_NUMBER: _ClassVar[int]
    X_FIELD_NUMBER: _ClassVar[int]
    Y_FIELD_NUMBER: _ClassVar[int]
    Z_FIELD_NUMBER: _ClassVar[int]
    IS_PUBLIC_FIELD_NUMBER: _ClassVar[int]
    CREATOR_ID_FIELD_NUMBER: _ClassVar[int]
    UPDATER_ID_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    id: str
    element_id: str
    name: str
    srid: int
    x: float
    y: float
    z: float
    is_public: bool
    creator_id: str
    updater_id: str
    created_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    def __init__(self, id: _Optional[str] = ..., element_id: _Optional[str] = ..., name: _Optional[str] = ..., srid: _Optional[int] = ..., x: _Optional[float] = ..., y: _Optional[float] = ..., z: _Optional[float] = ..., is_public: bool = ..., creator_id: _Optional[str] = ..., updater_id: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class Antenna(_message.Message):
    __slots__ = ["id", "element_id", "name", "description", "type", "vendor", "model", "url", "gain", "beam_width", "gain_profile_data", "gain_profile_format", "gain_profile_url", "creator_id", "updater_id", "created_at", "updated_at", "deleted_at"]
    ID_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    VENDOR_FIELD_NUMBER: _ClassVar[int]
    MODEL_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    GAIN_FIELD_NUMBER: _ClassVar[int]
    BEAM_WIDTH_FIELD_NUMBER: _ClassVar[int]
    GAIN_PROFILE_DATA_FIELD_NUMBER: _ClassVar[int]
    GAIN_PROFILE_FORMAT_FIELD_NUMBER: _ClassVar[int]
    GAIN_PROFILE_URL_FIELD_NUMBER: _ClassVar[int]
    CREATOR_ID_FIELD_NUMBER: _ClassVar[int]
    UPDATER_ID_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    id: str
    element_id: str
    name: str
    description: str
    type: str
    vendor: str
    model: str
    url: str
    gain: float
    beam_width: int
    gain_profile_data: bytes
    gain_profile_format: str
    gain_profile_url: str
    creator_id: str
    updater_id: str
    created_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    def __init__(self, id: _Optional[str] = ..., element_id: _Optional[str] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., type: _Optional[str] = ..., vendor: _Optional[str] = ..., model: _Optional[str] = ..., url: _Optional[str] = ..., gain: _Optional[float] = ..., beam_width: _Optional[int] = ..., gain_profile_data: _Optional[bytes] = ..., gain_profile_format: _Optional[str] = ..., gain_profile_url: _Optional[str] = ..., creator_id: _Optional[str] = ..., updater_id: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class RadioPort(_message.Message):
    __slots__ = ["id", "radio_id", "name", "tx", "rx", "min_freq", "max_freq", "max_power", "antenna_id", "antenna_location_id", "antenna_azimuth_angle", "antenna_elevation_angle", "enabled", "creator_id", "updater_id", "created_at", "updated_at", "deleted_at", "antenna", "antenna_location", "antenna_elevation_delta"]
    ID_FIELD_NUMBER: _ClassVar[int]
    RADIO_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    TX_FIELD_NUMBER: _ClassVar[int]
    RX_FIELD_NUMBER: _ClassVar[int]
    MIN_FREQ_FIELD_NUMBER: _ClassVar[int]
    MAX_FREQ_FIELD_NUMBER: _ClassVar[int]
    MAX_POWER_FIELD_NUMBER: _ClassVar[int]
    ANTENNA_ID_FIELD_NUMBER: _ClassVar[int]
    ANTENNA_LOCATION_ID_FIELD_NUMBER: _ClassVar[int]
    ANTENNA_AZIMUTH_ANGLE_FIELD_NUMBER: _ClassVar[int]
    ANTENNA_ELEVATION_ANGLE_FIELD_NUMBER: _ClassVar[int]
    ENABLED_FIELD_NUMBER: _ClassVar[int]
    CREATOR_ID_FIELD_NUMBER: _ClassVar[int]
    UPDATER_ID_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    ANTENNA_FIELD_NUMBER: _ClassVar[int]
    ANTENNA_LOCATION_FIELD_NUMBER: _ClassVar[int]
    ANTENNA_ELEVATION_DELTA_FIELD_NUMBER: _ClassVar[int]
    id: str
    radio_id: str
    name: str
    tx: bool
    rx: bool
    min_freq: int
    max_freq: int
    max_power: float
    antenna_id: str
    antenna_location_id: str
    antenna_azimuth_angle: float
    antenna_elevation_angle: float
    enabled: bool
    creator_id: str
    updater_id: str
    created_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    antenna: Antenna
    antenna_location: Location
    antenna_elevation_delta: float
    def __init__(self, id: _Optional[str] = ..., radio_id: _Optional[str] = ..., name: _Optional[str] = ..., tx: bool = ..., rx: bool = ..., min_freq: _Optional[int] = ..., max_freq: _Optional[int] = ..., max_power: _Optional[float] = ..., antenna_id: _Optional[str] = ..., antenna_location_id: _Optional[str] = ..., antenna_azimuth_angle: _Optional[float] = ..., antenna_elevation_angle: _Optional[float] = ..., enabled: bool = ..., creator_id: _Optional[str] = ..., updater_id: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., antenna: _Optional[_Union[Antenna, _Mapping]] = ..., antenna_location: _Optional[_Union[Location, _Mapping]] = ..., antenna_elevation_delta: _Optional[float] = ...) -> None: ...

class Radio(_message.Message):
    __slots__ = ["id", "element_id", "name", "description", "fcc_id", "device_id", "serial_number", "location_id", "location", "enabled", "creator_id", "updater_id", "created_at", "updated_at", "deleted_at", "ports"]
    ID_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    FCC_ID_FIELD_NUMBER: _ClassVar[int]
    DEVICE_ID_FIELD_NUMBER: _ClassVar[int]
    SERIAL_NUMBER_FIELD_NUMBER: _ClassVar[int]
    LOCATION_ID_FIELD_NUMBER: _ClassVar[int]
    LOCATION_FIELD_NUMBER: _ClassVar[int]
    ENABLED_FIELD_NUMBER: _ClassVar[int]
    CREATOR_ID_FIELD_NUMBER: _ClassVar[int]
    UPDATER_ID_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    PORTS_FIELD_NUMBER: _ClassVar[int]
    id: str
    element_id: str
    name: str
    description: str
    fcc_id: str
    device_id: str
    serial_number: str
    location_id: str
    location: Location
    enabled: bool
    creator_id: str
    updater_id: str
    created_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    ports: _containers.RepeatedCompositeFieldContainer[RadioPort]
    def __init__(self, id: _Optional[str] = ..., element_id: _Optional[str] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., fcc_id: _Optional[str] = ..., device_id: _Optional[str] = ..., serial_number: _Optional[str] = ..., location_id: _Optional[str] = ..., location: _Optional[_Union[Location, _Mapping]] = ..., enabled: bool = ..., creator_id: _Optional[str] = ..., updater_id: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., ports: _Optional[_Iterable[_Union[RadioPort, _Mapping]]] = ...) -> None: ...

class Monitor(_message.Message):
    __slots__ = ["id", "radio_port_id", "monitored_radio_port_id", "name", "description", "types", "formats", "config", "exclusive", "enabled", "creator_id", "updater_id", "created_at", "updated_at", "deleted_at", "radio_port", "monitored_radio_port"]
    ID_FIELD_NUMBER: _ClassVar[int]
    RADIO_PORT_ID_FIELD_NUMBER: _ClassVar[int]
    MONITORED_RADIO_PORT_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    TYPES_FIELD_NUMBER: _ClassVar[int]
    FORMATS_FIELD_NUMBER: _ClassVar[int]
    CONFIG_FIELD_NUMBER: _ClassVar[int]
    EXCLUSIVE_FIELD_NUMBER: _ClassVar[int]
    ENABLED_FIELD_NUMBER: _ClassVar[int]
    CREATOR_ID_FIELD_NUMBER: _ClassVar[int]
    UPDATER_ID_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    RADIO_PORT_FIELD_NUMBER: _ClassVar[int]
    MONITORED_RADIO_PORT_FIELD_NUMBER: _ClassVar[int]
    id: str
    radio_port_id: str
    monitored_radio_port_id: str
    name: str
    description: str
    types: str
    formats: str
    config: str
    exclusive: bool
    enabled: bool
    creator_id: str
    updater_id: str
    created_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    radio_port: RadioPort
    monitored_radio_port: RadioPort
    def __init__(self, id: _Optional[str] = ..., radio_port_id: _Optional[str] = ..., monitored_radio_port_id: _Optional[str] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., types: _Optional[str] = ..., formats: _Optional[str] = ..., config: _Optional[str] = ..., exclusive: bool = ..., enabled: bool = ..., creator_id: _Optional[str] = ..., updater_id: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., radio_port: _Optional[_Union[RadioPort, _Mapping]] = ..., monitored_radio_port: _Optional[_Union[RadioPort, _Mapping]] = ...) -> None: ...

class Policy(_message.Message):
    __slots__ = ["id", "spectrum_id", "element_id", "allowed", "auto_approve", "url", "priority", "max_duration", "when_unoccupied", "disable_emit_check", "allow_inactive", "allow_conflicts"]
    ID_FIELD_NUMBER: _ClassVar[int]
    SPECTRUM_ID_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_ID_FIELD_NUMBER: _ClassVar[int]
    ALLOWED_FIELD_NUMBER: _ClassVar[int]
    AUTO_APPROVE_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    PRIORITY_FIELD_NUMBER: _ClassVar[int]
    MAX_DURATION_FIELD_NUMBER: _ClassVar[int]
    WHEN_UNOCCUPIED_FIELD_NUMBER: _ClassVar[int]
    DISABLE_EMIT_CHECK_FIELD_NUMBER: _ClassVar[int]
    ALLOW_INACTIVE_FIELD_NUMBER: _ClassVar[int]
    ALLOW_CONFLICTS_FIELD_NUMBER: _ClassVar[int]
    id: str
    spectrum_id: str
    element_id: str
    allowed: bool
    auto_approve: bool
    url: str
    priority: int
    max_duration: int
    when_unoccupied: bool
    disable_emit_check: bool
    allow_inactive: bool
    allow_conflicts: bool
    def __init__(self, id: _Optional[str] = ..., spectrum_id: _Optional[str] = ..., element_id: _Optional[str] = ..., allowed: bool = ..., auto_approve: bool = ..., url: _Optional[str] = ..., priority: _Optional[int] = ..., max_duration: _Optional[int] = ..., when_unoccupied: bool = ..., disable_emit_check: bool = ..., allow_inactive: bool = ..., allow_conflicts: bool = ...) -> None: ...

class Constraint(_message.Message):
    __slots__ = ["id", "min_freq", "max_freq", "bandwidth", "max_eirp", "min_eirp", "exclusive", "area_id", "area"]
    ID_FIELD_NUMBER: _ClassVar[int]
    MIN_FREQ_FIELD_NUMBER: _ClassVar[int]
    MAX_FREQ_FIELD_NUMBER: _ClassVar[int]
    BANDWIDTH_FIELD_NUMBER: _ClassVar[int]
    MAX_EIRP_FIELD_NUMBER: _ClassVar[int]
    MIN_EIRP_FIELD_NUMBER: _ClassVar[int]
    EXCLUSIVE_FIELD_NUMBER: _ClassVar[int]
    AREA_ID_FIELD_NUMBER: _ClassVar[int]
    AREA_FIELD_NUMBER: _ClassVar[int]
    id: str
    min_freq: int
    max_freq: int
    bandwidth: int
    max_eirp: float
    min_eirp: float
    exclusive: bool
    area_id: str
    area: _geo_pb2.Area
    def __init__(self, id: _Optional[str] = ..., min_freq: _Optional[int] = ..., max_freq: _Optional[int] = ..., bandwidth: _Optional[int] = ..., max_eirp: _Optional[float] = ..., min_eirp: _Optional[float] = ..., exclusive: bool = ..., area_id: _Optional[str] = ..., area: _Optional[_Union[_geo_pb2.Area, _Mapping]] = ...) -> None: ...

class Spectrum(_message.Message):
    __slots__ = ["id", "element_id", "name", "description", "url", "enabled", "creator_id", "updater_id", "created_at", "updated_at", "approved_at", "denied_at", "starts_at", "expires_at", "deleted_at", "constraints", "policies"]
    ID_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    URL_FIELD_NUMBER: _ClassVar[int]
    ENABLED_FIELD_NUMBER: _ClassVar[int]
    CREATOR_ID_FIELD_NUMBER: _ClassVar[int]
    UPDATER_ID_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    APPROVED_AT_FIELD_NUMBER: _ClassVar[int]
    DENIED_AT_FIELD_NUMBER: _ClassVar[int]
    STARTS_AT_FIELD_NUMBER: _ClassVar[int]
    EXPIRES_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    CONSTRAINTS_FIELD_NUMBER: _ClassVar[int]
    POLICIES_FIELD_NUMBER: _ClassVar[int]
    id: str
    element_id: str
    name: str
    description: str
    url: str
    enabled: bool
    creator_id: str
    updater_id: str
    created_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    approved_at: _timestamp_pb2.Timestamp
    denied_at: _timestamp_pb2.Timestamp
    starts_at: _timestamp_pb2.Timestamp
    expires_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    constraints: _containers.RepeatedCompositeFieldContainer[Constraint]
    policies: _containers.RepeatedCompositeFieldContainer[Policy]
    def __init__(self, id: _Optional[str] = ..., element_id: _Optional[str] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., url: _Optional[str] = ..., enabled: bool = ..., creator_id: _Optional[str] = ..., updater_id: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., approved_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., denied_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., starts_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., expires_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., constraints: _Optional[_Iterable[_Union[Constraint, _Mapping]]] = ..., policies: _Optional[_Iterable[_Union[Policy, _Mapping]]] = ...) -> None: ...

class IntConstraint(_message.Message):
    __slots__ = ["id", "name", "max_power", "area_id", "area", "description"]
    ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    MAX_POWER_FIELD_NUMBER: _ClassVar[int]
    AREA_ID_FIELD_NUMBER: _ClassVar[int]
    AREA_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    id: str
    name: str
    max_power: float
    area_id: str
    area: _geo_pb2.Area
    description: str
    def __init__(self, id: _Optional[str] = ..., name: _Optional[str] = ..., max_power: _Optional[float] = ..., area_id: _Optional[str] = ..., area: _Optional[_Union[_geo_pb2.Area, _Mapping]] = ..., description: _Optional[str] = ...) -> None: ...

class RtIntConstraint(_message.Message):
    __slots__ = ["id", "name", "metric", "comparator", "value", "aggregator", "period", "description", "criticality"]
    ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    METRIC_FIELD_NUMBER: _ClassVar[int]
    COMPARATOR_FIELD_NUMBER: _ClassVar[int]
    VALUE_FIELD_NUMBER: _ClassVar[int]
    AGGREGATOR_FIELD_NUMBER: _ClassVar[int]
    PERIOD_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    CRITICALITY_FIELD_NUMBER: _ClassVar[int]
    id: str
    name: str
    metric: str
    comparator: str
    value: float
    aggregator: str
    period: int
    description: str
    criticality: int
    def __init__(self, id: _Optional[str] = ..., name: _Optional[str] = ..., metric: _Optional[str] = ..., comparator: _Optional[str] = ..., value: _Optional[float] = ..., aggregator: _Optional[str] = ..., period: _Optional[int] = ..., description: _Optional[str] = ..., criticality: _Optional[int] = ...) -> None: ...

class Grant(_message.Message):
    __slots__ = ["id", "element_id", "name", "description", "priority", "spectrum_id", "starts_at", "expires_at", "status", "status_ack_by", "op_status", "op_status_updated_at", "creator_id", "updater_id", "created_at", "updated_at", "approved_at", "owner_approved_at", "denied_at", "revoked_at", "deleted_at", "constraints", "int_constraints", "rt_int_constraints", "radio_ports", "logs", "replacement"]
    ID_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    PRIORITY_FIELD_NUMBER: _ClassVar[int]
    SPECTRUM_ID_FIELD_NUMBER: _ClassVar[int]
    STARTS_AT_FIELD_NUMBER: _ClassVar[int]
    EXPIRES_AT_FIELD_NUMBER: _ClassVar[int]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    STATUS_ACK_BY_FIELD_NUMBER: _ClassVar[int]
    OP_STATUS_FIELD_NUMBER: _ClassVar[int]
    OP_STATUS_UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    CREATOR_ID_FIELD_NUMBER: _ClassVar[int]
    UPDATER_ID_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    APPROVED_AT_FIELD_NUMBER: _ClassVar[int]
    OWNER_APPROVED_AT_FIELD_NUMBER: _ClassVar[int]
    DENIED_AT_FIELD_NUMBER: _ClassVar[int]
    REVOKED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    CONSTRAINTS_FIELD_NUMBER: _ClassVar[int]
    INT_CONSTRAINTS_FIELD_NUMBER: _ClassVar[int]
    RT_INT_CONSTRAINTS_FIELD_NUMBER: _ClassVar[int]
    RADIO_PORTS_FIELD_NUMBER: _ClassVar[int]
    LOGS_FIELD_NUMBER: _ClassVar[int]
    REPLACEMENT_FIELD_NUMBER: _ClassVar[int]
    id: str
    element_id: str
    name: str
    description: str
    priority: int
    spectrum_id: str
    starts_at: _timestamp_pb2.Timestamp
    expires_at: _timestamp_pb2.Timestamp
    status: str
    status_ack_by: _timestamp_pb2.Timestamp
    op_status: str
    op_status_updated_at: _timestamp_pb2.Timestamp
    creator_id: str
    updater_id: str
    created_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    approved_at: _timestamp_pb2.Timestamp
    owner_approved_at: _timestamp_pb2.Timestamp
    denied_at: _timestamp_pb2.Timestamp
    revoked_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    constraints: _containers.RepeatedCompositeFieldContainer[Constraint]
    int_constraints: _containers.RepeatedCompositeFieldContainer[IntConstraint]
    rt_int_constraints: _containers.RepeatedCompositeFieldContainer[RtIntConstraint]
    radio_ports: _containers.RepeatedCompositeFieldContainer[RadioPort]
    logs: _containers.RepeatedCompositeFieldContainer[GrantLog]
    replacement: GrantReplacement
    def __init__(self, id: _Optional[str] = ..., element_id: _Optional[str] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., priority: _Optional[int] = ..., spectrum_id: _Optional[str] = ..., starts_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., expires_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., status: _Optional[str] = ..., status_ack_by: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., op_status: _Optional[str] = ..., op_status_updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., creator_id: _Optional[str] = ..., updater_id: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., approved_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., owner_approved_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., denied_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., revoked_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., constraints: _Optional[_Iterable[_Union[Constraint, _Mapping]]] = ..., int_constraints: _Optional[_Iterable[_Union[IntConstraint, _Mapping]]] = ..., rt_int_constraints: _Optional[_Iterable[_Union[RtIntConstraint, _Mapping]]] = ..., radio_ports: _Optional[_Iterable[_Union[RadioPort, _Mapping]]] = ..., logs: _Optional[_Iterable[_Union[GrantLog, _Mapping]]] = ..., replacement: _Optional[_Union[GrantReplacement, _Mapping]] = ...) -> None: ...

class GrantLog(_message.Message):
    __slots__ = ["id", "grant_id", "status", "op_status", "message", "created_at"]
    ID_FIELD_NUMBER: _ClassVar[int]
    GRANT_ID_FIELD_NUMBER: _ClassVar[int]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    OP_STATUS_FIELD_NUMBER: _ClassVar[int]
    MESSAGE_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    id: str
    grant_id: str
    status: str
    op_status: str
    message: str
    created_at: _timestamp_pb2.Timestamp
    def __init__(self, id: _Optional[str] = ..., grant_id: _Optional[str] = ..., status: _Optional[str] = ..., op_status: _Optional[str] = ..., message: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class GrantReplacement(_message.Message):
    __slots__ = ["id", "grant_id", "new_grant_id", "description", "created_at", "constraint_change_id", "observation_id"]
    ID_FIELD_NUMBER: _ClassVar[int]
    GRANT_ID_FIELD_NUMBER: _ClassVar[int]
    NEW_GRANT_ID_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    CONSTRAINT_CHANGE_ID_FIELD_NUMBER: _ClassVar[int]
    OBSERVATION_ID_FIELD_NUMBER: _ClassVar[int]
    id: str
    grant_id: str
    new_grant_id: str
    description: str
    created_at: _timestamp_pb2.Timestamp
    constraint_change_id: str
    observation_id: str
    def __init__(self, id: _Optional[str] = ..., grant_id: _Optional[str] = ..., new_grant_id: _Optional[str] = ..., description: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., constraint_change_id: _Optional[str] = ..., observation_id: _Optional[str] = ...) -> None: ...

class Claim(_message.Message):
    __slots__ = ["id", "element_id", "ext_id", "type", "source", "html_url", "name", "description", "creator_id", "updater_id", "created_at", "updated_at", "verified_at", "denied_at", "deleted_at", "grant_id", "grant"]
    ID_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_ID_FIELD_NUMBER: _ClassVar[int]
    EXT_ID_FIELD_NUMBER: _ClassVar[int]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    SOURCE_FIELD_NUMBER: _ClassVar[int]
    HTML_URL_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    CREATOR_ID_FIELD_NUMBER: _ClassVar[int]
    UPDATER_ID_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    VERIFIED_AT_FIELD_NUMBER: _ClassVar[int]
    DENIED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    GRANT_ID_FIELD_NUMBER: _ClassVar[int]
    GRANT_FIELD_NUMBER: _ClassVar[int]
    id: str
    element_id: str
    ext_id: str
    type: str
    source: str
    html_url: str
    name: str
    description: str
    creator_id: str
    updater_id: str
    created_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    verified_at: _timestamp_pb2.Timestamp
    denied_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    grant_id: str
    grant: Grant
    def __init__(self, id: _Optional[str] = ..., element_id: _Optional[str] = ..., ext_id: _Optional[str] = ..., type: _Optional[str] = ..., source: _Optional[str] = ..., html_url: _Optional[str] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., creator_id: _Optional[str] = ..., updater_id: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., verified_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., denied_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., grant_id: _Optional[str] = ..., grant: _Optional[_Union[Grant, _Mapping]] = ...) -> None: ...

class Event(_message.Message):
    __slots__ = ["header", "zone", "antenna", "radio", "radio_port", "monitor", "grant", "spectrum", "claim"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ZONE_FIELD_NUMBER: _ClassVar[int]
    ANTENNA_FIELD_NUMBER: _ClassVar[int]
    RADIO_FIELD_NUMBER: _ClassVar[int]
    RADIO_PORT_FIELD_NUMBER: _ClassVar[int]
    MONITOR_FIELD_NUMBER: _ClassVar[int]
    GRANT_FIELD_NUMBER: _ClassVar[int]
    SPECTRUM_FIELD_NUMBER: _ClassVar[int]
    CLAIM_FIELD_NUMBER: _ClassVar[int]
    header: _event_pb2.EventHeader
    zone: Zone
    antenna: Antenna
    radio: Radio
    radio_port: RadioPort
    monitor: Monitor
    grant: Grant
    spectrum: Spectrum
    claim: Claim
    def __init__(self, header: _Optional[_Union[_event_pb2.EventHeader, _Mapping]] = ..., zone: _Optional[_Union[Zone, _Mapping]] = ..., antenna: _Optional[_Union[Antenna, _Mapping]] = ..., radio: _Optional[_Union[Radio, _Mapping]] = ..., radio_port: _Optional[_Union[RadioPort, _Mapping]] = ..., monitor: _Optional[_Union[Monitor, _Mapping]] = ..., grant: _Optional[_Union[Grant, _Mapping]] = ..., spectrum: _Optional[_Union[Spectrum, _Mapping]] = ..., claim: _Optional[_Union[Claim, _Mapping]] = ...) -> None: ...

class RequestHeader(_message.Message):
    __slots__ = ["req_id", "elaborate"]
    REQ_ID_FIELD_NUMBER: _ClassVar[int]
    ELABORATE_FIELD_NUMBER: _ClassVar[int]
    req_id: str
    elaborate: bool
    def __init__(self, req_id: _Optional[str] = ..., elaborate: bool = ...) -> None: ...

class ResponseHeader(_message.Message):
    __slots__ = ["req_id"]
    REQ_ID_FIELD_NUMBER: _ClassVar[int]
    req_id: str
    def __init__(self, req_id: _Optional[str] = ...) -> None: ...

class GetZoneRequest(_message.Message):
    __slots__ = ["header", "id"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    id: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., id: _Optional[str] = ...) -> None: ...

class GetZoneResponse(_message.Message):
    __slots__ = ["header", "zone"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ZONE_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    zone: Zone
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., zone: _Optional[_Union[Zone, _Mapping]] = ...) -> None: ...

class GetLocationRequest(_message.Message):
    __slots__ = ["header", "id"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    id: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., id: _Optional[str] = ...) -> None: ...

class GetLocationResponse(_message.Message):
    __slots__ = ["header", "location"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    LOCATION_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    location: Location
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., location: _Optional[_Union[Location, _Mapping]] = ...) -> None: ...

class GetAntennaRequest(_message.Message):
    __slots__ = ["header", "id"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    id: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., id: _Optional[str] = ...) -> None: ...

class GetAntennaResponse(_message.Message):
    __slots__ = ["header", "antenna"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ANTENNA_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    antenna: Antenna
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., antenna: _Optional[_Union[Antenna, _Mapping]] = ...) -> None: ...

class GetRadioPortRequest(_message.Message):
    __slots__ = ["header", "id"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    id: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., id: _Optional[str] = ...) -> None: ...

class GetRadioPortResponse(_message.Message):
    __slots__ = ["header", "radio_port"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    RADIO_PORT_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    radio_port: RadioPort
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., radio_port: _Optional[_Union[RadioPort, _Mapping]] = ...) -> None: ...

class GetRadioRequest(_message.Message):
    __slots__ = ["header", "id"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    id: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., id: _Optional[str] = ...) -> None: ...

class GetRadioResponse(_message.Message):
    __slots__ = ["header", "radio"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    RADIO_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    radio: Radio
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., radio: _Optional[_Union[Radio, _Mapping]] = ...) -> None: ...

class GetMonitorRequest(_message.Message):
    __slots__ = ["header", "id"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    id: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., id: _Optional[str] = ...) -> None: ...

class GetMonitorResponse(_message.Message):
    __slots__ = ["header", "monitor"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    MONITOR_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    monitor: Monitor
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., monitor: _Optional[_Union[Monitor, _Mapping]] = ...) -> None: ...

class GetMonitorsRequest(_message.Message):
    __slots__ = ["header", "element_id", "radio_port_id", "monitor", "types", "formats", "exclusive", "enabled"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_ID_FIELD_NUMBER: _ClassVar[int]
    RADIO_PORT_ID_FIELD_NUMBER: _ClassVar[int]
    MONITOR_FIELD_NUMBER: _ClassVar[int]
    TYPES_FIELD_NUMBER: _ClassVar[int]
    FORMATS_FIELD_NUMBER: _ClassVar[int]
    EXCLUSIVE_FIELD_NUMBER: _ClassVar[int]
    ENABLED_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    element_id: str
    radio_port_id: str
    monitor: str
    types: str
    formats: str
    exclusive: bool
    enabled: bool
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., element_id: _Optional[str] = ..., radio_port_id: _Optional[str] = ..., monitor: _Optional[str] = ..., types: _Optional[str] = ..., formats: _Optional[str] = ..., exclusive: bool = ..., enabled: bool = ...) -> None: ...

class GetMonitorsResponse(_message.Message):
    __slots__ = ["header", "monitors"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    MONITORS_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    monitors: _containers.RepeatedCompositeFieldContainer[Monitor]
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., monitors: _Optional[_Iterable[_Union[Monitor, _Mapping]]] = ...) -> None: ...

class GetGrantRequest(_message.Message):
    __slots__ = ["header", "id"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    id: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., id: _Optional[str] = ...) -> None: ...

class GetGrantResponse(_message.Message):
    __slots__ = ["header", "grant"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    GRANT_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    grant: Grant
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., grant: _Optional[_Union[Grant, _Mapping]] = ...) -> None: ...

class GetGrantsRequest(_message.Message):
    __slots__ = ["header", "element_id", "radio_port_id", "creator_id", "time", "time_max", "freq", "exclusive", "approved", "owner_approved", "denied", "revoked", "deleted", "spectrum_id", "status", "op_status"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_ID_FIELD_NUMBER: _ClassVar[int]
    RADIO_PORT_ID_FIELD_NUMBER: _ClassVar[int]
    CREATOR_ID_FIELD_NUMBER: _ClassVar[int]
    TIME_FIELD_NUMBER: _ClassVar[int]
    TIME_MAX_FIELD_NUMBER: _ClassVar[int]
    FREQ_FIELD_NUMBER: _ClassVar[int]
    EXCLUSIVE_FIELD_NUMBER: _ClassVar[int]
    APPROVED_FIELD_NUMBER: _ClassVar[int]
    OWNER_APPROVED_FIELD_NUMBER: _ClassVar[int]
    DENIED_FIELD_NUMBER: _ClassVar[int]
    REVOKED_FIELD_NUMBER: _ClassVar[int]
    DELETED_FIELD_NUMBER: _ClassVar[int]
    SPECTRUM_ID_FIELD_NUMBER: _ClassVar[int]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    OP_STATUS_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    element_id: str
    radio_port_id: str
    creator_id: str
    time: _timestamp_pb2.Timestamp
    time_max: _timestamp_pb2.Timestamp
    freq: int
    exclusive: bool
    approved: bool
    owner_approved: bool
    denied: bool
    revoked: bool
    deleted: bool
    spectrum_id: str
    status: str
    op_status: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., element_id: _Optional[str] = ..., radio_port_id: _Optional[str] = ..., creator_id: _Optional[str] = ..., time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., time_max: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., freq: _Optional[int] = ..., exclusive: bool = ..., approved: bool = ..., owner_approved: bool = ..., denied: bool = ..., revoked: bool = ..., deleted: bool = ..., spectrum_id: _Optional[str] = ..., status: _Optional[str] = ..., op_status: _Optional[str] = ...) -> None: ...

class GetGrantsResponse(_message.Message):
    __slots__ = ["header", "grants"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    GRANTS_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    grants: _containers.RepeatedCompositeFieldContainer[Grant]
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., grants: _Optional[_Iterable[_Union[Grant, _Mapping]]] = ...) -> None: ...

class GetSpectrumRequest(_message.Message):
    __slots__ = ["header", "id"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    id: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., id: _Optional[str] = ...) -> None: ...

class GetSpectrumResponse(_message.Message):
    __slots__ = ["header", "spectrum"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    SPECTRUM_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    spectrum: Spectrum
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., spectrum: _Optional[_Union[Spectrum, _Mapping]] = ...) -> None: ...

class SubscribeRequest(_message.Message):
    __slots__ = ["header", "filters", "include"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    FILTERS_FIELD_NUMBER: _ClassVar[int]
    INCLUDE_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    filters: _containers.RepeatedCompositeFieldContainer[_event_pb2.EventFilter]
    include: bool
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., filters: _Optional[_Iterable[_Union[_event_pb2.EventFilter, _Mapping]]] = ..., include: bool = ...) -> None: ...

class SubscribeResponse(_message.Message):
    __slots__ = ["header", "events"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    EVENTS_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    events: _containers.RepeatedCompositeFieldContainer[Event]
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., events: _Optional[_Iterable[_Union[Event, _Mapping]]] = ...) -> None: ...
