from validate import validate_pb2 as _validate_pb2
from google.protobuf import timestamp_pb2 as _timestamp_pb2
from zms_api.event.v1 import event_pb2 as _event_pb2
from zms_api.geo.v1 import geo_pb2 as _geo_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class EventCode(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    EC_UNSPECIFIED: _ClassVar[EventCode]
    EC_OBSERVATION: _ClassVar[EventCode]
    EC_ANNOTATION: _ClassVar[EventCode]
    EC_PROPSIM: _ClassVar[EventCode]
    EC_PROPSIM_JOB: _ClassVar[EventCode]
EC_UNSPECIFIED: EventCode
EC_OBSERVATION: EventCode
EC_ANNOTATION: EventCode
EC_PROPSIM: EventCode
EC_PROPSIM_JOB: EventCode

class Annotation(_message.Message):
    __slots__ = ["id", "observation_id", "type", "name", "description", "value", "creator_id", "updater_id", "created_at", "updated_at", "deleted_at"]
    ID_FIELD_NUMBER: _ClassVar[int]
    OBSERVATION_ID_FIELD_NUMBER: _ClassVar[int]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    VALUE_FIELD_NUMBER: _ClassVar[int]
    CREATOR_ID_FIELD_NUMBER: _ClassVar[int]
    UPDATER_ID_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    id: str
    observation_id: str
    type: str
    name: str
    description: str
    value: str
    creator_id: str
    updater_id: str
    created_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    def __init__(self, id: _Optional[str] = ..., observation_id: _Optional[str] = ..., type: _Optional[str] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., value: _Optional[str] = ..., creator_id: _Optional[str] = ..., updater_id: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class Observation(_message.Message):
    __slots__ = ["id", "monitor_id", "collection_id", "types", "format", "description", "min_freq", "max_freq", "freq_step", "starts_at", "ends_at", "data", "violation", "violation_verified_at", "interference", "interference_verified_at", "element_id", "creator_id", "updater_id", "created_at", "updated_at", "deleted_at", "violated_grant_id", "interfered_grant_id", "annotations"]
    ID_FIELD_NUMBER: _ClassVar[int]
    MONITOR_ID_FIELD_NUMBER: _ClassVar[int]
    COLLECTION_ID_FIELD_NUMBER: _ClassVar[int]
    TYPES_FIELD_NUMBER: _ClassVar[int]
    FORMAT_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    MIN_FREQ_FIELD_NUMBER: _ClassVar[int]
    MAX_FREQ_FIELD_NUMBER: _ClassVar[int]
    FREQ_STEP_FIELD_NUMBER: _ClassVar[int]
    STARTS_AT_FIELD_NUMBER: _ClassVar[int]
    ENDS_AT_FIELD_NUMBER: _ClassVar[int]
    DATA_FIELD_NUMBER: _ClassVar[int]
    VIOLATION_FIELD_NUMBER: _ClassVar[int]
    VIOLATION_VERIFIED_AT_FIELD_NUMBER: _ClassVar[int]
    INTERFERENCE_FIELD_NUMBER: _ClassVar[int]
    INTERFERENCE_VERIFIED_AT_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_ID_FIELD_NUMBER: _ClassVar[int]
    CREATOR_ID_FIELD_NUMBER: _ClassVar[int]
    UPDATER_ID_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    UPDATED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    VIOLATED_GRANT_ID_FIELD_NUMBER: _ClassVar[int]
    INTERFERED_GRANT_ID_FIELD_NUMBER: _ClassVar[int]
    ANNOTATIONS_FIELD_NUMBER: _ClassVar[int]
    id: str
    monitor_id: str
    collection_id: str
    types: str
    format: str
    description: str
    min_freq: int
    max_freq: int
    freq_step: int
    starts_at: _timestamp_pb2.Timestamp
    ends_at: _timestamp_pb2.Timestamp
    data: bytes
    violation: bool
    violation_verified_at: _timestamp_pb2.Timestamp
    interference: bool
    interference_verified_at: _timestamp_pb2.Timestamp
    element_id: str
    creator_id: str
    updater_id: str
    created_at: _timestamp_pb2.Timestamp
    updated_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    violated_grant_id: str
    interfered_grant_id: str
    annotations: _containers.RepeatedCompositeFieldContainer[Annotation]
    def __init__(self, id: _Optional[str] = ..., monitor_id: _Optional[str] = ..., collection_id: _Optional[str] = ..., types: _Optional[str] = ..., format: _Optional[str] = ..., description: _Optional[str] = ..., min_freq: _Optional[int] = ..., max_freq: _Optional[int] = ..., freq_step: _Optional[int] = ..., starts_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., ends_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., data: _Optional[bytes] = ..., violation: bool = ..., violation_verified_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., interference: bool = ..., interference_verified_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., element_id: _Optional[str] = ..., creator_id: _Optional[str] = ..., updater_id: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., updated_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., violated_grant_id: _Optional[str] = ..., interfered_grant_id: _Optional[str] = ..., annotations: _Optional[_Iterable[_Union[Annotation, _Mapping]]] = ...) -> None: ...

class Event(_message.Message):
    __slots__ = ["header", "observation", "annotation"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    OBSERVATION_FIELD_NUMBER: _ClassVar[int]
    ANNOTATION_FIELD_NUMBER: _ClassVar[int]
    header: _event_pb2.EventHeader
    observation: Observation
    annotation: Annotation
    def __init__(self, header: _Optional[_Union[_event_pb2.EventHeader, _Mapping]] = ..., observation: _Optional[_Union[Observation, _Mapping]] = ..., annotation: _Optional[_Union[Annotation, _Mapping]] = ...) -> None: ...

class RequestHeader(_message.Message):
    __slots__ = ["req_id", "elaborate"]
    REQ_ID_FIELD_NUMBER: _ClassVar[int]
    ELABORATE_FIELD_NUMBER: _ClassVar[int]
    req_id: str
    elaborate: bool
    def __init__(self, req_id: _Optional[str] = ..., elaborate: bool = ...) -> None: ...

class ResponseHeader(_message.Message):
    __slots__ = ["req_id"]
    REQ_ID_FIELD_NUMBER: _ClassVar[int]
    req_id: str
    def __init__(self, req_id: _Optional[str] = ...) -> None: ...

class GetObservationRequest(_message.Message):
    __slots__ = ["header", "id"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    id: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., id: _Optional[str] = ...) -> None: ...

class GetObservationResponse(_message.Message):
    __slots__ = ["header", "observation"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    OBSERVATION_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    observation: Observation
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., observation: _Optional[_Union[Observation, _Mapping]] = ...) -> None: ...

class GetObservationsRequest(_message.Message):
    __slots__ = ["header", "min_freq", "max_freq", "min_power", "max_power", "violation", "starts_at", "ends_at"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    MIN_FREQ_FIELD_NUMBER: _ClassVar[int]
    MAX_FREQ_FIELD_NUMBER: _ClassVar[int]
    MIN_POWER_FIELD_NUMBER: _ClassVar[int]
    MAX_POWER_FIELD_NUMBER: _ClassVar[int]
    VIOLATION_FIELD_NUMBER: _ClassVar[int]
    STARTS_AT_FIELD_NUMBER: _ClassVar[int]
    ENDS_AT_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    min_freq: int
    max_freq: int
    min_power: float
    max_power: float
    violation: bool
    starts_at: _timestamp_pb2.Timestamp
    ends_at: _timestamp_pb2.Timestamp
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., min_freq: _Optional[int] = ..., max_freq: _Optional[int] = ..., min_power: _Optional[float] = ..., max_power: _Optional[float] = ..., violation: bool = ..., starts_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., ends_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class GetObservationsResponse(_message.Message):
    __slots__ = ["header", "observations"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    OBSERVATIONS_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    observations: _containers.RepeatedCompositeFieldContainer[Observation]
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., observations: _Optional[_Iterable[_Union[Observation, _Mapping]]] = ...) -> None: ...

class GetUnoccupiedSpectrumRequest(_message.Message):
    __slots__ = ["header", "min_freq", "max_freq", "occupancy_threshold", "min_bandwidth", "starts_at", "ends_at", "type", "format", "monitor_ids"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    MIN_FREQ_FIELD_NUMBER: _ClassVar[int]
    MAX_FREQ_FIELD_NUMBER: _ClassVar[int]
    OCCUPANCY_THRESHOLD_FIELD_NUMBER: _ClassVar[int]
    MIN_BANDWIDTH_FIELD_NUMBER: _ClassVar[int]
    STARTS_AT_FIELD_NUMBER: _ClassVar[int]
    ENDS_AT_FIELD_NUMBER: _ClassVar[int]
    TYPE_FIELD_NUMBER: _ClassVar[int]
    FORMAT_FIELD_NUMBER: _ClassVar[int]
    MONITOR_IDS_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    min_freq: int
    max_freq: int
    occupancy_threshold: float
    min_bandwidth: int
    starts_at: _timestamp_pb2.Timestamp
    ends_at: _timestamp_pb2.Timestamp
    type: str
    format: str
    monitor_ids: _containers.RepeatedScalarFieldContainer[str]
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., min_freq: _Optional[int] = ..., max_freq: _Optional[int] = ..., occupancy_threshold: _Optional[float] = ..., min_bandwidth: _Optional[int] = ..., starts_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., ends_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., type: _Optional[str] = ..., format: _Optional[str] = ..., monitor_ids: _Optional[_Iterable[str]] = ...) -> None: ...

class FrequencyRange(_message.Message):
    __slots__ = ["min_freq", "max_freq"]
    MIN_FREQ_FIELD_NUMBER: _ClassVar[int]
    MAX_FREQ_FIELD_NUMBER: _ClassVar[int]
    min_freq: int
    max_freq: int
    def __init__(self, min_freq: _Optional[int] = ..., max_freq: _Optional[int] = ...) -> None: ...

class GetUnoccupiedSpectrumResponse(_message.Message):
    __slots__ = ["header", "ranges", "starts_at", "ends_at"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    RANGES_FIELD_NUMBER: _ClassVar[int]
    STARTS_AT_FIELD_NUMBER: _ClassVar[int]
    ENDS_AT_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    ranges: _containers.RepeatedCompositeFieldContainer[FrequencyRange]
    starts_at: _timestamp_pb2.Timestamp
    ends_at: _timestamp_pb2.Timestamp
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., ranges: _Optional[_Iterable[_Union[FrequencyRange, _Mapping]]] = ..., starts_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., ends_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class GetTxConstraintsRequest(_message.Message):
    __slots__ = ["header", "radio_port_id", "freq", "area", "max_ext_rssi"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    RADIO_PORT_ID_FIELD_NUMBER: _ClassVar[int]
    FREQ_FIELD_NUMBER: _ClassVar[int]
    AREA_FIELD_NUMBER: _ClassVar[int]
    MAX_EXT_RSSI_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    radio_port_id: str
    freq: int
    area: _geo_pb2.Area
    max_ext_rssi: float
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., radio_port_id: _Optional[str] = ..., freq: _Optional[int] = ..., area: _Optional[_Union[_geo_pb2.Area, _Mapping]] = ..., max_ext_rssi: _Optional[float] = ...) -> None: ...

class GetTxConstraintsResponse(_message.Message):
    __slots__ = ["header", "max_power"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    MAX_POWER_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    max_power: float
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., max_power: _Optional[float] = ...) -> None: ...

class GetExpectedLossRequest(_message.Message):
    __slots__ = ["header", "radio_port_id", "freq", "bandwidth", "point"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    RADIO_PORT_ID_FIELD_NUMBER: _ClassVar[int]
    FREQ_FIELD_NUMBER: _ClassVar[int]
    BANDWIDTH_FIELD_NUMBER: _ClassVar[int]
    POINT_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    radio_port_id: str
    freq: int
    bandwidth: int
    point: _geo_pb2.Point
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., radio_port_id: _Optional[str] = ..., freq: _Optional[int] = ..., bandwidth: _Optional[int] = ..., point: _Optional[_Union[_geo_pb2.Point, _Mapping]] = ...) -> None: ...

class GetExpectedLossResponse(_message.Message):
    __slots__ = ["header", "loss"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    LOSS_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    loss: float
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., loss: _Optional[float] = ...) -> None: ...

class GetExpectedPowerRequest(_message.Message):
    __slots__ = ["header", "time", "grant_id", "radio_port_id", "freq", "bandwidth", "point"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    TIME_FIELD_NUMBER: _ClassVar[int]
    GRANT_ID_FIELD_NUMBER: _ClassVar[int]
    RADIO_PORT_ID_FIELD_NUMBER: _ClassVar[int]
    FREQ_FIELD_NUMBER: _ClassVar[int]
    BANDWIDTH_FIELD_NUMBER: _ClassVar[int]
    POINT_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    time: _timestamp_pb2.Timestamp
    grant_id: str
    radio_port_id: str
    freq: int
    bandwidth: int
    point: _geo_pb2.Point
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., time: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., grant_id: _Optional[str] = ..., radio_port_id: _Optional[str] = ..., freq: _Optional[int] = ..., bandwidth: _Optional[int] = ..., point: _Optional[_Union[_geo_pb2.Point, _Mapping]] = ...) -> None: ...

class PowerPerRadioPort(_message.Message):
    __slots__ = ["radio_port_id", "grant_id", "power"]
    RADIO_PORT_ID_FIELD_NUMBER: _ClassVar[int]
    GRANT_ID_FIELD_NUMBER: _ClassVar[int]
    POWER_FIELD_NUMBER: _ClassVar[int]
    radio_port_id: str
    grant_id: str
    power: float
    def __init__(self, radio_port_id: _Optional[str] = ..., grant_id: _Optional[str] = ..., power: _Optional[float] = ...) -> None: ...

class GetExpectedPowerResponse(_message.Message):
    __slots__ = ["header", "power", "radio_port_id", "power_per_radio_port"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    POWER_FIELD_NUMBER: _ClassVar[int]
    RADIO_PORT_ID_FIELD_NUMBER: _ClassVar[int]
    POWER_PER_RADIO_PORT_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    power: float
    radio_port_id: str
    power_per_radio_port: _containers.RepeatedCompositeFieldContainer[PowerPerRadioPort]
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., power: _Optional[float] = ..., radio_port_id: _Optional[str] = ..., power_per_radio_port: _Optional[_Iterable[_Union[PowerPerRadioPort, _Mapping]]] = ...) -> None: ...

class PropsimPowerGrant(_message.Message):
    __slots__ = ["id", "grant_id", "size", "created_at", "finished_at", "deleted_at", "geoserver_workspace", "geoserver_layer", "job_outputs"]
    ID_FIELD_NUMBER: _ClassVar[int]
    GRANT_ID_FIELD_NUMBER: _ClassVar[int]
    SIZE_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    FINISHED_AT_FIELD_NUMBER: _ClassVar[int]
    DELETED_AT_FIELD_NUMBER: _ClassVar[int]
    GEOSERVER_WORKSPACE_FIELD_NUMBER: _ClassVar[int]
    GEOSERVER_LAYER_FIELD_NUMBER: _ClassVar[int]
    JOB_OUTPUTS_FIELD_NUMBER: _ClassVar[int]
    id: str
    grant_id: str
    size: int
    created_at: _timestamp_pb2.Timestamp
    finished_at: _timestamp_pb2.Timestamp
    deleted_at: _timestamp_pb2.Timestamp
    geoserver_workspace: str
    geoserver_layer: str
    job_outputs: _containers.RepeatedCompositeFieldContainer[PropsimPowerGrantJobOutput]
    def __init__(self, id: _Optional[str] = ..., grant_id: _Optional[str] = ..., size: _Optional[int] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., finished_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., deleted_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ..., geoserver_workspace: _Optional[str] = ..., geoserver_layer: _Optional[str] = ..., job_outputs: _Optional[_Iterable[_Union[PropsimPowerGrantJobOutput, _Mapping]]] = ...) -> None: ...

class PropsimPowerGrantJobOutput(_message.Message):
    __slots__ = ["id", "propsim_power_grant_id", "propsim_job_output_id"]
    ID_FIELD_NUMBER: _ClassVar[int]
    PROPSIM_POWER_GRANT_ID_FIELD_NUMBER: _ClassVar[int]
    PROPSIM_JOB_OUTPUT_ID_FIELD_NUMBER: _ClassVar[int]
    id: str
    propsim_power_grant_id: str
    propsim_job_output_id: str
    def __init__(self, id: _Optional[str] = ..., propsim_power_grant_id: _Optional[str] = ..., propsim_job_output_id: _Optional[str] = ...) -> None: ...

class CreatePropsimPowerGrantRequest(_message.Message):
    __slots__ = ["header", "grant_id"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    GRANT_ID_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    grant_id: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., grant_id: _Optional[str] = ...) -> None: ...

class CreatePropsimPowerGrantResponse(_message.Message):
    __slots__ = ["header", "propsim_power_grant"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    PROPSIM_POWER_GRANT_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    propsim_power_grant: PropsimPowerGrant
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., propsim_power_grant: _Optional[_Union[PropsimPowerGrant, _Mapping]] = ...) -> None: ...

class PropsimPowerGlobal(_message.Message):
    __slots__ = ["id"]
    ID_FIELD_NUMBER: _ClassVar[int]
    id: str
    def __init__(self, id: _Optional[str] = ...) -> None: ...

class CreatePropsimPowerGlobalRequest(_message.Message):
    __slots__ = ["header", "grant_ids"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    GRANT_IDS_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    grant_ids: _containers.RepeatedScalarFieldContainer[str]
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., grant_ids: _Optional[_Iterable[str]] = ...) -> None: ...

class CreatePropsimPowerGlobalResponse(_message.Message):
    __slots__ = ["header", "propsim_power_global"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    PROPSIM_POWER_GLOBAL_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    propsim_power_global: PropsimPowerGlobal
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., propsim_power_global: _Optional[_Union[PropsimPowerGlobal, _Mapping]] = ...) -> None: ...

class CheckGrantConflictRequest(_message.Message):
    __slots__ = ["header", "grant1_id", "grant2_id"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    GRANT1_ID_FIELD_NUMBER: _ClassVar[int]
    GRANT2_ID_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    grant1_id: str
    grant2_id: str
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., grant1_id: _Optional[str] = ..., grant2_id: _Optional[str] = ...) -> None: ...

class CheckGrantConflictResponse(_message.Message):
    __slots__ = ["header", "is_conflict"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    IS_CONFLICT_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    is_conflict: bool
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., is_conflict: bool = ...) -> None: ...

class SubscribeRequest(_message.Message):
    __slots__ = ["header", "filters", "include"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    FILTERS_FIELD_NUMBER: _ClassVar[int]
    INCLUDE_FIELD_NUMBER: _ClassVar[int]
    header: RequestHeader
    filters: _containers.RepeatedCompositeFieldContainer[_event_pb2.EventFilter]
    include: bool
    def __init__(self, header: _Optional[_Union[RequestHeader, _Mapping]] = ..., filters: _Optional[_Iterable[_Union[_event_pb2.EventFilter, _Mapping]]] = ..., include: bool = ...) -> None: ...

class SubscribeResponse(_message.Message):
    __slots__ = ["header", "events"]
    HEADER_FIELD_NUMBER: _ClassVar[int]
    EVENTS_FIELD_NUMBER: _ClassVar[int]
    header: ResponseHeader
    events: _containers.RepeatedCompositeFieldContainer[Event]
    def __init__(self, header: _Optional[_Union[ResponseHeader, _Mapping]] = ..., events: _Optional[_Iterable[_Union[Event, _Mapping]]] = ...) -> None: ...
