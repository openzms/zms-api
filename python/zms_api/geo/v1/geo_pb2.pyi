from validate import validate_pb2 as _validate_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class Point(_message.Message):
    __slots__ = ["srid", "x", "y", "z"]
    SRID_FIELD_NUMBER: _ClassVar[int]
    X_FIELD_NUMBER: _ClassVar[int]
    Y_FIELD_NUMBER: _ClassVar[int]
    Z_FIELD_NUMBER: _ClassVar[int]
    srid: int
    x: float
    y: float
    z: float
    def __init__(self, srid: _Optional[int] = ..., x: _Optional[float] = ..., y: _Optional[float] = ..., z: _Optional[float] = ...) -> None: ...

class AreaPoint(_message.Message):
    __slots__ = ["id", "area_id", "x", "y", "z"]
    ID_FIELD_NUMBER: _ClassVar[int]
    AREA_ID_FIELD_NUMBER: _ClassVar[int]
    X_FIELD_NUMBER: _ClassVar[int]
    Y_FIELD_NUMBER: _ClassVar[int]
    Z_FIELD_NUMBER: _ClassVar[int]
    id: str
    area_id: str
    x: float
    y: float
    z: float
    def __init__(self, id: _Optional[str] = ..., area_id: _Optional[str] = ..., x: _Optional[float] = ..., y: _Optional[float] = ..., z: _Optional[float] = ...) -> None: ...

class Area(_message.Message):
    __slots__ = ["id", "element_id", "name", "description", "srid", "points"]
    ID_FIELD_NUMBER: _ClassVar[int]
    ELEMENT_ID_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    DESCRIPTION_FIELD_NUMBER: _ClassVar[int]
    SRID_FIELD_NUMBER: _ClassVar[int]
    POINTS_FIELD_NUMBER: _ClassVar[int]
    id: str
    element_id: str
    name: str
    description: str
    srid: int
    points: _containers.RepeatedCompositeFieldContainer[AreaPoint]
    def __init__(self, id: _Optional[str] = ..., element_id: _Optional[str] = ..., name: _Optional[str] = ..., description: _Optional[str] = ..., srid: _Optional[int] = ..., points: _Optional[_Iterable[_Union[AreaPoint, _Mapping]]] = ...) -> None: ...
