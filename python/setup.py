#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
#
# SPDX-License-Identifier: Apache-2.0

import setuptools

if __name__ == "__main__":
    setuptools.setup(
        name="zms-api",
        version="0.0.1",
        author="David M Johnson",
        author_email="johnsond@flux.utah.edu",
        url="https://gitlab.flux.utah.edu/openzms/zms-api/python",
        description="Python GRPC bindings for OpenZMS service internal APIs.",
        classifiers=[
            "Development Status :: 3 - Alpha",
            "Environment :: Other Environment",
            "Intended Audience :: Developers",
            "Operating System :: OS Independent",
            "Programming Language :: Python",
            "Programming Language :: Python :: 3",
            "Topic :: Utilities",
            "Typing :: Typed",
        ],
        packages=setuptools.find_packages(),
        install_requires=[
            "grpclib>=0.4.6",
            "protoc-gen-validate>=1.0.2",
            "protobuf>=4.25.0",
        ],
        python_requires=">=3.7",
        setup_requires=[
            "setuptools",
        ],
    )

